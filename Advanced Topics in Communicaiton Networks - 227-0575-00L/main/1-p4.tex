\setcounter{figure}{0}
\setcounter{code}{0}
\setcounter{table}{0}
\setcounter{eqn}{0}

\chapter{\pfour/}


%% Basics %%


\section{Construction of a \pfour/ program}

\subsection{Basics}

A \pfour/ program consists of three parts as shown in Figure \ref{fig:p4-parts}: 
\begin{enumerate}
  \item \textbf{Parser} Responsible for the header specification and extraction for incoming packets.
  \item \textbf{Match-Action Pipeline} Responsible for the verification of the packet checksum, optionally for the computation of metrics, for making a routing decision, and for recomputing the checksum with the updated header field values.
  \item \textbf{Deparser} Responsible for the reassembly of the headers with possibly updated fields and checksums.
\end{enumerate}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{fig/1-p4/fig/parts}
  \caption{The three basic blocs of a \pfour/ program}
  \label{fig:p4-parts}
\end{figure}

In code, these separate parts are defined independently. The switch is fully defined at the end of the \pfour/ file with the code shown in Listing \ref{code:p4-decl}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/switch-def.tex}
  \caption{Components of a \pfour/ switch.}
  \label{code:p4-decl}
\end{code}

\subsection{Parser}
The role of the parser is to specify what information to extract from the header of an incoming packet. It is defined as a finite state machine with three predefined states \textit{start}, \textit{accept}, and \textit{reject}. This part of the code simply starts at the \textit{start} state, parses some data, and transitions to another state  (or itself) based on what data was extracted. In order to know how to parse the headers, the parser relies on their definition as a derived \texttt{header} type. As an example, the definition of the Ethernet header can be found in Listing \ref{code:a-p4-derived-types} in the Appendix.

Listing \ref{code:p4-parser} shows an example of a parser corresponding to the state machine shown in Figure \ref{fig:p4-parser}.

\begin{figure}[H]
\centering
\input{fig/1-p4/tikz/parser.tex}
\caption{State diagram of the parser.}
\label{fig:p4-parser}
\end{figure}

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/parser.tex}
  \caption{Implementation of the parser in \pfour/.}
  \label{code:p4-parser}
\end{code}

\subsection{Match-Action Pipeline}
The match action pipeline is where the core router functionality lies. With the information contained in the headers that were parsed, it can verify the integrity of the packet using the checksum, make a routing decision, compute metrics, and finally recompute the checksum of the various headers if need be.

This stage is composed of sub-parts which are:
\begin{enumerate}
  \item \textbf{Checksum Verifier} Responsible for computing the packet checksum and comparing it to the checksum received. It can drop the packet if there is a mismatch.
  \item \textbf{Ingress \& egress pipeline} Responsible for the metrics computations and routing decision.
  \item \textbf{Checksum Computer} Responsible for the computation of the updated checksum when header fields are altered.
\end{enumerate}

This stage is named \textit{match-action} because it is constructed around the concept of assigning actions to patterns in the headers by matching tables or simply using control flow statements. 

As a first example, in a simple IP network with four hosts connected to the \pfour/ switch as shown in Figure \ref{fig:p4-four-hosts}, the switch simply looks up a table containing the mapping from the destination IP to the port number the switch must push the packet from for the packet to reach its destination. 

The table can be populated automatically using a controller, manually by issuing commands via the control command line interface, by using command files for the control plane of the switch to ingest at startup, or a distributed protocol such as ARP which is not described here. 

Listing \ref{code:p4-four-hosts-ip} shows the Ingress pipeline implementing the packet switching. Listing \ref{code:p4-four-hosts-table-cmds} shows the command files used to populate the table, and Table \ref{tab:p4-four-hosts-ip} shows the content of the table \texttt{ipv4\_fwd} used in Listing \ref{code:p4-four-hosts-ip}.

For this simple example, there is no need for an egress pipeline to be defined. It can however prove useful to conceptually separate two distinct parts of the processing. For instance to handle link failures separate from the routing decision or to perform rate limiting.

\begin{figure}[H]
  \centering
  \input{fig/1-p4/tikz/four-hosts.tex}
  \caption{Simple example network.}
  \label{fig:p4-four-hosts}
\end{figure}

\begin{table}[H]
  \centering
  \input{fig/1-p4/table/ipv4-fwd.tex}
  \caption{Contents of \texttt{ipv4\_fwd}.}
  \label{tab:p4-four-hosts-ip}
\end{table}

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/four-hosts-ip.tex}
  \caption{Implementation of ingress pipeline for a simple packet switcher in \pfour/.}
  \label{code:p4-four-hosts-ip}
\end{code}

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/ipv4-fwd-cmd.tex}
  \caption{Command file used to populate \texttt{ipv4\_fwd}}
  \label{code:p4-four-hosts-table-cmds}
\end{code}



\subsection{Deparser}
The role of the deparser is simply to emit all relevant headers with the relevant data. Listing \ref{code:p4-deparser} shows an instantiation of a deparser capable of creating the Ethernet, IPv4, TCP, and UDP headers. It is important to note only the headers marked as valid are emitted. The headers are valid when 
\begin{itemize}
  \item They were parsed in the Parser and (possibly) have some values changed in their fields at the match-action pipeline stage;
  \item They were created from nothing at the match-action pipeline stage and were explicitly set as valid.
\end{itemize}

\begin{code}
  \centering
  \input{fig/1-p4/code/deparser.tex}
  \caption{Implementation of a simple deparser.}
  \label{code:p4-deparser}
\end{code}


%% Environment %%


\section{\pfour/ Environment}
\pfour/ introduces two concepts to describe the environment where the language can be used to program a network device. The \pfour/ target and the \pfour/ architecture. The target represents a specific device the \pfour/ code is to be run on.  It is the hardware or software entity being programmed. Provided a valid program for this target, the compiler produces target specific binaries.

\subsection{\pfour/ Targets}
The challenge for the \pfour/ targets is to allow for field reprogrammable switches while maintaining a reasonable performance and cost. 

\subsubsection*{Fixed-Function switches}
Fixed-function switches achieve forwarding speed on the order of the Terabit per second \cite{reprog-switches}. In order to achieve this rate, the switch is constructed with a parser/deparser pair along with a sequence of processing stages. Each stage is configured for a specific usage as shown in the example in Figure \ref{fig:p4-fixed-func-sw}.

\begin{figure}[H]
  \centering
  \includegraphics[width=.6\textwidth]{fig/1-p4/fig/fixed-func.png}
  \caption{Example of the construction of a fixed-function switch.}
  \label{fig:p4-fixed-func-sw}
\end{figure}

This specificity makes it impossible to add new tables, change the match type, change the table sizes, or to support new headers or actions.

\subsubsection*{Reprogrammable switches}
In order to allow for the functionality to be changed while maintaining a similar performance as the fixed-function switches, a new approach is necessary. Both the parser and pipeline stages are defined through the Reprogrammable Match Table (RMT) model. 

Conceptually, the pipeline consists of a sequence of generic processing units which process packets one after the other simultaneously as shown in Figure \ref{fig:p4-concept-pipeline}. Each stage is directly connected to an appropriate amount of memory \cite{reprog-switches}.

\begin{figure}[H]
  \centering
  \includegraphics[width=.7\textwidth]{fig/1-p4/fig/concept-pipeline.png}
  \caption{Conceptual view of the pipeline of a reprogrammable switch}
  \label{fig:p4-concept-pipeline}
\end{figure}

\paragraph*{Parser} In practice the functionality of the parser is divided into two components: the header identification and header extraction \cite{hw}. The header identification stage implements the parse graph state-machine and the header extraction extracts the chosen fields once the header is identified. They both rely on runtime information instead of hard-coded logic. This information is stored in two types of memory depending on its use. Ternary Content-Addressable Memory (TCAM) is used for ternary matches (i.e. matches with a mask) and is used by the header identification stage and Static Random-Access Memory (SRAM) is used to store state information, fields to extract, and other data necessary to store. A conceptual view of the parser is shown in Figure \ref{fig:p4-concept-parser}.

\begin{figure}[H]
  \centering
  \includegraphics[width=.7\textwidth]{fig/1-p4/fig/concept-parser.png}
  \caption{Conceptual view of the parser of a reprogrammable switch}
  \label{fig:p4-concept-parser}
\end{figure}

At compilation, the logical pipeline for the state diagram defined in \pfour/ code is transformed into a physical one. The compiler  transforms each logical stage into one or more physical stages. Each physical stage contains a dedicated SRAM and TCAM. In each stage, the RMT model relies on multiple vectorized Arithmetic Logic Units (ALUs), one per word of the header to perform actions on the result of a match.

With the RMT model, it is only approximately 15\% more expensive to build a reprogrammable switch than a fixed-function switch.

\subsection{\pfour/ Architecture}
The architecture model represents a class of targets whose capabilities are standardized to a well-defined set of operations with a clear interface. The architecture used in this course is the \texttt{v1model}. It closely resembles the Protocol Independent Switching Architecture (PISA). Figure \ref{fig:p4-v1arch} shows a graphical depiction of the \texttt{v1model}. Each different architecture has a different model, set of metadata, and available set of \texttt{extern}.

\begin{figure}[H]
  \centering
  \includegraphics[width=.65\textwidth]{fig/1-p4/fig/v1model}
  \caption{Graphical description of the \texttt{v1model}.}
  \label{fig:p4-v1arch}
\end{figure}

\subsubsection{Metadata}

Each architecture defines the metadata it supports, both the \textit{standard metadata} and the \textit{intrinsic metadata}. For the \texttt{v1model}, these are shown in Listing \ref{code:a-p4-metadata} in the Appendix.

\subsubsection{Externs}
Each architecture also defines a set of \texttt{externs}. These are black-box functions available through an interface defined in the architecture. A few examples of the available externs in the \texttt{v1model} are shown in Listing \ref{code:a-p4-externs} in the Appendix.

Externs can be used to maintain state and can be modified by both the control plane and the data plane.


%% Language spec %%


\section{\pfour/ Language}

\pfour/ is a statically typed language. Its syntax is inspired from C. Variables and constants are defined in a fashion similar to C as shown in Listing \ref{code:p4-var-const}. In \pfour/ variables \textbf{cannot} be used to maintain state between different packets. To do so, one must use stateful objects defined as \texttt{extern} or tables. The topic of state maintenance is discussed in detail in Section \ref{sec:p4-stateful-obj}.
 
\begin{code}[H]
  \centering
  \input{fig/1-p4/code/var-const.tex}
  \caption{Variable and constant declaration in \pfour/.}
  \label{code:p4-var-const}
\end{code}

\subsection{Data types}
The basic data types available in \pfour/ are shown in Table \ref{tab:p4-data-types}. Note floating point numbers and strings are not available for performance reasons.

\begin{table}[H]
  \centering
  \input{fig/1-p4/table/data-types.tex}
  \caption{Basic types in \pfour/.}
  \label{tab:p4-data-types}
\end{table}

Basic types can be composed into derived types into \texttt{header}, \texttt{header\_union}, \texttt{struct}, \texttt{tuples}, \texttt{enums}, \texttt{typedef} type specialization, \texttt{extern}, \texttt{parser}, \texttt{control}, and \texttt{package}. Listing \ref{code:a-p4-derived-types} in the Appendix shows instances of some derived types.

Structs are similar to C structs, headers are similar to structs, but contain a hidden \texttt{validity} field. This field is set to true when the header is parsed or manually when the header is constructed. 

Headers can be composed into arrays as follows \texttt{Mpls\_h[}$N$\texttt{] mpls;} which defines an array of up to $N$ MPLS headers.

\subsection{Operations}
There are only a limited set of operations available. They are shown in Table \ref{tab:p4-ops}. Note both division and modulus are not available for performance reasons.
\begin{table}[H]
  \centering
  \input{fig/1-p4/table/operations.tex}
  \caption{Available operations in \pfour/.}
  \label{tab:p4-ops}
\end{table}


\subsection{Statements}
Statements are also pretty similar to C and fairly standard with \texttt{return} and \texttt{exit} statements and \texttt{switch} and \texttt{if}-\texttt{else} blocks.

Conditions and switches can however not be used in parsers and only in control blocks. For the parser the \texttt{select} construct allows for state switching similar to a \texttt{switch} block. The parser also contains more advanced concepts such as \texttt{verify} for error handling in the parser, \texttt{lookahead} to access bits not parsed yet, and sub-parsers which function like subroutines for parsing.

\subsection{Control blocks}
Control blocks are used for the packet processing in the ingress and egress pipelines. They can contain three types of components (tables, actions, and control flow segments) as well as an \texttt{apply} block which is the entry point for the control block. 

\subsubsection{Tables}\label{sec:p4-tables}
Tables are constructs that allow state maintenance between the packets. They can only be changed by the control plane. In a nutshell, a table matches on an input and returns the action associated to the item it matched and, optionally, the parameters to pass to that action. Different matching heuristics are available. Some are listed in Table \ref{tab:p4-table-match}.

\begin{table}[H]
  \centering
  \input{fig/1-p4/table/table-match.tex}
  \caption{Some matching conditions available in \pfour/.}
  \label{tab:p4-table-match}
\end{table}

A table can be applied to a packet in a control block by using \texttt{table.apply()}. 

Checking whether the application of the table resulted in a hit is performed with \texttt{table.apply().hit}. This can be used as the condition of an \texttt{if} statement for instance.

Checking which action was executed upon the table application is performed with \texttt{ipv4\_lpm.apply().action\_run}. This value can be used as the parameter of a \texttt{switch} block as shown in Listing \ref{code:p4-action-run}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/action-run.tex}
  \caption{Switching on the action performed by a table application.}
  \label{code:p4-action-run}
\end{code}

\subsubsection{Actions}
Actions are similar to C functions, they can be called either directly in a control flow block or returned from a table. 

Actions parameter are usually directional and annotated with \texttt{in} for a read-only parameter, \texttt{out} for an uninitialized value to be written to and, \texttt{inout} which is functionally equivalent to a call by reference.

Action parameters returned from a table lookup do not have a direction since they originate from the control plane.

\subsubsection{Control flow blocks}
Control flow can be found in actions or in the \texttt{apply} section of the control block. They are essentially \texttt{switch} and \texttt{if}-\texttt{else} blocks.

\subsection{Checksum validation \& computation}
In the \texttt{v1model}, two \texttt{extern} are defined to validate and compute checksums. Their definition and use is found in Listing \ref{code:a-p4-chksm} in the Appendix.

\subsection{More advanced concepts}
Packets can also be cloned, recirculated, and sent to the control plane. 


%% Stateful Objects %%


\section{Stateful objects}\label{sec:p4-stateful-obj}

Stateful are present as tables in \pfour/ (see Section \ref{sec:p4-tables}) and as several types of \texttt{extern} primitives defined in \texttt{v1model} such as registers, counters, meters, etc. They can be used to maintain state across different packets to measure the inter-packet gap for instance or to implement fancy probabilistic data structures as discussed in Section \ref{sec:prob-ds}.

\subsection{Registers}
Registers are used to store arbitrary data of a certain type. They are defined as arrays. They can be used to gather metrics or to maintain a state across packets. Their definition and usage is shown in Listing \ref{code:p4-regs}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/regs.tex}
  \caption{Register definition and usage.}
  \label{code:p4-regs}
\end{code}

Two examples of their usage for computing inter-packet gaps and for implementing a stateful firewall are given in Listings \ref{code:a-p4-inter-pkt-gap} and \ref{code:a-p4-stateful-fw} in the appendix.

\subsection{Counters}
Counters are special types of registers which are write-only from the data plane. The control plane can then retrieve the counts when necessary. They are also defined as arrays with an index. There are three types of counters: bytes, packets, and bytes and packets which respectively count the number of bytes, packets, and both. They are defined and used as shown in Listing \ref{code:p4-ctr}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/ctr.tex}
  \caption{Counter definition and usage.}
  \label{code:p4-ctr}
\end{code}

A slightly different type of counter also exist. \textit{Direct counters} are counters which can be directly attached to tables. They have a cell per table entry and automatically count upon matching the table entry. Their usage is shown in Listing \ref{code:p4-direct-ctr}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/direct-ctr.tex}
  \caption{Direct counter definition and usage.}
  \label{code:p4-direct-ctr}
\end{code}

\subsection{Meters}
Meters are mostly used for rate limiting. They measure the rate of traffic, either in bytes, packets, or packets and bytes. The control plane can set the bounds according to which the traffic is classified. Typically, two bounds are used: the peak information rate (PIR), and the committed information rate (CIR). A flow exceeding none of the rates is classified as green, one exceeding the CIR but not the PIR is classified as orange, and one exceeding both is classified as red. Using the color of the traffic, the switch can subsequently police or shape the traffic. The definition and usage of meters is shown in Listing \ref{code:p4-meter}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/meter.tex}
  \caption{Meter definition and usage.}
  \label{code:p4-meter}
\end{code}

Analogous to direct counters, direct meters associated to tables can also be defined as shown in Listing \ref{code:p4-direct-meter}.

\begin{code}[H]
  \centering
  \input{fig/1-p4/code/direct-meter.tex}
  \caption{Direct meter definition and usage.}
  \label{code:p4-direct-meter}
\end{code}

\subsection{Access Control for stateful objects}
Table \ref{tab:p4-access-ctrl} shows what interactions are allowed with tables, registers, counters, and meters from both the data plane and the control plane.

\begin{table}[H]
  \centering
  \input{fig/1-p4/table/access-ctrl.tex}
  \caption{Access control for stateful objects}
  \label{tab:p4-access-ctrl}
\end{table}

