\setcounter{figure}{0}
\setcounter{code}{0}
\setcounter{table}{0}
\setcounter{eqn}{0}

\chapter{Load Balancing}
Shortest path routing, one of the simple heuristics according to which traffic can be routed through a network, does not guarantee good link utilization. In this chapter, techniques used to optimize the network utilization are introduced. 

For instance, in the topology shown on Figure \ref{fig:lb-short-path} when using shortest-path routing algorithms like OSPF, links on the path R3-R6-R7-R5 are not utilized at all for traffic between the sources and the destination. Assuming all the links have the same capacity, this halves the network throughput by leaving half links unused.

\begin{figure}[H]
  \centering
  \input{fig/2-load_balancing/tikz/shortest-path.tex}
  \caption{Example topology where links are not used when using shortest-path routing like OSPF.}
  \label{fig:lb-short-path}
\end{figure}

\section{Basic concepts}
Techniques used to balance the load across the network have two main characteristics: the granularity at which the traffic is split at nodes, and the congestion awareness of the technique.

\subsection{Granularity} 
The granularity with which the traffic can be split at nodes can take three main different forms: 

\begin{itemize}
  \item \textbf{Packet splitting} where each packet is randomly sent to one of the possible next hops. \\
  Packet splitting can be seriously detrimental to the performance of TCP traffic because packets can be reordered at the destination triggering TCP congestion avoidance mechanism.
  \item \textbf{Flow splitting} where each flow, characterized with its source and destination IPs, source and destination ports, protocol. \\
  This method avoids the packet reordering problem posed by Packet splitting, however it has a very coarse granularity making it more subject to hash collisions. These can lead to suboptimal link utilization.
  \item \textbf{Flowlet splitting} where each flowlet corresponds to a burst of TCP flow or UDP traffic. \\
  This method is a hybrid between Packet and Flow traffic splitting. It avoids the TCP packet reordering of the packet splitting approach while allowing for a finer granularity, leading to a higher probability of a better global network utilization. 
\end{itemize}

\subsection{Congestion awareness} 
Congestion awareness, if implemented in the protocol, can be of two forms local and global congestion awareness. 

\subsubsection{Local congestion awareness}
With local congestion awareness, each node independently tracks the utilization of their output links for each destination. While this idea seems good, it is counter-productive in cases where the network or the load is asymmetric. 

For instance in a network such as shown in Figure \ref{fig:lb-asym-local} with ECMP (see Section \ref{sec:lb-ecmp}), the traffic is on average equally split between the R3-R2 and R3-R4 links, leading to a utilization of R3-R2 of 10Gbps and a utilization of R3-R4 of 20Gbps, leading to a global network throughput of 60Gbps. 

\begin{figure}[H]
  \centering
  \input{fig/2-load_balancing/tikz/asym-local.tex}
  \caption{Asymmetric case where local congestion awareness is counterproductive}
  \label{fig:lb-asym-local}
\end{figure}

With a local congestion awareness where the utilization of the output queues are monitored and balanced, since the congestion happens on the link R2-R5 which is out of scope for R3, R3 will balance the utilization of the links R3-R2 and R3-R4 leading both being used only for 10 Gbps. In this case, the global network throughput is 50 Gbps, which is lower than congestion unaware ECMP (see Section \ref{sec:lb-ecmp}) routing. 

This example shows local congestion is not a good solution.

\section{Equal Cost Multipath (ECMP) Routing} \label{sec:lb-ecmp}
Equal Cost Multipath (ECMP) Routing \cite{rfc-ospfv2} is a congestion agnostic protocol which splits the traffic by flows and send them randomly to equal cost paths to their destinations. It is usually implemented in two stages where all destinations for which there are multiple equal cost paths are grouped together and given a unique group ID. This ID is then used as a proxy for the destination and used to match the possible next hops for the packet.

Listings \ref{code:a-lb-ecmp-p1} and \ref{code:a-lb-ecmp-p2} in the Appendix shows the ingress pipeline of an implementation of ECMP in \pfour/. The contents of the tables are shown in the Appendix in Listing \ref{code:a-lb-ecmp-cmd} and made for the network topology shown in Figure \ref{fig:lb-ecmp-network}.

\begin{figure}[H]
  \centering
  \input{fig/2-load_balancing/tikz/ecmp-network.tex}
  \caption{Example network where ECMP is used to balance the traffic load.}
  \label{fig:lb-ecmp-network}
\end{figure}

Since ECMP is congestion agnostic, it has no built-in mechanism to handle link congestion. Moreover, since it splits traffic by flows, this coarse granularity make it susceptible for uneven splitting of the traffic across the different paths. Thus, while this protocol has the advantage of being relatively simple and easy to implement, it is not (and should not be) used in networks experiencing high load or where link utilization is important.

\section{CONGA: distributed congestion-aware load balancing for datacenters}
In datacenters, top-of-the-rack switches, or leaves switches are interconnected through core switches. The leaves switches are used to send traffic from one of the server rack to another. In CONGA \cite{conga}, each leaf switch monitors the congestion to all the other leaf switches in the datacenter. Traffic is split into flowlets and when traffic needs to be sent to another leaf, the leaf switch simply greedily chooses the path with the least congestion.

The congestion is measured using fast (millisecond order) feedback loops directly in the data plane. All devices – including the core switches – on the network continually monitor the link utilization. All leaf switches also maintain a table containing a congestion metric for each of the paths to each of the other leaves. 

The congestion tables are filled by adding a small header to each packet sent to another leaf switch. The header contains the path the traffic is sent through and information about the congestion experienced by the packet. First initialized at 0, the congestion metric is updated at each switch it traverses, and the value is replaced by the maximum of the received congestion metric and the congestion experienced at the link. At arrival, the metric will be the maximum of the link congestion experienced along the path. For this information to make it back to the source leaf switch, the congestion metric is then piggybacked onto some other traffic going to the source. Upon receiving this update, the source leaf switch updates its table.

At the source, flowlets are routed through the link experiencing the least congestion.

\section{LetFlow}
LetFlow \cite{letflow} is a simple congestion-oblivious protocol whose performance is close to the performance of CONGA. The simple idea behind LetFlow is to simply randomly assign flowlets to available paths. It is essentially flowlet ECMP.

Despite not gathering any metric about the congestion, LetFlow is in some regard congestion aware because flowlets are elastic and adapt their size based on the congestion experienced onto a path. On an uncongested path, the flowlets will be bigger and smaller for congested path because of the congestion control present at the transport layer (TCP/QUIC, etc.). Thus, the flowlet sizes implicitly encode path congestion information which in turn determines the amount of traffic sent through a path.

An implementation the ingress pipeline of LetFlow for the same network topology (Figure \ref{fig:lb-ecmp-network}) as for the ECMP implementation is given in the Appendix in Listing \ref{code:a-lb-letflow-p1} and \ref{code:a-lb-letflow-p1} and the commands to populate the tables are shown in the appendix in Listing \ref{code:a-lb-letflow-cmd}.


