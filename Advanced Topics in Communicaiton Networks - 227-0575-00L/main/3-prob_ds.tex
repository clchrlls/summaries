\setcounter{figure}{0}
\setcounter{code}{0}
\setcounter{table}{0}
\setcounter{eqn}{0}

\chapter{Probabilistic Data Structures}\label{sec:prob-ds}

It is often necessary to compute statistics about the traffic during runtime. These statistics can be used to detect attacks, rate limit certain users, or to monitor the network usage by customers of the network. When dealing with non-trivial amount of data and when requiring a high speed for statistic computation and retrieval, it is often not possible to compute actual and accurate statistics. This is why it is often necessary to resort to probabilistic data structures which allow for efficient computation of statistics with less, but predictable, accuracy.

Several of these probabilistic data structures are described here, each for a special purpose: Bloom filters are used to approximate set membership (see Section \ref{sec:pds-bloom}) and CountMin sketches which approximate item frequency (see Section \ref{sec:pds-cm-sketch}).

\section{Bloom filters}\label{sec:pds-bloom}
Bloom filters are used to approximate the set membership property. In a deterministic implementation of set membership, the result is deterministic and always correct, but the number of operation depends on the size of the set and is thus no constant. This is not ideal for application where time is a scarce resource such as network devices. Instead, by relaxing the output of the set membership to allow for a predictable rate of false positives, it is possible to implement set membership with a constant runtime of $\mathcal{O}(k)$ and constant memory footprint of $\mathcal{O}(M)$.

Bloom filters consist of a bit vector of size $M$ whose entries are initialized to 0 and $k$ hash functions. Adding an element is performed by hashing the element with the $k$ hash functions modulo $M$ and setting all the corresponding bit vector cells to 1. Testing set membership is performed similarly where set membership is assumed when all cells corresponding to the $k$ hash functions modulo $M$ contain the value 1, otherwise the element is not part of the set. Listing \ref{code:pds-bloom-pseudocode} shows the pseudocode for the Bloom filter.

\begin{code}[H]
  \centering
  \input{fig/3-prob_ds/code/bloom.tex}
  \caption{Pseudocode for a Bloom filter.}
  \label{code:pds-bloom-pseudocode}
\end{code}

Bloom filters guarantee a false negative (i.e. set membership is false when the element is in the set) cannot occur. However, false positives can occur. For instance in the simplest case if there are two elements $e_1$ and $e_2$ whose hash is the same for all $k$ hash functions, the set containing only $e_1$ will seem to also contain $e_2$. With only one element in the set, the probability of false positive is rather low ($M^{-k}$), but it grows quickly with the number of the elements of the set.

For a set containing $N$ elements represented with a Bloom filter with a bit vector of length $M$ and $k$ perfectly random hash functions, the probability of a false positive occurring is shown in Equation \ref{eq:pds-bloom-fp}.

\begin{eqn}[H]
  \input{fig/3-prob_ds/eqn/bloom-fp.tex}
  \caption{False positive rate of a Bloom filter.}
  \label{eq:pds-bloom-fp}
\end{eqn}

Given a size $M$ and an expected number of elements $N$, the optimal number of hash functions can be found by finding the minimum of the false positive probability function in Equation \ref{eq:pds-bloom-fp} or its approximation with respect to $k$ using calculus. The equation for the optimal number of hash functions $\hat{k}$ is shown in Equation \ref{eq:pds-bloom-opt-k}.

\begin{eqn}[H]
  \input{fig/3-prob_ds/eqn/bloom-opt-k.tex}
  \caption{Optimal number of hash functions $\hat{k}$.}
  \label{eq:pds-bloom-opt-k}
\end{eqn}

\subsection{\pfour/ Bloom filter implementation}
Bloom filters can be implemented in \pfour/ and in the \texttt{v1model} using registers and available hash functions. An example of an implementation is shown in the appendix in Listing \ref{code:a-pds-bloom-p4}. The interface of hash functions and the list of available hash functions in the \texttt{v1model} is shown in Listing \ref{code:pds-hashes}.

\begin{code}[H]
  \centering
  \input{fig/3-prob_ds/code/hashes.tex}
  \caption{Hash interface and available hash functions in the \texttt{v1model}.}
  \label{code:pds-hashes}
\end{code}

When used in practice, Bloom filters must be periodically reset in order to avoid too high false positive rates. In order to avoid the filter to be offline while resetting, two filters are often used in practice where with one being used while the other is being reset and are swapped when needed.

Several extensions to Bloom filters exist among which counting Bloom filters that allow for deletions and Invertible bloom filters that allow for the listing of their contents.

\section{CountMin Sketches}\label{sec:pds-cm-sketch}
Bloom filter help to probabilistically determine set membership. CountMin Sketches on the other hand provide an approximation of the frequency of an element in a stream. As for the bloom filter, an exact algorithm providing the frequency of an element in a stream requires linear space and time. For instance, a stream with $n$ distinct elements requires $n$ counters thus a memory footprint of $\mathcal{O}(kM)$ and a runtime of $\mathcal{O}(k)$.

Here again, a constant space and time algorithm exists to retrieve the frequency of an element if we allow for a probabilistically predictable error in the count. CountMin Sketches are designed to have a predictable L1 error bounds (Equation \ref{eqn:pds-l1-bound}) on the frequency of elements. Here again, hash collisions cause over-counting, but under counting is impossible.

\begin{eqn}[H]
  \centering
  \input{fig/3-prob_ds/eqn/l1-bound.tex}
  \caption{L1 error probability bound}
  \label{eqn:pds-l1-bound}
\end{eqn}

Where $\hat{x}_i$ is the estimated frequency of element $i$, $x_i$ its actual frequency, $\Vert\mathbf{x}\Vert_{1}$ the sum of frequencies. Meaning that the probability that the error between the actual and estimated exceeding $\varepsilon\Vert\mathbf{x}\Vert_{1}$ is not greater than $\delta$.

A CountMin Sketch is composed of $k$ hash functions and $k$ arrays of $M$ integer entries. When adding an element to the count, the $k$ hash functions are applied modulo $M$ and the counters at the indices corresponding to the outputs of the $k$ hash functions in each of the $k$ arrays are incremented by one. In order to retrieve the count for an element, the element is hashed by each of the $k$ hash functions modulo $M$ and the minimum of the corresponding counters in the $k$ arrays are taken as the count.

Given $\varepsilon$ and $\delta$ for the error bound, the number of hash functions and arrays $k$ and the number of counters per array $M$ can be chosen with the formulas shown in Equation \ref{eqn:psd-opti-cnsts-cm-sketch}.

\begin{eqn}[H]
  \centering
  \input{fig/3-prob_ds/eqn/opti-cnsts.tex}
  \caption{Formulas to determine CountMin Sketch parameters given $\varepsilon$ and $\delta$}
  \label{eqn:psd-opti-cnsts-cm-sketch}
\end{eqn}

However, CountMin Sketches have their limitations, for instance, when the number of occurrences of elements is unbalanced, the error will favor element with a higher occurrence. For instance with $\varepsilon = 10^{-2}$, $\Vert \mathbf{x} \Vert_1 = 10^4$ ($\Rightarrow \varepsilon\Vert \mathbf{x} \Vert_1 = 10^2$) and two flows $x_a$ and $x_b$, the error relative to the overall size of the stream is $1\%$ whereas the errors relative to each of the flow sizes are $10\%$ for flow $x_a$ and $200\%$ for stream $x_b$. CountMin sketches are also unable to handle rare events, causality, and patterns.
