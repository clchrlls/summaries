\setcounter{figure}{0}
\setcounter{code}{0}
\setcounter{table}{0}
\setcounter{eqn}{0}

\chapter{RSVP}

\section{Multiprotocol label switching (MPLS)}
In a nutshell, the idea behind Multiprotocol label switching (MPLS) is for routers at the edge of the network, Ingress Label Edge Routers (Ingress LERs) in the MPLS jargon, to assign a label to packets entering the network. Then when packets reach subsequent routers in the network called Label Switching Routers (LSRs), only the label the packets carry and their ingress port is considered for forwarding the packets. Finally, when the packets leave the network, the routers at the edge of the network called Egress Label Edge Routers (Egress LERs) strip packets of their label before sending them outside the network.

The label used in MPLS takes the form of an extra MPLS header stack added to the forwarded packets. The MPLS header format is described in Listing \ref{code:rsvp-mpls-header}.

\begin{code}[H]
  \centering
  \input{fig/4-rsvp/code/mpls-header.tex}
  \caption{MPLS header content}
  \label{code:rsvp-mpls-header}
\end{code}


\subsection{Label Switching Routers (LSRs)}
Upon reception of a packet containing a MPLS header, the LSR can perform three operations on the labelled packet as allowed by the MPLS data plane. These operations and their effect are listed in Table \ref{tab:rsvp-mpls-ops}.

\begin{table}[H]
  \centering
  \input{fig/4-rsvp/table/mpls-ops.tex}
  \caption{Operations allowed on MPLS labelled packets}
  \label{tab:rsvp-mpls-ops}
\end{table}

LSRs forward traffic by applying these operations depending on the content of their label forwarding tables. These tables match  the label on the top of the header stack and contain two values: the next hop the packet is to be sent to, and the operation performed on the header stack (\texttt{PUSH}, \texttt{SWAP}, or \texttt{POP}). Typically, if the next hop is the LSR itself, this indicates that the LSR is the final destination and that the packet must then be forwarded as an IP packet after stripping the packet of the MPLS header.

There are multiple advantages of label switching comparing to regular IP longest prefix matching. For instance, the forwarding table the LSRs need to maintain in order to forward packets is significantly smaller than the entire BGP forwarding table because it typically contains a number of entries of the order of the number of exit nodes for the network, not the entire list of BGP prefixes. This makes it significantly faster to update the tables after a link failure for instance. Moreover, since the labels are matched exactly, not with a longest prefix match, the matching process is also significantly more efficient.

\subsection{Ingress Label Edge Routers (Ingress LERs)}
The Ingress Label Edge Routers (Ingress LERs) are routers susceptible to receive traffic from outside the network. They are thus responsible to transform the IP routed traffic into label routed traffic. 

In principle, the way Ingress LERs perform this transformation is to divide all the set of possible incoming packets into several Forward Equivalency Classes (FECs). A FEC is a set of IP packets that are forwarded in the same manner in the network. For instance, these can be all packets forwarded to the same prefix or all packets sent to the same BGP next hop. Then a single label is associated to each of the FECs.

\subsection{Label Switched Paths (LSPs)}
Label switching paths are paths followed by a labelled packet starting at an Ingress LER and ending at an Egress LER. The establishment of the LSPs dictate the routing tables for the traffic and are needed in order for the labelled traffic to be routed through the network.

LSPs can be aggregated together into larger LSPs. Technically this is achieved by stacking an extra header on top of the existing MPLS header. Figure \ref{fig:net-meta-lsp} shows a part of a network where meta LSPs are used to aggregate traffic 1 and 2 so R2 only needs a small amount of entries in its label forwarding table to forward the packets. Listing \ref{code:tab-meta-lsp} shows the command files to fill the relevant label forwarding entries of the label forwarding tables for R1, R3, and R3.

\begin{figure}[H]
  \centering
  \input{fig/4-rsvp/tikz/net-meta-lsp.tex}
  \caption{Part of a network where LSPs are aggregated}
  \label{fig:net-meta-lsp}
\end{figure}

\begin{code}[H]
  \centering
  \input{fig/4-rsvp/code/tab-meta-lsp.tex}
  \caption{Commands to fill forwarding tables}
  \label{code:tab-meta-lsp}
\end{code}


A simple way of establishing LSPs is to assign one label to each source-destination pair in the network, yielding $\mathcal{O}(N^2)$ LSPs. Other less straight-forward ways to distribute FEC-labels mapping is using the Label Distribution Protocol (LDP) or use the LSPs as circuits and reserving circuits using RSVP-TE for instance (see Section \ref{sec:rsvp-te}). Another way is to piggyback the assignation of labels onto messages of other preexisting protocols such as BGP.

\section{Constrained path selection}
Two main categories of constraints exist: additive/multiplicative and concave constraints. 

Additive and multiplicative path constraints (e.g. the greatest bandwidth, the least delay, the least number of hops, etc.) are essentially the same because multiplicative constrained optimization can be transformed into additive constrained optimization by taking the log. Solving for one additive or multiplicative constraint is performed using Dijkstra's shortest path algorithm. However, for more than one constraint, the problem is NP-Complete. Algorithms approximating the solution exist, but no deterministic algorithm can solve more than one additive or multiplicative constraint in polynomial time.

Concave constraints (e.g. all routers must be located in Switzerland, etc.), are relatively easy to deal with since solving for them only involve removing the edges or vertices from the graph to be optimized.

The combination of additive or multiplicative constraints and concave constraints is easy to performed in two steps, first solve for the concave constraints, then solve the additive or multiplicative constraint(s) on the resulting graph.

\section{RSVP-TE}\label{sec:rsvp-te}
RSVP-TE is based on the Resource Reservation Protocol (RSVP), which is used to reserve resources in the network be it capacity, path, etc. The RVSP protocol is based on two messages:

\begin{itemize}
  \item \texttt{PATH} messages are used to reserve resources in the network.
  \item \texttt{RESV} messages are used to acknowledge the reservation of the desired resource.
\end{itemize}

When extended to MPLS, RSVP is called RSVP-TE. With RSVP-TE the Ingress LER sends \texttt{PATH} messages towards the Egress LSR. The contents of the \texttt{PATH} messages and their use is shown in Table \ref{tab:rsvp-path-content}.

\begin{table}[H]
  \centering
  \input{fig/4-rsvp/table/path-content.tex}
  \caption{Contents of the \texttt{PATH} message}
  \label{tab:rsvp-path-content}
\end{table}

When a \texttt{PATH} message reaches an LSR, the LSR checks whether the resource can be allocated, if not the \texttt{PATH} packet is dropped. Otherwise, it updates the \texttt{Previous Host} field of the \texttt{PATH} message with its identifier and sends it on its way to the destination. It also temporarily saves the information about the LSP to be established. This information contains an identifier, the resource to reserve, and the received \texttt{IP Previous host} field of the \texttt{PATH} message.

If the resource could be allocated in all the LSRs on the path and the \texttt{PATH} message reaches the Egress LSR, it checks whether the resource can be allocated. If it cannot, the packet is simply dropped. Otherwise, the Egress LSR reserves the resource, updates its forwarding table and generates a \texttt{RESV} it sends back to the \texttt{IP Previous host} it received in the \texttt{PATH} message it received. The contents of the \texttt{RESV} message is explained in Table \ref{tab:rsvp-resv-content}.

\begin{table}[H]
  \centering
  \input{fig/4-rsvp/table/resv-content.tex}
  \caption{Contents of the \texttt{RESV} message}
  \label{tab:rsvp-resv-content}
\end{table}

Upon receiving the \texttt{RESV} message, the LSRs on the path commit to the reservation, update their forwarding tables with the label received in the \texttt{RESV} message, generate their own label, update the fields in the \texttt{RESV} message and forward the message to the \texttt{IP Previous Destination} associated to the LSP it stored when receiving the \texttt{PATH} message. This ensures the path taken by the \texttt{PATH} message and the \texttt{RESV} message is the same path.

When receiving the \texttt{RESV} message, the Ingress LER commits to the reservation, updates its forwarding table with the label expected by the next hop in the LSP and the LSP is established.
 
If at any point in the travel of the \texttt{PATH} message along the to-be-established LSP there are not enough resource available, the \texttt{PATH} message never reaches the Egress LER, thus the resource are never actually reserved and after the timeout, the LSR purge clears their storage and the Ingress LER is implicitly informed the LSP cannot be established.

Note it is also possible for the Ingress LER to define a specific path for the LSP establishment using the Explicit Route Object (ERO). In this case both the \texttt{PATH} message and the \texttt{RESV} message (if it is ever generated) use the path specified by the Ingress LER.

\section{In practice}
It is extremely impractical to use RSVP-TE for each and every flow in the network because of the overhead it generates. In  practice a mix of RSVP-TE and of Label Distribution Protocol (LDP) is used. LDP is a simple protocol which used assigns labels according to the IGP shortest path. 

In practice the LDP LSPs are used to provide basic connectivity and, on top of that, some additional LSPs are defined for specific applications or to apply additional constraints to some flows.