\setcounter{figure}{0}
\setcounter{code}{0}
\setcounter{table}{0}
\setcounter{eqn}{0}

\chapter{Quality of Service (QoS)}
A Quality of Service (QoS) capable router is a router capable of treating different type of traffic with differently. For instance, Voice over IP (VoIP) traffic should be forwarded with less delay than Video on Demand (VOD) traffic because it suffers more from delays than VOD traffic.

The functionality of a QoS capable router is often split into four stages shown in Figure \ref{fig:qos-stages}:  classification, shaping and policing, buffer acceptance, and scheduling. These four aspects are discussed in the next sections.

\begin{figure}[H]
  \centering
  \includegraphics[width=.8\textwidth]{fig/5-qos/fig/stages.png}
  \caption{Stages of a QoS capable router}
  \label{fig:qos-stages}
\end{figure}

\section{Scheduling}
Scheduling is used to determine in what order the outbound packets are emitted on the output interface. In some cases it might for instance interesting to try to minimize the delay of certain type of traffic such as VoIP. In order to do so, several types of queues can be used. Two important properties of schedulers are the work-preservation property and starvation property. 

A queue is said to be work-preserving if the queue never leaves the output interface idle when there is at least one packet waiting to be transmitted on this interface. Conversely, a queue is non-work-preserving if it is possible an interface to be left idle while there is a packet waiting to be emitted on that interface. In general, depending on how the scheduler handles state, it is possible it becomes non-work-preserving.

Starvation occurs in some types of scheduling. It consists of a packet or a set of packets continually being denied processing. Conversely, when it is impossible for a scheduler to continuously deny a packet the access to the output interface it is said to exhibit starvation.

\subsection{First In First Out (FIFO)}
A First In First Out queue (FIFO) is the simplest scheduling strategy. It simply processes packets in the order they enter the queue. There is no prioritization nor differentiation. A FIFO exhibits no starvation and is work-preserving, however it is unable to prioritize any type of flows and guarantees no fairness between the flows.

\subsection{Priority Queuing (PQ)}
Priority queues consist in a set of FIFO which are all assigned a priority. Different types of traffic are assigned different priorities and the packets in the higher priority queues are always served before the less prioritized types of traffic. This still simple scheduling mechanism is work-preserving, but can exhibit starvation. If the higher priority traffic rate exceeds or meets the rate of the output interface, the lower priority traffic will never be served and will be dropped.

\subsection{(Weighted) Fair Queuing (WFQ/FQ)}
Fair queuing, is also built using multiple FIFO queues. Each flow has a dedicated queue and the queues are served in turn using a round-robin algorithm. FQ require a lot of resources because it needs a queue per flow and are not work-preserving in their simplest implementation. However, FQ provide isolation, meaning misbehaving flows do not impact others which means no starvation can occur. It also approximates Max-Min Fairness. 

\begin{definition}[Max-Min Fairness]
  Given a set of $N$ bandwidth requests $\{r_i\}_{i=1}^N$, a total available bandwidth $C$, the Max-Min allocations $\{a_i\}_{i=1}^N$ are defined for all
  \[
    a_i = \min\left(f,r_i\right)
  \]
  Where $f$ is the unique value such that $\sum_{i=1}^N a_i = C$.
\end{definition}

In other words, Max-Min fairness guarantees that if $\exists \, i \in \{1,\dots,N\}$ s.t. $ a_i < r_i$ then $\forall \,j \in \{1,\dots,N\} \ a_j \leq a_i $ or, in English, if there is a flow not being allocated everything it required, then all other flows were allocated the same or a smaller amount.

% Listing \ref{code:qos-maxmin} displays the pseudocode for the algorithm computing the Max-Min allocations. 

% \begin{code}[H]
%   \centering
%   \input{fig/5-qos/code/maxmin.tex}
%   \caption{Pseudocode for the Max-Min algorithm}
%   \label{code:qos-maxmin}
% \end{code}

One of the challenges of FQ is that packets are of discrete non-constant sizes making the bandwidth shares to become uneven and hurting the fairness of the queue. Flows with smaller packets get penalized.

FQ generalizes to Weighted FQ (WFQ) in which each different stream is assigned a weight and the time spent serving each queue is proportional to its weight.

\subsection{Composite schedulers}
In practice, different types of schedulers are used together in a hierarchical fashion to accommodate all the complexity of the different types of traffic. An example of a composite queue is shown in Figure \ref{fig:qos-composite}.

\begin{figure}[H]
  \centering
  \input{fig/5-qos/tikz/composite.tex}
  \caption{An instance of a composite queue}
  \label{fig:qos-composite}
\end{figure}

\section{Buffer Acceptance}
When traffic rate exceeds the capabilities of the output interface some packets must be dropped. There are several ways of doing so. Two of them are common examples. Tail drop and Random Early Detection (RED).

\subsection{Tail Drop}
The Tail drop algorithm is the simplest of the two. It simply consists on dropping newly arrived packets in the queue when it is full. Its advantage is its simplicity, however it tends to synchronize the TCP flows. 

\subsection{Random Early Detection (RED)}
Random dropping is a more involved algorithm where an additional functionality is added between no dropping when the queue is not full and full dropping when it is. In this new case, applied when the queue is mostly full but not entirely full, packets are dropped probabilistically with a probability scaling with the size of the queue. Figure \ref{fig:qos-red-prob} shows the behavior of the queue. The $x$-axis represents the number of elements in the queue and the $y$-axis the probability with which the packets are dropped. The point at which the packets begin to be probabilistically dropped is defined by the managers of the switch and depend on its load.

\begin{figure}[H]
  \centering
  \input{fig/5-qos/tikz/red-prob.tex}
  \caption{Representation of the behavior of the RED buffer acceptance policy}
  \label{fig:qos-red-prob}
\end{figure}

While being harder to implement, RED has multiple advantages. It is more fair than tail drop since larger flows have more packets and are thus more likely to have one of them dropped with respect to smaller flows that are not impacted as much. Additionally, the random dropping avoids the TCP flows synchronization.

\section{Shaping \& Policing}
Another important component of the QoS capability of a switch is the traffic policing and shaping it can perform. In a commercial setting it can be important to be able to monitor the usage of the network by the clients and if needed to restrict it according to some policies. To shape or police the traffic, three metrics can be gathered: 
\begin{itemize}
  \item \textbf{Maximum rate} of the traffic. Monitoring this quantity is not very useful contrary to what it can seem at first glance because internet traffic is inherently bursty, the maximum rate can be much higher than the average rate. Thus monitoring and acting on this metric can be wasteful.
  \item \textbf{Average rate} of the traffic. Here again, using only the average rate is not that useful. Since traffic is bursty, it is possible that the average rate is well below the infrastructure capabilities, but to exceed it widely during bursts.
  \item \textbf{Burstiness} of the traffic. Burstiness is an important factor to consider. In the perfect setup, the sender is allowed to send traffic at a very high rate during short bursts without overwhelming the network too much.
\end{itemize}

\subsection{Token buckets}
Token buckets is a data structure specially apt to monitor traffic while accounting for both the average rate, the burstiness, and the maximum rate. It can be seen conceptually as a bucket of tokens allocated for each traffic category or flow. This bucket has a predefined constant filling rate $r$, a maximum capacity $C$, and a maximum bucket emptying rate $R$. The tokens represent data volumes. 

Whenever a packet arrives (say of 100kb), at the bucket, if the bucket is full enough ($\geq 100$kb), then the packet is transmitted and the packet size is subtracted from the bucket capacity. If the bucket is not full enough, the packet is either dropped, delayed, or marked. At all times, the bucket fills up with at the packet filling rate $r$. The bucket content never exceeds its capacity $C$. If the rate at which packets arrive exceeds $R$ they are also dropped, delayed, or marked.

To enact on the specification, three options are possible:
\begin{itemize}
  \item \textbf{Policing} All the data in excess of the specification are dropped.
  \item \textbf{Shaping} All the data in excess of the specification are delayed until they fit the specification.
  \item \textbf{Marking} All the data in excess of the specification is marked as such and given a lower priority, i.e. they are the first to be dropped if dropping packets is necessary.
\end{itemize}

\section{Classification}
Classification is the first component of the QoS pipeline. It simply consists in classifying the packets, so they are processed according to their class. This allows for the traffic to be policed, shaped, and scheduled correctly. The ideal classifier has a queue per traffic flow, but the cost of such an approach is prohibitive so, in practice, they are classified into a fixed set of categories the managers of the network define.


