\chapter{Data streams}
In many applications it is not possible to access the whole dataset in order to derive some statistics from it. Sometimes the data is streamed to the processing unit and the unit does not have the time or the power to store it to analyze it later. The data must be processed on the fly with limited memory and processing power, and the rest of the data is discarded.
\section{Streaming algorithms} The assumption made by stream algorithms is that the data is infinite, and the memory is fixed. Thus, approximation and pseudo-randomization is used. Common estimates we could want to derive from data and their memory cost necessary to derive their exact value is shown in Table \ref{tab:estimates-cost}
\begin{table}[!ht]
    \centering
    \begin{tabular}{l|l}
        Value & Memory cost \\\hline
        Count of elements &\cellcolor{green!25}$O(\log(n))$ \\
        Maximum, minimum, sum, etc. &\cellcolor{green!25}$O(1)$\\
        Average (count + sum)&\cellcolor{green!25}$O(\log(n))$ \\
        Havy hitters (most frequent values) &\cellcolor{red!25}$O(n\log(n))$\\
        Number of distinct elements &\cellcolor{red!25}$O(n\log(n))$
    \end{tabular}
    \caption{Common values estimated and their cost}
    \label{tab:estimates-cost}
\end{table}
\section{Heavy hitters} In order to get the most frequent hitter with a frequency $\theta > 0.5$, we can simply take the data points as they arrive and perform the following: each time a datapoint arrives:
\begin{itemize}
    \item If the memory is empty we keep the element and assign it a count of 1.
    \item If the memory already contains a datapoint with the same value we increment its count.
    \item If the memory already contains an element of a different value, we decrement its count and discard the new datapoint.
\end{itemize}
In the end, we look at the element in memory and, in order to reach a final count, we perform a second pass on the data to get the exact count.\par
When $\theta < 0.5$ we proceed slightly differently. Instead of directly annihilating pairs, we keep a set $K$ of distinct values along with their count. When $\vert K \vert
\geq \frac{1}{\theta}$, we decrement each value and delete those whose count hit 0. At the end, a second pass is also necessary to eliminate false positives.\par
The memory complexity is $O(\log \frac{n}{\theta})$ and the computational cost $O(n)$.
\section{Distinct elements count} Again, in order to get the exact number of distinct elements, the most efficient way to proceed is to build a hash table at memory cost of $O(n\log(n))$.
\subsection{Flajolet-martin algorithm} Instead of aiming for exact counts, FM algorithm uses hash functions as uniform random variables. This assumption is ok since, with good hash functions the output is pseudorandom. Moreover, with good hash functions : $h(x_1) = h(x_2) \Leftrightarrow x_1 = x_2$.\par
With this observation, we simply compute the hash of the incoming elements and keep a running minimum of the values of the hash. At the end, when we want to get the number of elements, since $h(\cdot)$ is assumed to be uniform on the possible values, there should be $k = \frac{H}{\min_{i} h(x_i)}$, with $H$ the highest value possible for $h(x)$, and if the hash values are normalized to the interval $[0,1]$, $k = \frac{1}{min_{i} h(x_i)}$.\par
While this estimator is unbiased, its variance is very high. In order to reduce the variance, we can use $J$ hash functions and aggregate their minima in order to lower the noise.\par
A further refinement of the algorithm is only to keep the number of leading zeros $R_j$ for each hash function $h_j(\cdot)$ instead of keeping the actual value. This reduces the computation cost and memory cost of the running minimum further.\par
When all the $R_j$ have been computed, we take their median $R$ (which is better than the average in this case) and compute $k=\frac{2^R}{0.77351}$. This method has a memory cost of $O(J)$ and a computational cost of $O(n)$.
\section{Document similarity}
In order to compute quickly an approximation of the similarity between documents by first decomposing each document into a series of shingles (or $n$-grams). With these shingles, we can estimate the similarity of the documents by computing the Jaccard index $J(A,B)$. 
\[J(A,B) = \frac{\vert A \cap B \vert }{ \vert A \cup B \vert}\]
This is still an expensive computation to make but we can approximate it by computing the sketch of each document $s(A)$ and $s(B)$. The sketch of a document $D$ is defined as the smallest value over all shingles $d$ it contains of the hash of a shingle, i.e. $s(D) = \min_{d\in D} h(d)$. With these sketches it is shown that $P(s(A) = s(B)) = J(A,B)$. Thus, by computing multiple sketches with multiple hash functions and counting the number of time they matched over the total number of sketches made by document, we approximate without bias the Jaccard index of the two documents.
\section{Dimensionality reduction} Dimensionality reduction can be used to simplify the computation of various metrics and to ease the search for nearest neighbors. The traditional exact methods are memory and computationally expensive, but we can get a good approximation of the distance between two points by using randomized dimensionality reduction. By projecting the vector onto some random subspace of a dimensionality there is a high probability that the far away points will not be closer than close points in the high dimensional space. This is guaranteed by Johnson-Lindenstrauss Lemma.
\subsection{Johnson-Lindenstrauss lemma} Given $n$ data points $\{x_i\}_{i=1}^n \subset \mathbb{R}^d$ and an error tolerance $0 < \epsilon < 1$, there is a linear function $f:\mathbb{R}^d \mapsto \mathbb{R}^{d'}$ such that:
\begin{equation*}
    (1-\epsilon)\Vert x_i - x_j \Vert^2 \leq \Vert f(x_i) - f(x_j) \Vert^2 \leq (1 + \epsilon)\Vert x_i - x_j \Vert^2 
\end{equation*}
For $d' > \frac{8\log(n)}{\epsilon^2}$

