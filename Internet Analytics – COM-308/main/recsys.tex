\chapter{Recommender systems}
Recommender systems usually fit into one of the two following categories: Collaborative filtering and content-based recommenders. Collaborative filters are content agnostic but learns from the choice users made in the past. It tries to determine the popularity of items as well as the similarity from a user to another in order to extrapolate from similar users' choices. Content-based recommenders try to find similarities between items and recommends items similar to those the user liked before. In practice, these two methods are combined to provide better recommendations.
\section{Collaborative filtering}
\subsection{Netflix competition model} This model uses a set of users $U$, a set of items $I$ and a rating function $r : U \times I \mapsto A \subset \mathbb{R} $. $A$ indicates the preference of a user for a given product. From the function, a matrix $R$ is built, it represents the weights of a bipartite graph between the users and the items. Two methods can be user to generate predictions for user-items pairs: Neighborhood methods and latent factor methods.
Neighborhood methods look at what items the user liked, finds the user that gave similar ratings and uses these ratings to predict the rating of the item for the original user. Latent factor methods assume there exists a low dimensional set of features that can reasonably model the user's preferences and that can also model the degree to which the items belong to these features.
\subsection{Performance metric} The metric we use to estimate the performance of an estimator is the root-mean-square.
\[\text{RMS} = \sqrt{\frac{1}{C}\sum_{u,i}(r_{u,i} - \hat{r_{ui}})^2}\]
Where $C$ is the number of rated pairs.
\subsection{Baseline predictor} A very simple predictor is defined by assuming there exist an average rating $\bar{r}$ over all users and items, a user bias $b_u$ and an item bias $b_i$. With these metrics we compute the estimated rating of item $i$ by user $u$ as
\[
    \hat{r}_{ui} = \bar{r} + b_u + b_i
\]
In order to compute these parameters we could use simple averages but these usually do not yield good results. Instead, we find compute $\bar{r}$ and find $b_u$ and $b_i$ as
\[\min_{b_u, b_i} \sum_{u,i \in R} (r_{u,i} - \hat{r}_{u,i})^2\]
Instead of naively solving for the good parameters, we introduce a regularization parameter $\lambda$ in order to avoid overfitting. We can estimate the value of $\lambda$ by validation.
\[ 
    (\hat{b}_u, \hat{b}_i) = \argmin{b_i, b_u} \sum (r_{u,i} - \bar{r} - b_{u} - b_i)^2 + \lambda \left( \Vert b_u \Vert + \Vert b_i \Vert\right) 
\]
In order to solve find the minimizing parameters $\hat{b}_u$ and $\hat{b}_i$, we either perform gradient descent or transform into a least squares problem $\argmin{b} \Vert Ab - c \Vert^2$. $A$ is constructed by having each of its rows representing one $(u,i)$ pair with a 1 at position $u$ and another 1 at position $u + i$. $b = [b_u \vert b_i]^T$, and $c = [r_k - \bar{r}]$.\par
If the matrix is full rank, we simply solve for $b$ with $(A^TA)^{-1}A^Tc$, if it is not, we can find the pseudoinverse $A^\dagger = U\Sigma^\dagger V^T$ by SVD by inverting all nonzero elements of $\Sigma$ in order to build $\Sigma^\dagger$. Then $b = V\Sigma^\dagger U^Tc$
\subsection{Neighborhood models} The neighborhood model presented here expands on the baseline model by adding the preference of similar users weighted by the similarity to the user we wish to predict the rating for:
\[
    \hat{r}_{ui} = \bar{r} + b_u + b_i + \frac{\sum_{v \in L_u} \text{sim}(u,v)r_{vi}}{\sum_{v \in L_u}\text{sim}(u,v)}
\] 
This method is intuitive but has a couple of drawbacks: if no other user in the set of similar users $L_u$ has rated item $i$ it is impossible to predict anything and the choice of the similarity metric hugely impacts the performance of the model.\par
Using this method it is only possible to use the similarity between the users or the items.
\subsection{Latent factor models} Here we first set a value $K$ of the number of latent factors the model is going to have. Then we assign to each user a vector $p_u \in \mathbb{R}^k$ representing the degree to which a concept appeals to the user. We do the same with the items with vectors $q_i \in \mathbb{R}^k$. Then the predicted rating is:
\[
    \hat{r_{ui}} = \bar{r} + b_u + b_i + p_u^Tq_i
\]
Here we need to find the matrices $P, Q$ that minimizes the RMS error (again with regularization to avoid overfitting):
\[
    (\hat{P}, \hat{Q}) = \argmin{P,Q} \sum_{u,i \in R} (r_{u,i} - p_u^Tq_i)^2 + \lambda (\Vert  P \Vert_F^2 + \Vert  Q \Vert_F^2 )
\]
In order to find these matrices, we either use stochastic gradient descent (SGD) or alternating least square (ALS): fix $P$ minimize for $Q$, fix $Q$ minimize for $P$, etc. SGD performs usually better except for very sparse datasets where ALS has the advantage.
\section{Content based recommenders}
Here, we analyze the items in order to derive a structure from the liking-pattern of a user on the items. The recommender will recommend items similar to those the user already likes.
\subsection{Vector space model} In order to represent text, the documents are turned into a vector of the occurrences of the words it contains. Each entry represents the number of times the word corresponding to the position of the entry appears in the document. This method ignores the order of words and is thus irreversible. The vectors are usually very sparse.
\subsection{TF-IDF} This method allows to estimate the importance of a word in a given document from a corpus. For instance, if the corpus is cake recipes, flour, which is common to nearly all recipes, is not very important event if it appears with a large frequency, whereas if only one recipe mentions lemon a lot, the word lemon will be very important to distinguish the recipes. In order to do so, two measures are computed : the term frequency (TF) and the inverse document frequency IDF. TF is computed for each document-term pair $(d,t)$ and IDF for each term $t$.
\begin{align*}
    \text{TF}_{d,t} &= \frac{f_{d,t}}{\max_{\tilde{t}} f_{d,\tilde{t}}}\\
    \text{IDF}_{t} &= \log\left( \frac{n_t}{N} \right)
\end{align*}
Where $f_{d,t}$ is the number of occurrences of term $t$ in document $d$, and $n_t$ the number of documents  where the term $t$ appears. \par
We then compute the TFIDF = TF $\times$ IDF matrix in which the entry $d,t$ gives the importance of term  $t$ in document $d$. This allows us to very easily query the database by creating a TFIDF vector for the words in the query by treating as a document and finding the document whose vector is the most similar to the queries. \par
In order to use this model as a recommender system, a positive query for the items the user liked, a negative query for the items the user dislikes, classify all the remaining items by their similarity to one of these two queries. This way we are able to label each of the items and to answer query through $k$NN or some other classifier. The assumption used here is that the likelihood of user $u$ liking document $d \sim \text{sim}(u,d)$ where we defined the similarity measure sim as one of these metrics:
\begin{align*}
    \text{sim}(u,d) &= \cos(u,d)\\
    \text{sim}(u,d) &= \frac{\sum_{i} (u_i - \bar{u})(d_i - \bar{d})}{\sqrt{\sum_{i} (u_i - \bar{u})^2\sum_{i} (d_i - \bar{d})^2}} = \cos(u - \bar{u},d - \bar{d})
\end{align*}
For this method to work better it is necessary to preprocess the dataset by removing very common words, lemmatizing the words, etc.\par
This method, while working reasonably well has a couple drawbacks since it does not provide a confidence in its prediction and is based on pure heuristics without physical meaning.
\subsection{\textit{k}-Nearest Neighbors}
\paragraph{Idea} Given a new sample $x$ to classify, we assign it to the class of the majority of its $k$ the nearest points. We use a validation method to determine the value $k$ for which the classifier performs the best. 
\paragraph{Assumption} The training set and the testing set arise from the same underlying distribution, i.e. the training set must be representative of real life.
\paragraph{Remarks}
\begin{itemize}
\item The parameter $k$ governs the degree of smoothness of the boundary between the classes. 
\item This classifier is very easily generalized to multi-class classification.
\item The distance metric must be chosen carefully because of problems arising from noisy data or high dimensionality
\item It is necessary to load the entire dataset for each classification : that's slow. Some methods exist to reduce the dataset size though : Condensed Nearest Neighbors (Fast Condensed Nearest Neighbors also exist)
\item Normalization of the features is necessary in order for the features to be all of equal importance $\mathbf{x}_\text{norm} = \frac{\mathbf{x} - \mu}{\sigma}$. (The mean and standard deviation are made here feature-wise)
\item $k$NN performs poorly with highly unbalanced datasets.
\item It is impossible to distribute the computation, very slow for large datasets
\end{itemize}
\subsection{Machine Learning models} 
c.f. summary introduction to Machine Learning
\paragraph{Laplace smoothing} Laplace smoothing is a regularization technique that solves problems arising when some words never appears. It adds a bias towards a uniform distribution of the terms and serves as a form of regularization. It is done by adding a uniform probability on all terms on top of the dataset distribution.

