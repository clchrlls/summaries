\chapter{Text models}
Text models allow representing text in a meaningful and mathematical way. The difficulty with this task is that words can have multiple meanings and multiple words can mean the same thing. It is often assumed that the context of a word suggest the meaning of a word and exchangeable words carry the same meaning in the same context. Under these assumptions, two main methods can be used to take the context of a word into account to derive ts meaning: Continuous bag of words, where the model is tasked the word in some given context, and skip-gram where the model is tasked to predict the context of a word.

\begin{table}[!ht]
    \centering
    \begin{tabularx}{0.7 \linewidth}{|Y|Y|Y|Y|Y|}\hline
        Never & go & \cellcolor{red!25} ? & you & up\\\hline
    \end{tabularx}
    \caption{Continuous bag of words}
    \label{tab:cts-bag-words}
\end{table}

\begin{table}[!ht]
    \centering
    \begin{tabularx}{0.7 \linewidth}{|Y|Y|Y|Y|Y|}\hline
        \cellcolor{red!25} ? & \cellcolor{red!25} ? & give & \cellcolor{red!25} ? & \cellcolor{red!25} ?\\\hline
    \end{tabularx}
    \caption{Skip-gram}
    \label{tab:skip-gram}
\end{table}

\section{Word2Vec} Word2Vec is a model that yields vector representations of words in a relatively small (a few hundred) dimensional vector space. It uses skip grams during training. The data slides over the skip-gram window, the context words are denoted $C(w)$ and the middle word $w$. The goal is to learn the distribution $\theta$ such that the following expression is maximized:
\begin{align*}
    \prod_{w \in X}\prod_{c \in C(w)} p(c \vert w ; \theta) = \prod_{w, c,w \in X} p(c \vert w ; \theta)
\end{align*}
With each word $w \in W$ and each word in the context $c \in W$ are represented each by one vector $u_w$ and $v_c$ respectively (NB: there are separate representation for the same word depending on whether it appears in context or at the middle)
\[
    p(c \vert w) = \frac{e^{u_w^T v_c}}{\sum_{\tilde{c} \in W} e^{u_w^T v_{\tilde{c}}}}
\]
Which is the softmax function.\par
The problem with the approach just proposed is that the denominator of the softmax is very expensive to compute, so instead of using this objective, we instead turn the task into classification between $(w,c)$ that are in the corpus and those who are not with $P(D = 1) = \frac{1}{1 - e^{-u_w^Tv_c}}$ being the probability that $u_w, v_c$ is plausible text. We need to maximize this probability, but we also need to take into account false positive so overall, we find the distribution $\theta$ that maximizes the following :
\begin{align*}
    \theta &= \argmax{\theta} \sum_{w,c \in X} \log P(D=1\vert w, c) +\sum_{w,c \notin X} \log P(D=0\vert w, c) \\ 
    &= \argmax{\theta} \sum_{w,c \in X} \log \sigma\left(u_w^Tv_c\right) +\sum_{w,c \notin X} \log \sigma\left(-u_w^Tv_c\right)\\
    \text{With} \qquad \qquad \sigma(a) &= \frac{1}{1-e^{-a}}
\end{align*}
Although, the number of pairs $w,c \notin X$ is too large to be computed each time, only sampling this set gives good results.
\paragraph{Properties} This model has a lot of very useful properties. The distances between word vectors have meaning and allow for 'Germany - Berlin + France = Paris'. Moreover, the distances are the same across different languages which is very useful for translation.
\section{Topic models} Topic models classify documents into multiple categories. Both the approaches presented here are unsupervised and yield very reasonable results.
\subsection{Latent Semantic Indexing} Latent Semantic Indexing (LSI) simply performs SVD on the TFIDF matrix of the corpus. The latent factors produced this way are the topics and there are usually a few hundred of them. This technique has however a couple of drawbacks: 
\begin{itemize}
    \item Synonyms are not merged together and homonyms are not split, this must be done manually
    \item the model is based on heuristics, there is no statistical foundation
    \item it is not generative
    \item the results are sometimes hard to interpret
    \item this model is outperformed in performance and interpretability by other modern approaches
\end{itemize}
\subsection{Probabilistic Latent Semantic Indexing} Probabilistic Latent Semantic Indexing (pLSI) improves on LSI by assuming there is a distribution of topics for each document and topics have distinct word distribution. Basically each document can be described as the Bayesian network (c.f. Chapter \ref{chap:bayesnet}) in Figure \ref{fig:plsi-bayesnet}.
\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[node distance=2cm]
        \node[draw, circle](D)                     {$D$};
        \node[draw, circle](T1) [below left of=D]  {$T_1$};
        \node[draw, circle](T2) [below of=D]       {$T_2$};
        \node[draw, circle](T3) [below right of=D] {$T_3$};
        \node[draw, circle](W1) [below of=T1]      {$W_1$};
        \node[draw, circle](W2) [below of=T2]      {$W_2$};
        \node[draw, circle](W3) [below of=T3]      {$W_3$};

        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (A) -- (T1);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (A) -- (T2);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (A) -- (T3);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T1) -- (W1);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T2) -- (W2);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T3) -- (W3);
    \end{tikzpicture}
    \caption{Bayesian network of a document in pLSI}
    \label{fig:plsi-bayesnet}
\end{figure}
pLSI is flexible, but it still has a quite high number of parameters: $P(T \vert D)$ has $KM$ parameters ($M$ being the number of documents, and $K$ the number of topics) and $P(W \vert T)$ has $KV$ (with $V$ the number of words). Since it is linear in the number of documents, it can quickly start to overfit the dataset. Furthermore, while it is possible to generate new words for a document, it is still impossible to generate new documents.
\subsection{Latent Dirichlet Allocation} Latent Dirichlet Allocation (LDA) further improves pLSI by assuming there is a latent factor on which the topic distribution of the documents depend. This assumption yields the Bayesian network in Figure \ref{fig:lda-bayesnet}. In this model, the topic distribution is itself sampled from an underlying hidden distribution.

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[node distance=2.5cm]
        %alpha
        \node[] (Alph) at (0,0) {$\alpha$};
        %docs
        \node[draw, circle](D1)  at (-5, -2) {$D_1$};
        \node[draw, circle](D2)  at ( 0, -2) {$D_2$};
        \node[draw, circle](D3)  at ( 5, -2) {$D_3$};
        %topics
        \node[draw, circle](T11) [below left of=D1]  {$T_{11}$};
        \node[draw, circle](T12) [below of=D1]       {$T_{12}$};
        \node[draw, circle](T13) [below right of=D1] {$T_{13}$};
        \node[draw, circle](T21) [below left of=D2]  {$T_{21}$};
        \node[draw, circle](T22) [below of=D2]       {$T_{22}$};
        \node[draw, circle](T23) [below right of=D2] {$T_{23}$};
        \node[draw, circle](T31) [below left of=D3]  {$T_{31}$};
        \node[draw, circle](T32) [below of=D3]       {$T_{32}$};
        \node[draw, circle](T33) [below right of=D3] {$T_{33}$};
        %words
        \node[draw, circle](W11) [below of=T11] {$W_{11}$};
        \node[draw, circle](W12) [below of=T12] {$W_{12}$};
        \node[draw, circle](W13) [below of=T13] {$W_{13}$};
        \node[draw, circle](W21) [below of=T21] {$W_{21}$};
        \node[draw, circle](W22) [below of=T22] {$W_{22}$};
        \node[draw, circle](W23) [below of=T23] {$W_{23}$};
        \node[draw, circle](W31) [below of=T31] {$W_{31}$};
        \node[draw, circle](W32) [below of=T32] {$W_{32}$};
        \node[draw, circle](W33) [below of=T33] {$W_{33}$};

        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (Alph) -- (D1);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (Alph) -- (D2);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (Alph) -- (D3);

        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D1) -- (T11);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D1) -- (T12);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D1) -- (T13);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D2) -- (T21);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D2) -- (T22);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D2) -- (T23);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D3) -- (T31);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D3) -- (T32);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (D3) -- (T33);


        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T11) -- (W11);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T12) -- (W12);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T13) -- (W13);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T21) -- (W21);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T22) -- (W22);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T23) -- (W23);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T31) -- (W31);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T32) -- (W32);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (T33) -- (W33);
        
    \end{tikzpicture}
    \caption{Bayesian network of a document in LDA}
    \label{fig:lda-bayesnet}
\end{figure}

The topic distribution $\theta$ over the documents is sampled from a Dirichlet distribution $\text{Dir}(\alpha)$:
\[
    P(\mathbf{\theta}_m ; \alpha) = \prod_{i=1}^K \theta_{im}^{-\alpha_i - 1}
\]
Where the $\theta_{im}$ are normalized such that $\sum_{i=1}^K \theta_{im} = 1$, $\theta_{im} > 0$, and $\alpha > 1$. In the Dirichlet distribution, the domain is the $n$-dimentional simplex and with different values of $\alpha$ yield the following distributions shown in Figure \ref{fig:dirichlet-dist}. When $\alpha \rightarrow \infty$, then the Dirichlet distribution is a Dirac.\par
Then, when conditioned on the topic, the word distribution follow a distribution with parameters $\beta$ as for pLSI.
\begin{figure}[ht] 
    \centering
    \includegraphics[scale=1]{fig/dirichlet.jpg}
    \caption{Examples of Dirichlet distribution with various assignments for $\alpha$}
    \label{fig:dirichlet-dist}
\end{figure}
LDA is a powerful model since it does not have a lot of parameters ($K$ for $\alpha$ and $KV$ for $\beta$) and they are independent on the number of documents. It is also fully generative. It is currently the best performing approach for unsupervised document clustering, and can be extended to a model where the parameters $\alpha$ evolve with time, giving insights on how topics change over time.