\chapter{Refreshers}
\section{Cryptography}

\subsection{Security properties}
Multiple secutity properties can be desired depending on the situation. For instance a broadcasting service may not care about the fact ability of everyone to have access to the content broadcasted, but may care about the inability of other actors to impersonate it (as is the case with GPS, for instance). Other situations may require different properties.

\begin{itemize}
    \item \textbf{Secrecy} \\
    Secrecy encompass other similar properties: Confidentiality, Anonymity, and privacy. Secrecy guarantees the inability of the unintended parties to access the data. 
    \item \textbf{Confidentiality} \\
    Confidentiality is keeping someone else's data secret. 
    \item \textbf{Privacy} \\
    Privacy is keeping data about a person secret.
    \item \textbf{Anonymity} \\
    Anonymity is keeping the identity of a person secret.
    \item \textbf{Integrity} \\
    Data integrity is ensuring the data is not tampered with (intentionnaly or not). Data authentication is ensuring the data has the source it claims. Entity authentication or integrity is the verification of the claimed identity of the entity. Often, integrity is used on static data and authentication on transferred data. Data authentication during a transfer implies data integrity as well.
\end{itemize}

\subsection{Symmetric cryptography}
Symmetric cryptography is cryptography where the key used to decrypt is the same than the key used to decrypt. In a two party setup, it implies the need for the two parties to agree on a shared secret key. The primitives available and their notation are whown in Table \ref{tab:crypt-refr-primitives}.

\begin{table}[!ht]
    \centering
    \begin{tabular}{l|l}
        Primitive & Notation \\\hline
        Key & $k$ \\
        Encryption & $E_k($plaintext$)$ = $\{$plaintext$\}_k$ = ciphertext \\
        Decryption & $D_k($ciphertext$)$ = plaintext \\
    \end{tabular}
    \caption{Primitives and notations for symmetric cryptography}
    \label{tab:crypt-refr-primitives}
\end{table}

\subsubsection{One-time pad}\label{sec:refresh-otp}
The one-time pad is the simplest scheme of symmetric encryption. Before the communication, both parties agree on random sequences of bits, the pads, which act as keys. Then, when the communication takes place, the sender xorrs the plaintext $P$ with the pad $k$ and sends the resulting ciphertext $C$ . The receiver xorrs it again and retrieves the plaintext.

\begin{align*}
    C &= E_k(P) = P\oplus k \\
    P&= D_k(C) = C\oplus k \\
\end{align*}

This encryption scheme provides perfect secrecy, i.e. the attacker has the same chance of recovering the plaintext regardless of whether they know the ciphertext or not or, equivalently, when an attacker knows the ciphertext, their best chance of recovering the plaintext is random guesses.\\
This strong property however only holds when \textit{i}) the pad is kept secret and \textit{ii}) the pad is never reused. Indeed if the same pad is reused for two plaintexts $P_1$ and $P_2$, xorring the corresponding ciphertexts $C_1$ and $C_2$ reveals some information about the plaintexts. 

\begin{align*}
    C_1 \oplus C_2 = k \oplus P_1 \oplus k \oplus P_2 = P_1 \oplus P_2
\end{align*}

Then, knowing either $P_1$ or $P_2$ reveals the other, and statistical analysis is possible on $P_1 \oplus P_2$ to recover the plaintexts.
 
\subsubsection{Stream ciphers}
Stream ciphers are schemes where a secret shared key $k$ and a publicly shared initialisation vector $IV$ are used by both parties to generate a sequence of pseudorandom bits PRG($IV, k$). This stream is then used as a one-time pad to encrypt and decrypt the data. When transferring data, the $IV$ is arbitrarily chosen by the sender and sent along the ciphertext. The $IV$ is used here to prevent the pad reuse problem explained before in Section \ref{sec:refresh-otp}. Indeed, without this parameter, the keystream would be always be the same since the key does not change. AES in counter mode (AES-CTR) and ChaCha are both stream ciphers.\\
In AES-CTR, the $IV$ and the value of a counter is combined in an invertible way, then given as input to the AES block cipher along with the key. The resulting block is used as a pad for a block of the plaintext. For the next block, the counter is incremented and the process starts again. Decryption is the same process. This process is shown in Figure \ref{fig:refresh-aes-ctr}.

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/AES-CTR.png}
    \caption{AES in CTR mode}
    \label{fig:refresh-aes-ctr}
\end{figure}

ChaCha20 is another stream cipher. It is a high-speed adaptation of the Salsa stream cipher and is used in TLS1.3 and Wireguard VPN.\\
A major issue not addressed by the stream ciphers is that the modification of a given bit of ciphertext results in the modification of the same bit in plaintext, which can have dire consequences e.g. if the most significant bit of the amount transferred in a wire tranfer is flipped. To defend against that, guarantees about authenticity are needed.\\
AES in Galois counter mode (AES-GCM) and ChaCha20 with Poly1365 (ChaPoly)solve that issue. These mode allows for data encryption and authentication in the same pass. Both are a form of authenticated encryption with additional data (AEAD) which augments the plaintext with some data to be integrity protected, encrypts the plaintext, and authenticates both the ciphertext and the additiuonal data.

\subsubsection{Block ciphers}
Block ciphers are pseudorandom permutation generators $PRP$ which define a one-to-one mapping from a key to a permutation (to avoid the birthday paradox). The plaintext is cut into blocks of the same size and encrypted separately block-by-block.\\
AES electronic codebook (AES-ECB) is the simplest mode of operation of the AES block-cipher. It encrypts each block with the same key as shown in Figure \ref{fig:refresh-aes-ecb}. 

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/AES-ECB.png}
    \caption{AES in ECB mode}
    \label{fig:refresh-aes-ecb}
\end{figure}

This scheme is considered rather weak. Since it does not involve an initialisation vector and the key remains the same, two same plaintext blocks will result in two same ciphertexts. While it does not have the one time pad reuse problem of revealing information about the plaintext to the extent of the one-time-pad, it allows to recover the structure in data which repeats at the same rate as blocksize. This is particularly obvious when encrypting simple images as shown in Figure \ref{fig:refresh-ecb-image}. Moreover, the individual blocks can be substituted and reordered by an attacker

\begin{figure}[!ht]
    \centering
    \begin{minipage}{.8\linewidth}
        \begin{minipage}{0.3\linewidth}
            \fig{fig/refresh/Tux.jpg}
        \end{minipage}
        \hfill
        \begin{minipage}{0.3\linewidth}
            \fig{fig/refresh/Tux_ecb.jpg}
        \end{minipage}
        \hfill
        \begin{minipage}{0.3\linewidth}
            \fig{fig/refresh/Tux_secure.jpg}
        \end{minipage}
    \end{minipage}
    \caption{Image (left) encrypted with AES ECB mode (middle) and with another mode resulting in pseudo-randomness (right)\cite{WikipediaEN:ECB}}
    \label{fig:refresh-ecb-image}
\end{figure}

AES in cipher block chaining mode (AES-CBC) fixes this problem by introducing an initialisation vector $IV$ and chaining the blocks. The first plaintext block is xorred with the $IV$ before passong through the AES algorithm, and subsequent plaintext blocks are xorred with the previous ciphertext block before passing being encrypted. Its implementation is shown in Figure \ref{fig:refresh-aes-cbc}. In this implementation, an altered ciphertext only influences two blocks. 

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/AES-CBC.png}
    \caption{AES in CBC mode}
    \label{fig:refresh-aes-cbc}
\end{figure}

\subsubsection{Message authentication codes}
Message authentication codes (MACs) provide a cryptographic checksum to a message. It can only be computed correctly if the key in known. If the MAC is based on a hask function it is a hash-based MAC (HMAC), and if it is block cipher based is is a CMAC. Combining the ciphertext and the MAC is a complicated endeavour so AEAD is more widely used to achieve secrecy and authenticity.

\subsection{Asymmetric cryptography}
In asymmetric cryptography the keys used for encryption and decryption (or signature and verification) are two distinct keys. The two main primitives are the Diffie-Hellman key exchange and RSA. Without authenticity guarantees, it is easier for an attacker to perform a Machine-in-the-Middle (MitM) attack.\\
Asymmetric cryptography has many advantages over symmetric cryptography, but it is also orders of magnitude slower than symmetric cryptography and requires significantly longer keys.

\subsubsection{Diffie-Hellman}\label{sec:dhe} The Diffie-Hellman key exchange (DHE) algorithm allows for two parties to establish a shared secret over an insecure channel. Two mathematical properties are used for two flavours of DHE: the discrete logarithm problem and elliptic curves. 
\subsubsection{RSA} RSA allows for encryption/decryption and signature/verification. Signatures are stronger than authentication codes since it allows the receiver to perform authentication and to prove the origin of the messsage is the claimed one to third parties, which they cannot do with symmetric cryptography.

\subsection{Hash functions}
Cryptographic hash functions are one-way functions which map arbitrary length data to a fixed length digest. A cryptographic hash function satisfies the following properties:
\begin{enumerate}
    \item \textbf{One-way} \\
    Given $y = H(x)$, it is impossible to find $x'$ such that $H(x') = y$. 
    \item \textbf{Weak collision resistance} \\
    Given $x$, it is impossible to find $x' = x$ s.t. $H(x) = H(x')$.
    \item \textbf{Strong collision resistance} \\
    It is impossible to find $x, x'$ s.t. $H(x) = H(x')$.
\end{enumerate}
Examples of such functions are MD5, SHA1, SHA-2, SHA-3, Poly1305, but some properties of MD5 and SHA-1 have been broken.

\subsubsection{Attack complexity} The attack complexity for cryptographic hash functions are on average $2^{n-1}$ for attacking one-wayness and weak collision resistance, and, due to the birthday paradox, $2^{n/2}$ for the strong collision resistance. 

\subsubsection{One-way hash chains} Hashing functions can be used to create a useful primitive: the one-way hash chain. These can be used to create authenticity with trust on first use (TOFU) or some other bootstrapping method. The sender generates a cryptographically secure random number $n$ and creates a sequence of length $N$ of the hashes of this number $\{h_N = n, h_{N-1} = H(h_N), \dots , h_0 = H(h_1)\}$ with a public hashing function $H$. The elements of this sequence are then released sequentially starting from $h_0$. Thus when a receiver receives a message along with $h_k$, they can use the $h_{k-1}$ providedd with the previous message to verify these message came from the same sender by verifying $H(h_{k-1}) = h_k$. This scheme requires loose synchronisation, but can achieve authentication cheaply. Once the chain is ended, the last number is used to bootstrap the next chain.

\subsubsection{Merkle hash tree} Merkle hash trees are used to verify the integrity of data in an efficient manner. An example of the composition of a hash tree is shown in Figure \ref{fig:refresh-merkle-tree}. For instance, after the a receiver has received all the files, they compute the values of the hash tree until it reaches the root. The receiver then compares their value to the one sent by the sender. If it matches, all is good, otherwise, the sender can figure out what files have been modified by going down the merkle tree.

\begin{figure}[ht]
    \centering
    \resizebox*{.8\textwidth}{!}{
        \begin{tikzpicture}
            \node[] at (0,0) (D0) {$D_0$};
            \node[] at (2,0) (D1) {$D_1$};
            \node[] at (4,0) (D2) {$D_2$};
            \node[] at (6,0) (D3) {$D_3$};
            \node[] at (8,0) (D4) {$D_4$};
            \node[] at (10,0) (D5) {$D_5$};
            \node[] at (12,0) (D6) {$D_6$};
            \node[] at (14,0) (D7) {$D_7$};

            \node[] at (1, 2) (H01) {$H_{01} = H(D_0 \Vert D_1)$};
            \node[] at (5, 2) (H23) {$H_{23} = H(D_2 \Vert D_3)$};
            \node[] at (9, 2) (H45) {$H_{45} = H(D_4 \Vert D_5)$};
            \node[] at (13, 2) (H67) {$H_{67} = H(D_6 \Vert D_7)$}; 

            \node[] at (3,4) (H03) {$H_{03} = H(H_{01} \Vert H_{23})$};
            \node[] at (11,4) (H47) {$H_{47} = H(H_{45} \Vert H_{67})$};

            \node[] at (7,6) (H07) {$H_{07} = H(H_{03} \Vert H_{47})$};


            \draw (D0.north) -- (H01.south);
            \draw (D1.north) -- (H01.south);
            \draw (D2.north) -- (H23.south);
            \draw (D3.north) -- (H23.south);
            \draw (D4.north) -- (H45.south);
            \draw (D5.north) -- (H45.south);
            \draw (D6.north) -- (H67.south);
            \draw (D7.north) -- (H67.south);

            \draw (H01.north) -- (H03.south);
            \draw (H23.north) -- (H03.south);
            \draw (H45.north) -- (H47.south);
            \draw (H67.north) -- (H47.south);

            \draw (H03.north) -- (H07.south);
            \draw (H47.north) -- (H07.south);
        \end{tikzpicture}
    }
    \caption{Merkle hash tree}
    \label{fig:refresh-merkle-tree}
\end{figure}

\section{Network}
Internet is a stack of various protocols, the layer model is used to achieve encapsulation and thus modularity and interoperability. The layers and some of the protocols in the corresponding layers are shown in Table \ref{tab:refresh-layers}.

\begin{table}[ht!]
    \centering
    \begin{tabular}{l | l}
        Layer & Protocol \\\hline
        7. Application & HTTP, HTTPS, SMTP, TLS/SSL, \dots \\
        4. Transport & TCP, QUIC, \dots \\
        3. Internet & IP \\
        1/2. Link & 802.11, Ethernet, \dots
    \end{tabular}
    \caption{Layers and some protocols in them}
    \label{tab:refresh-layers}
\end{table}

\subsection{Internet protocol}
The internet protocol for packet forwarding (IP) is in the first layer after the link layer. It is in the data plane. The end hosts specify the source and the destination of the packets, there is no control over the path(s) taken by the packets and no guarantee is offered for the delivery of the packets. It uses the internet control message protocol (ICMP) for error messages, the address resolution protocol (ARP) in order to translate between the IP and MAC addresses, the dynamic host configuration protocol (DHCP) to assign IP addresses to hosts, network address translation (NAT) to switch from the local network to the internet, and the border gateway protocol (BGP) for global routing between autonomous systems (ASes) (see Section \ref{sec:refresh-bgp}) in the control plane. The full header is shown in Figure \ref{fig:refresh-ipv4-header}

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/ipv4_header.png}
    \caption{IPv4 header\cite{WikipediaEN:IPV4}}
    \label{fig:refresh-ipv4-header}
\end{figure}

\subsubsection{IPv4}
IPv4 use various fields for straightforward needs such as version, header (IHL), total length, protocol, and header checksum. IPv4 packets use 4-bytes IP addresses which are independent from the link layer. Other fields are used to handle the different size of packets: identification, fragment offset, and fragment control bits. Finally other fields are used for other specific needs: differentiated services to differentiate between types of services for prioritization for instance and time to live which is used for debugging by traceroute for instance.
\paragraph{ICMP}
When a router encounters an error while forwarding, it drops the packet and sends back an ICMP error packet to the sender. The source needs to rectify. The different code errors are shown in Table \ref{tab:refresh-icmp-codes}.
\begin{table}[!ht]
    \begin{tabular}{m{6cm} | m{2.5cm} | m{4cm}}
        Name & Code & Usage \\\hline
        Dest. unreachable (net or host) & 3/0 or 1 & Lack of connectivity\\
        Dest unreachable (fragment) & 3/4 & Path MTU Discovery \\
        Time exceeded (transit) & 11/0 & Traceroute \\
        Echo request or reply & 8 or 0/0 & Ping
    \end{tabular}
    \caption{ICMP error codes}
    \label{tab:refresh-icmp-codes}
\end{table}
\subsubsection{IPv6}
IPv6 use 128-bit addresses, which take up most of the header. The notation of an IPv6 address is eight groups of four hexadecimal digits, omitting leading zeroes and one continuous sequence of zeros. For instance \lstinline{2001:0db8:0000:0000:0000:ff00:0042:8329 = 2001:db8::ff00:42:8329}. The header of an IPv6 packet is shown in Figure \ref{fig:refresh-ipv6-header}.
\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/ipv6_header.png}
    \caption{IPv6 header\cite{WikipediaEN:IPV6}}
    \label{fig:refresh-ipv6-header}
\end{figure}
The typical networks sizes are /32 for each ISP, /48 for each location, /64 for each logical network. Each IPv6 capable host has already a link locak address (typically \texttt{fe80::/64}). Then in order to ask for a proper unique IP, the hosts canuse router advertisments, or additionally DHCPv6.
\subsubsection{Allocation of IP addresses}
The IP addresses are allocated by delegates of IANA, which manages the ip addresses worldwide. These delegates then delegate the actual assignment to ISPs. These addresses are assigned and used by prefixes. The notation is \lstinline{<ip_address>/<prefix_length>} for instance 129.132.0.0/16 is ranges from 129.132.0.0 to 129.132.255.255. The longer the prefix the more specific the address space is and the less addresses it contains.
\subsubsection{Middleboxes: NAT}
Since there are less IPV4 addresses than is needed ($\approx$4 billions) and for other purposes, some devices on the network break the layer model. Firewalls do so to control what enters and exits local networks, but NATs also do that in order to overcome the ip address scarcity problem. NAT essentially translates the local IP addresses of the hosts in the local network to a single IP address and translates the addresses between the local and global network. In order to do so, the NATs must look into and edit the application layer data. Usually, it uses the port numbers to extend the ip addresses. A translation table is shown in Table \ref{tab:refresh-nat-translation-table}.
\begin{table}[!ht]
    \centering
    \begin{tabular}{m{4cm} | m{4cm}}
        Host perspective & ISP perspective \\\hline
        192.168.1.12:5523 & 44.25.80.3:1500 \\
        192.168.1.15:1243 & 44.25.80.3:1501 \\
        192.168.2.23:1243 & 44.25.80.3:1502 \\
    \end{tabular}
    \caption{NAT translation table}
    \label{tab:refresh-nat-translation-table}
\end{table}
\subsection{BGP}\label{sec:refresh-bgp}
The border gateway protocol (BGP) handles the routing between the autonomous systems (ASes). Is uses a path-vector protocol and disseminates information about the location and paths for IP prefixes. It is a business oriented protocol, not necessarily a technical one. The path chosen will not necessarily be the most performant one where the parties have contracts with one another for cost-efficiency.

\subsection{Transport}
The transport lyer is in layer 4. Its role is to make a link between services running on different nodes. Different guarantees are given to the upper layers as to reliability and ordering.

\subsubsection{Transmission control protocol} 
The transmission control protocol (TCP) guarantees the packets arrive without changes at destination and at the correct application, segments the data, provides a way to preserve their ordering despite the nonexistent guarantees provided by the lower layer (IP), and avoids to overload the destination and the network. In order to achieve this feature, TCP adds some information in a header shown in Figure \ref{fig:refresh-tcp-header}.

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/tcp-header.png}
    \caption{The TCP header}
    \label{fig:refresh-tcp-header}    
\end{figure}

\paragraph{Connection establishment} The connection between the source and the destination is established using the three-way handshake to guarantee they are both ready to start transferring data and to negotiate parameters for subsequent transfer of data. To establish a connection, the client sends a SYN packet containing the initial sequence number SYN($x$). The server replies with the sequence number for the other direction SYN($y$), and an ACK($x + 1$) for the SYN($x$) received. Finally, the client answers with an ACK($y+1$) for the SYN($y$) received. The sequence numbers set up are then used for the subsequent exchanges.

\paragraph{Connection release} The connection is released by the client who sends a FIN packer. The server replies with an acknowledgement to the FIN and  another FIN for the client. Finally, the client sends an ultimate FIN packet and the connection is closed.

\paragraph{Flow control} Flow control is used not to overload the receiver. After the client sends a segment, the server replies with an ACK for the packet received, and the window size. The window size is the remaining available space in the receiver's buffer. The client then proceeds to send segments while the difference between the last frame sent and the last acknowledged frame is smaller or equal than the window. Figure \ref{fig:refresh-tcp-sw} shows an example of the sliding window flow control algorithm.

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/tcp-sw.png}
    \caption{Example of the sliding window running}
    \label{fig:refresh-tcp-sw}
\end{figure}

With the sliding window, the detection of a lost segment is achieved by a timer. When a segment is not acked before the timer reaches 0, the segment is retransmitted.

\paragraph{Congestion control} In order to allocate the bandwidth fairly amongst the users, and to minimise the congestion of the network, a supplementary mechanism is put in place. A congestion window is also maintained by the sender. This window (i.e. the amount that can be sent before waiting for an ACK) is set according to measured metrics about the connection. Table \ref{tab:refresh-tcp-cc} shows the various metrics used by various flavours of TCP. 

\begin{table}[!ht]
    \centering
    \begin{tabular}{l | l | l}
        Signal & Protocol 
        & Pros/Cons\\\hline
        \multirow{2}{*}{Packet loss} & \multirow{2}{*}{TCP NewReno, TCP Cubic} & + Hard to get wrong \\
        & & $-$ Congestion detected late \\
        \multirow{2}{*}{Packet delay} & \multirow{2}{*}{Compound TCP, BBR} & + Congestion detected early \\
        & & $-$ Congestion needs to be inferred \\
        \multirow{2}{*}{Router indication} & TWP w. Explicit & + Early \\
        & congestion detection & $-$ Router support needed \\
    \end{tabular}
    \caption{Signals used to detect congestion by different flavours of TCP}
    \label{tab:refresh-tcp-cc}
\end{table}

Using this signal, TCP can detect when congestion happens and reduce its rate accordingly. There are two main ways to increase the sending rate:

\begin{enumerate}
    \item \textbf{Additive increase multiplicative decrease} (AMID) \\
    In AMID, the number of segments sent each round is augmented by a fixed number of segments. When congestion is detected, the number of segments sent each round is reduced to a fraction of the last number used. The increase is additional and the decrease is multiplicative. The graph of the number of segments is then a sawtooth, oblique fot the increase and vertical for the decrease.
    \item \textbf{Slow start} \\
    Slow start is used to reach the equilibrium point faster. Each round the number of segments is doubled. The increase is thus exponential. When congestion is detected, the number of segments is the one before congestion happened and Slow start is switched to AMID to stop overshooting and finding more accurately the maximum rate.
\end{enumerate}

\subsubsection{User datagram protocol}
User datagram protocol (UDP) only takes care of sending the packets to the right application with a checksum. No other guarantees are given. Despite the shortcomings posed by the lack of guarantees, this lacks implies very low overhead to send data. The UDP packet is shown in Figure \ref{fig:refresh-udp-header}.

\begin{figure}[!ht]
    \centering
    \fig{fig/refresh/udp-header.png}
    \caption{UDP packet}
    \label{fig:refresh-udp-header}
\end{figure}

\subsection{Domain name system}\label{sec:refresh-dns}
The domain name system (DNS) is used to translate user rememberable identifiers for webservices: host names (e.g. ethz.ch) to machine usable identifiers: IP addresses. These are resolved recussively and in a distributed fashion. First a DNS request is sent from a client to a local DNS server which knows the address of the root servers which know the addresses of the top level domains (.ch, .com, etc.) which, in  turn, know the addresses of autoritative servers for the domain (ethz.ch, etc.).\\
In order to resolve the host names efficiently, the local DNS server caches the recently queried host names to serve them quicker in the future. 



