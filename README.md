Here you'll find all the summaries I made during my studies. Some are finished, others are not, and all of them may contain all sorts of mistakes, so be careful when you read them.
Feel free to open issues/merge requests if you want to correct/edit/expand/etc. I might not be able to respond to them quickly but I'll do eventually.

I hope it can be useful for you !