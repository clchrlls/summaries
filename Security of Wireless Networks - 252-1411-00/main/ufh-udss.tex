\chapter{Uncoordinated FHSS and DSSS}
In broadcast communication, resistance to jamming becomes a totally different problem. Since many devices are receivers, it is extremely impractical to safely distribute the shared secret without it being available to the jammer because any of the receivers can leverage the secret key in order to jam the communication. Despite this, some techniques based on shared keys exist and are described in Sections \ref{sec:br-antijam-sys} and \ref{sec:dyn-jam-mitig}. These methods however, require coodrination between the sender and the receivers (even if just for the initial key distribuition) thus do not work for unknown receivers. Other solutions which do not rely on pre shared keys exist and are described in Sections \ref{sec:ufh} and \ref{sec:udss}.

\section{Broadcast anti-jamming systems}\label{sec:br-antijam-sys} 
Broadcast anti-jamming systems \cite{DESMEDT2001223} is a technique based on frequency hopping spread spectrum (Section \ref{sec:bas-jam-fhss}). In this scheme, the sender sends simultaneously the same signal on several frequencies and each of the receivers listen to a subset of these frequencies. This allows for a jamming-resistant broadcasting scheme for up to $j - 1$ clolluding receivers, where $j$ depends on the channel allocation table.\\
The channel allocation table (Table \ref{tab:br-antijam-sys-channel-alloc}) describes the allocation of the channels for each of the receivers. This table is public and allows for each receiver to listen to the allocated frequencies. The allocations are made such that 
at least $j - 1$ receivers are needed in order to cover the whole set of frequencies which is necessary for successful jamming. This problem boils down to the set-coverage problem. \\
\begin{table}[!ht]
    \centering
    \begin{tabular}{ l | c | c | c | c | c | c }
        Channel & Sender      & Receiver1  & Receiver2  & Receiver3  & Receiver3  & \dots \\\hline
        1       &  \checkmark &            & \checkmark &            &            & \dots \\
        2       &  \checkmark & \checkmark &            &            &            & \dots \\
        3       &  \checkmark & \checkmark &            & \checkmark &            & \dots \\
        4       &  \checkmark &            &            & \checkmark &            & \dots \\
        5       &  \checkmark & \checkmark &            &            & \checkmark & \dots \\
        \dots   & \dots       & \dots      & \dots      & \dots      & \dots      & \\
    \end{tabular}
    \caption{Example of a channel allocation table}
    \label{tab:br-antijam-sys-channel-alloc}
\end{table}
The frequency allocation table (Table \ref{tab:br-antijam-sys-freq-alloc})is kept secret and is generated and updated via a pseudo-noise generator. For each of the parties, the table values are only known for the channel they have been allocated.\\
\begin{table}[!ht]
    \centering
    \begin{tabular}{ l | c | c | c | c | c | c }
        Channel         & 1     & 2     & 3     & 4     & 5     & \dots \\\hline
        Frequency [GHz] & 2.453 & 2.847 & 2.046 & 2.565 & 2.284 & \dots \\
    \end{tabular}
    \caption{Snapshot of an example of a frequency allocation table}
    \label{tab:br-antijam-sys-freq-alloc}
\end{table}
This method works because of the following theorem (which derives from the set coverage problem)
\newpage
\begin{theorem}
    For the channel set $C = \{c_i\}_{i = 1}^{m}$, the receivers set $R = \{R_i\}_{i = 1}^{l}$, the subsets of the channels allocated to each receiver $CR = \{C_i\}_{i = 1}^{l}$, and $j$ the number of colluding receivers.\\
    If $\vert  C_i \vert \geq 1 + (j - 1)d$ for all $i \in \{1, 2, \dots, l\}$ and $\vert C_i \wedge C_k \vert \leq d$ for all $i \neq k$, then $(C, CR)$ is an broadcast anti-jamming system.
\end{theorem}
Since broadcast anti-jamming systems relies on secret information distributed accross the receivers, we can also consider this scheme as a multicast
solution.

\section{Dynamic Jamming Mitigation}\label{sec:dyn-jam-mitig}
Dynamic Jamming Mitigation \cite{4509772} is based on DSSS (Section \ref{sec:bas-jam-dsss}) and uses a binary key tree (Figure \ref{fig:bin-tree-dyn-jam-mitig}) as well as the ability of the receivers to report jamming when it occurs to achieve broadcast anti-jamming. Each node of the tree represents a spreading code and each user $N_i$ is given a unique spreading code at a leaf $C_i$ of the tree as well as all the codes on the path from the allocated leaf to the root of the tree. \\
\begin{figure}[!ht]
    \centering
    \resizebox*{\textwidth}{!}{
    \begin{tikzpicture}
        \node[draw, circle](C07) at (0, 0) {$C_{07}$};

        \node[draw, circle](C03) at (-4, -1) {$C_{03}$};
        \node[draw, circle](C47) at ( 4, -1) {$C_{47}$};


        \node[draw, circle](C01) at (-6 , -2) {$C_{01}$};
        \node[draw, circle](C23) at (-2 , -2) {$C_{23}$};
        \node[draw, circle](C45) at ( 2 , -2) {$C_{45}$};
        \node[draw, circle](C67) at ( 6 , -2) {$C_{67}$};

        \node[draw, circle](C0) at (-7 , -3) {$C_{0}$};
        \node[draw, circle](C1) at (-5 , -3) {$C_{1}$};
        \node[draw, circle](C2) at (-3 , -3) {$C_{2}$};
        \node[draw, circle](C3) at (-1 , -3) {$C_{3}$};
        \node[draw, circle](C4) at ( 1 , -3) {$C_{4}$};
        \node[draw, circle](C5) at ( 3 , -3) {$C_{5}$};
        \node[draw, circle](C6) at ( 5 , -3) {$C_{6}$};
        \node[draw, circle](C7) at ( 7 , -3) {$C_{7}$};

 
        \node[draw, circle](N0) at (-7 , -4.5) {$N_{0}$};
        \node[draw, circle](N1) at (-5 , -4.5) {$N_{1}$};
        \node[draw, circle](N2) at (-3 , -4.5) {$N_{2}$};
        \node[draw, circle](N3) at (-1 , -4.5) {$N_{3}$};
        \node[draw, circle](N4) at ( 1 , -4.5) {$N_{4}$};
        \node[draw, circle](N5) at ( 3 , -4.5) {$N_{5}$};
        \node[draw, circle](N6) at ( 5 , -4.5) {$N_{6}$};
        \node[draw, circle](N7) at ( 7 , -4.5) {$N_{7}$};

        \draw (C07) -- (C03);
        \draw (C07) -- (C47);

        \draw (C03) -- (C01);
        \draw (C03) -- (C23);
        \draw (C47) -- (C45);
        \draw (C47) -- (C67);

        \draw (C01) -- (C0);
        \draw (C01) -- (C1);
        \draw (C23) -- (C2);
        \draw (C23) -- (C3);
        \draw (C45) -- (C4);
        \draw (C45) -- (C5); 
        \draw (C67) -- (C6);
        \draw (C67) -- (C7);
        
    \end{tikzpicture}}
    \caption{Example of a binary tree used for dynamic jamming mitigation}
    \label{fig:bin-tree-dyn-jam-mitig}
\end{figure}
Each time the sender wants to broadcast a message, they first select a disjoint cover of codes (for instance $C_{01}$, $C_{23}$, and $C_{47}$ and transmit the message spread with each of the codes in the cover, then the sender select some test codes outside of those already used (for instance $C_{0}$, $C{3}$, and $C_{67}$) and transmits again the message using these codes. This supplementary transmission can allow the receivers to detect a jamming has taken place when it occurs. If a receiver receives a message on a test code but not on the corresponding detectable code, it can report to the sender a jamming has occured. \\
Splitting and reforming the tree allows for the transmitter to send the messages on less than $2j - 1$ codes, where $j$ is the expected number of jammers. However, this method requires highly flexible receivers which are able to receive and transmit on a large number of codes. This approach also requires duplex communication and prior sharing of key material.

\section{Uncoordinated frequency hopping spread spectrum}\label{sec:ufh}
Uncoordinated frequency hopping spread spectrum (UFH) is a technique that avoids the need to previously share a key to avoid jamming. The obvious solution would be to use public key cryptography to share the key, but for that we need a channel, for which we need to have some anti-jamming scheme, for which we need a key. So plain public key cryptography cannot work. \\
Instead, the idea in FSSS is for the sender to send break apart the message it wants to send into multiple fragments and send each of them on multiple channels. The receiver then can listen to random channel until it gathers all the fragments.\\
This method disables the attacker to jam the communication but they are still able to jam individual channels (or a subset of them, to insert or modify packets. In order to prevent these attacks, the messages are linked together which allows the receiver to reconstruct the message and check its integrity. Doing this using hashes or using a one-way accumulator allows for the receiver to do that efficiently, recucing the complexity from $O((I + 1)^l)$ to $O((I + 1)l)$, where $l$ is the number of fragments that compose the message, and $I$ is the number of fakes the attacker sends for each fragment. Other solutions such as Fountain codes exist.

\section{Uncoordinated direct sequence spread spectrum}\label{sec:udss}
In uncoordinated direct sequence spread spectrum (UDSS), the sender and the receivers are given a set pre-shared of spreading codes. When the receiver transmits, it transmits using a random spreading code from the set. The sender thus only needs to listen to the medium and try all the possible spreading codes from the pre-shared set to recover the message. A rafinement of this tecknique is for the sender to generate a pseudorandom spreading code, to transmit the message using the generated code, and then transmit the spreading code using one of the spreading codes from the pre-shared set. This scheme is a kind of delayed key disclosure schemnes (like Tesla in Chapter \ref{sec:tesla}).