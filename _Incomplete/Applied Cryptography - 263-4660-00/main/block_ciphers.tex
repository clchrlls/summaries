\chapter{Block ciphers}
\begin{definition}[Block ciphers]
    A block cipher with key length $k$ and block size $n$ consists of to sets of efficiently computable permutations:
    \begin{align*}
        E: \{0,1\}^k \times \{0,1\}^n \mapsto \{0,1\}^n \qquad \text{and} \qquad D: \{0,1\}^k \times \{0,1\}^n \mapsto \{0,1\}^n
    \end{align*}
    such that $D(K, E(K,m)) = m$. 
\end{definition}

Many different standards for symmetric encryption exist with various key sizes, results of cryptanalysis, standardisation and support in crypto libraries, cost of implementation, runtime costa, and implementation security. As a rule of thumb, due to its widespread support and quite good security propertie, if one needs a symmetric cipher, one should use AES. 

\section{Security of Block ciphers}
Three different types of attacks exists against symmetric ciphers whose target is the key $k$:
\begin{itemize}
    \item Konwn plaintext attacks:\\
    The adversary gets to observe many plaintext/ciphertext pairs $(p,c)$ for a fixed key $k$ such that $E(k,p) = c$.
    \item Chosen plaintext attack:\\
    The adversary gets to choose many plaintexts $p$ and is given the corresponding ciphertext $c$ under a fixed key $k$ such that $E(k,p) = c$.
    \item Chosen ciphertext attack:\\
    The adversary gets to choose many ciphertexts $c$ and is given the corresponding plaintexts $p$ under a fixed key $k$ such that $D(k,c) = p$ 
\end{itemize}
Each of these scenarios allow the adversary to perform an exhaustive key search attack, since they possess pairs of plaintext and ciphertexts. Since each key $k$ yields a different pair of permutations $E(k, \cdot, D(k, \cdot$, one aspect of the security of a cipher can be determined by determining how different it looks like from a pair of random permutations. Even if the key space is smaller than the block space which forces the $E(k, \cdot, D(k, \cdot$ pair to be drawn from a way smaller set than the set of all possible permutations of a block.\\
Here we formalise the notion of looking random as the \textit{pseudo-randomness} property. 
\begin{definition}[Pseudo-randomness property (PRP)]
    An adversary $\mathcal{A}$ interacts with either $E(k, \cdot)$ for a random choice of the key $k$, or with a random permutation $\Pi$. The block cipher is said to be PRP if there is no efficient  $\mathcal{A}$ that can tell the difference between $E(k, \cdot)$ and $\Pi$. 
\end{definition}
\begin{definition}[Strong-pseudo-randomness property (strong-PRP)]    
    An adversary $\mathcal{A}$ interacts with either $E(k, \cdot), D(k, \cdot)$ for a random choice of the key $k$, or with a random permutation and its inverse $\Pi, \Pi^{-1}$. The block cipher is said to be strong-PRP if there is no efficient $\mathcal{A}$ that can tell the difference between $E(k, \cdot, D(k, \cdot)$ and $\Pi, \Pi^{-1}$
\end{definition}   
\begin{definition}[Adversary efficiency]  
The efficiency of $\mathcal{A}$ is quantified by the ressources consumed by $\mathcal{A}$: its running time and the number of queries made to the challenger ($E(k, \cdot, D(k, \cdot$ or $\Pi, \Pi^{-1}$)
\end{definition}
\begin{definition}[Advantage of the adversary]
    The advantage of the adversary represents their ability to distinguih between the random permutation and the cipher. It is the scaled probability that it is able to distinguish between the scenario. 
\end{definition}
\begin{definition}[Formal definition of PRP]\label{def:prp}
    A block cipher $E$ is defined to be $(q,t,\epsilon)$-secure as a PRP if for all adversaries $\mathcal{A}$ running in time at most $t$ and making $q$ queries to the oracle $F_N$ the advantage $\textbf{Adv}_{E}^{\text{PRP}}(\mathcal{A})$ is at most $\epsilon$. Where:
    \begin{align*}
        \textbf{Adv}_{E}^{\text{PRP}}(\mathcal{A}) = 2 \left\vert P\left( \textbf{Game} \ \texttt{PRP(}\mathcal{A}, E\texttt{)} \Rightarrow \texttt{True}\right) - \frac{1}{2}\right\vert
    \end{align*}
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{algorithm}[H]
                \SetKwFunction{Name}{PRP}
                \SetKwProg{Game}{Game}{:}{}
                \Game{\Name{$\mathcal{A}, E$}}{
                    $b \progets \{0,1\}$\;
                    $K \progets \{0,1\}^k$\;
                    $\Pi \progets \text{Perms}[\{0,1\}^n]$\;
                    $\hat{b} \progets \mathcal{A}^{F_N}()$\;
                    \KwRet $b = \hat{b}$\;
                }
            \end{algorithm}
        \end{minipage}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{algorithm}[H]
                \SetKwFunction{Name}{$F_N$}
                \SetKwProg{Oracle}{Oracle}{:}{}
                \Oracle{\Name{$x$}}{
                    \eIf{$b = 0$}{
                        $y \gets E(K,x)$\;
                    }{
                        $y \gets \Pi(x)$\;
                    }
                    \KwRet $y$\;
                }
            \end{algorithm}
        \end{minipage}
        \hfill
    \end{minipage}
\end{definition}
For specific block ciphers like AES, it we can't prove PRP, instead we must assume it in the absence of any cryptanalysis showing the contrary.

\section{AES}
As most other block ciphers, AES is constructed from the iteration of a simpler keyed round function. AES is a SP-network: each round function is built from a substitution step followed by a permutation step which directly implements Shannon's confusion and diffusion principles. The block size of AES is $n = 16$ bytes and the key size can be $k = 16$, 24, or 32 bytes. It is very efficient in hardware and no significant direct attacks are known, but implementations can be vulnerable to side channel attacks.\\
Several modes exist for AES which allow it to be used for data whose length is not exactly 16 bytes. Each of these modes have different performance characterictics, error propagation properties, and weaknesses.

\subsection{ECB mode}
The ECB (electronic code book) mode of AES is the simplest. As shown in Figure \ref{fig:ecb-diagram}, it simply splits the plaintext $p$ into blocks of 16 bytes $\{p_i\}_{i=1}^{b}$ (and possibly pad the last block) then encrypts each block under the same key: $c_i = E(K, p_i)$. The decryption is analoguous.

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[node distance=1.7cm]
        \node (p1) {$p_1$};
        \node[minimum size=1cm, draw, below of=p1 ] (ep1) {$E(K,\cdot)$};
        \draw[-stealth] (p1) -- (ep1);
        \node[below of=ep1] (c1) {$c_1$};
        \draw[-stealth] (ep1) -- (c1);

        \node[right of=p1, node distance = 2.5cm] (p2) {$p_2$};
        \node[minimum size=1cm, draw, below of=p2 ] (ep2) {$E(K,\cdot)$};
        \draw[-stealth] (p2) -- (ep2);
        \node[below of=ep2] (c2) {$c_2$};
        \draw[-stealth] (ep2) -- (c2);

        \node[right of=ep2, node distance = 2.5cm] (dots) {$\dots$};

        \node[minimum size=1cm, draw, right of=dots, node distance = 2.5cm] (epn) {$E(K,\cdot)$};
        \node[above of=epn] (pn) {$p_n$};
        \draw[-stealth] (pn) -- (epn);
        \node[below of=epn] (cn) {$c_n$};
        \draw[-stealth] (epn) -- (cn);
    \end{tikzpicture}
    \caption{AES in ECB mode}
    \label{fig:ecb-diagram}
\end{figure}

The ECB mode is a deterministic encription and trivially paralellizable, but for a fixed key $K$, a given block of plaintext is always encrypted into the same ciphertext, which can leas to serious information leakage in many application. Thus ECB is rarely the good mode to use. A good instance of the weakness of the ECB mode is the encryption of images in Figure \ref{fig:ecb-image-leak}.

\begin{figure}[!ht]
    \centering
    \begin{minipage}{.8\linewidth}
        \begin{minipage}{0.3\linewidth}
            \fig{fig/block_ciphers/Tux.jpg}
        \end{minipage}
        \hfill
        \begin{minipage}{0.3\linewidth}
            \fig{fig/block_ciphers/Tux_ecb.jpg}
        \end{minipage}
        \hfill
        \begin{minipage}{0.3\linewidth}
            \fig{fig/block_ciphers/Tux_secure.jpg}
        \end{minipage}
    \end{minipage}
    \caption{Image (left) encrypted with AES ECB mode (middle) and with another mode resulting in pseudo-randomness (right)\cite{WikipediaEN:ECB}}
    \label{fig:ecb-image-leak}
\end{figure}

\subsection{CBC mode}
The CBC (cipher block chaining) mode aims to hinder the information leakage of ECB mode. To do so, it uses the previous ciphertext block to randomise the input of the block cipher  at each application $c_i = E(K, p_i \oplus c_{i-1})$. $c_0$ is the initialisation vector ($IV$). Figure \ref{fig:cbc-diagram} shows a representation of the encryption process. Decryption is achieved by xorring the outputs of the decryption wth the previous ciphertext: $p_i = c_{i-1} \oplus D(K,c_i)$.

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[node distance = 1.5cm]
        \node (p1) {$p_1$};
        \node[below of= p1] (xor1) {$\oplus$};
        \node[left of=xor1] (iv) {$IV$};

        \draw[-stealth] (p1) -- (xor1);
        \draw[-stealth] (iv) -- (xor1);

        \node[minimum size=1cm, draw, below of=xor1 ] (ep1) {$E(K,\cdot)$};
        \draw[-stealth] (xor1) -- (ep1);
        \node[below of=ep1] (c1) {$c_1$};
        \draw[-stealth] (ep1) -- (c1);
        \node[below of= ep1, node distance = 0.85cm] (pc1) {$\cdot$};

        \node[right of=xor1] (hid) {};
        \node[right of= hid] (xor2) {$\oplus$};
        \draw[] (pc1) -| (hid.center);
        \draw[-stealth] (hid.center) -- (xor2);
        \node[above of =xor2] (p2) {$p_2$};
        \draw[-stealth] (p2) -- (xor2);
        \node[minimum size=1cm, draw, below of=xor2 ] (ep2) {$E(K,\cdot)$};
        \draw[-stealth] (xor2) -- (ep2);
        \node[below of=ep2] (c2) {$c_2$};
        \draw[-stealth] (ep2) -- (c2);
        \node[below of= ep2, node distance = 0.85cm] (pc2) {$\cdot$};

        \node[right of=xor2] (hid2) {};
        \node[right of= hid2] (dots) {$\dots$};
        \draw[] (pc2) -| (hid2.center);
        \draw[-stealth] (hid2.center) -- (dots);
    \end{tikzpicture}
    \caption{AES in CBC mode}
    \label{fig:cbc-diagram}
\end{figure}


\subsubsection{Error propagation in CBC}
In CBC, if an error occurs in the ciphertext. For instance if the $j$-th bit of $c_i$ is flipped, the corresponding plaintext $p_i$ is completely garbled, but the next plaintext $p_{i+1}$ will have its $j$-th bit flipped as well. More formally if $\tilde{c}_i = c_i \oplus \Delta$, then $\tilde{p}_{i+1} = p_{i+1} \oplus \Delta$. 

\subsubsection{Remarks}
The IV is needed for the decryption so it must be part of the transmitted ciphertext, hence why it is often denoted $c_0$. This fact leads to ciphertext expansion by one block. The IV should be uniformly randmo for each message encrypted because the lack of randomness leads to practical attacks. NB: this implies the need for a good source of randomness. \\
Since CBC modes only allow message of a length which is a multiple of the block size, padding is ncessary to achieve a proper length. This operation is however nontrivial since imporper padding can lead to practical attacks.\\
If the block size $n$ is too small, the CBC mode can become problematic because many encryptions will likely lead to ciphertext block collisions which enable attacks similar to the one-time pad reuse attack. These collisions start appearing after approximately $2^{n/2}$ encryptions by a birthday bound analysis.

\subsubsection{Attacks}
% later than CTR proof of sec

\subsection{CTR mode}
The CTR (counter) mode of AES uses a counter to generate pseudorandom blocks of output which are essentially turned into a bitstream. This bitstream is then used as a one-time pad to encrypt the plaintext. Figure \ref{fig:ctr-diagram} shows a representation of this process. Each block of ciphertext is computed by xorring the plaintext $p_i$ with the encryption of the counter value $ctr$, which is incremented for each block. Thus $c_i = E(K,ctr + i) \oplus p_i$.

\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[node distance = 1.5cm]
        \node (ctr0) {$ctr$};
        \node[minimum size=1cm, draw, below of=ctr0 ] (e0) {$E(K,\cdot)$};
        \node[below of=e0] (xor0) {$\oplus$};
        \node[left of=xor0] (p0) {$p_0$};
        \node[right of=xor0] (c0) {$c_0$};

        \draw[-stealth] (ctr0) -- (e0);
        \draw[-stealth] (e0) -- (xor0);
        \draw[-stealth] (p0) -- (xor0);
        \draw[-stealth] (xor0) -- (c0);


        \node[right of=ctr0, node distance=4cm] (ctr0) {$ctr + 1$};
        \node[minimum size=1cm, draw, below of=ctr0 ] (e0) {$E(K,\cdot)$};
        \node[below of=e0] (xor0) {$\oplus$};
        \node[left of=xor0] (p0) {$p_1$};
        \node[right of=xor0] (c0) {$c_1$};

        \draw[-stealth] (ctr0) -- (e0);
        \draw[-stealth] (e0) -- (xor0);
        \draw[-stealth] (p0) -- (xor0);
        \draw[-stealth] (xor0) -- (c0);

        \node[right of=e0, node distance= 3cm] (dots) {$\dots$}; 

        \node[right of=ctr0, node distance=6cm] (ctr0) {$ctr + l - 1$};
        \node[minimum size=1cm, draw, below of=ctr0 ] (e0) {$E(K,\cdot)$};
        \node[below of=e0] (xor0) {$\oplus$};
        \node[left of=xor0] (p0) {$p_l$};
        \node[right of=xor0] (c0) {$c_l$};

        \draw[-stealth] (ctr0) -- (e0);
        \draw[-stealth] (e0) -- (xor0);
        \draw[-stealth] (p0) -- (xor0);
        \draw[-stealth] (xor0) -- (c0);
    \end{tikzpicture}
    \caption{AES in CTR mode}
    \label{fig:ctr-diagram}
\end{figure}

This mode propagates the errors in the ciphertext directly to the plaintext. If $\tilde{c}_i = c_i \oplus \Delta$, then $\tilde{p}_i = p_i \oplus \Delta$. In this mode, no padding is necessary, but the initial counter value must be transmitted to the receiver along with the ciphertext if it is not available to the receiver by using synchronisation between the two parties. Fo this mode to remain secure, it is however important for all the counter values to be distinct accross each block, otherwise a problem similar to the one-time pad reuse problem arise.\\
The security of CTR mode relies on the pseudorandomness of the block cipher.

\subsubsection{Proof of IND-CPA-security of CTR mode}
In this proff we only shof the proof that a block cipher used in counter mode is secure to encrypt a message consisting of a single block. This proof can be extended to messages not strictly a multiple of the block size. In order to prove the IND-CPA-security of the CTR mode of a block cipher with $(q,t, \epsilon)$-PRP security we use a succession of four games: $G_0, \dots, G_3$. We show that from an adversary $\adv_1$ in $G_0$ we can construct an adversary $\adv_2$ that can win a PRP/PRF distinguishing game, which can, in turn be used to construct an adversary $\adv_3$ which can distinguish between a one-time pad encryption from random data. We also show that, during each transition, the adversary does not gain any significant advantage meaning that CTR mode is not much easier to break than one-time pads.
\begin{theorem}[IND-CPA security of CTR mode]
    If $E$ is $(q,t,\epsilon)$-secure as a PRP, then the CTR mode SE scheme based on $E$ is IND-CPA secure with parameters $(q,t,2\epsilon + 2\frac{q^2}{2^{n}})$.
    \begin{proof}
        Let $G_0$ be a game IND-CPA game, $G_1$ a PRP game, $G_2$ a PRP-PRF game, and $G_3$ a PRF-RAND game. Let $X_i$ denote the event that $\hat{b} = b$ in game $G_i$ and $P(X_i)$ as $q_i$. So:
        \begin{align*}
            \adva{E}{IND-CPA} &= \adva{E}{$G_0$}\\
            &= 2\abs{q_0 - \frac{1}{2}}\\
            &= 2\abs{(q_0 - q_1) + (q_1 - q_2) + (q_2 - q_3) + \left(q_3 - \frac{1}{2}\right)}\\
            & \leq 2 \left(\abs{q_0 - q_1} + \abs{q_1 - q_2} + \abs{q_2 - q_3} + \abs{q_3 - \frac{1}{2}}\right)\\
            &\stackrel{(*)}{=} 2 \left(\abs{q_0 - q_1} + \abs{q_1 - q_2} + \abs{q_2 - q_3} \right)
        \end{align*}
        ($\ast$) holds because one-time pad has perfect security so  $q_3 = \frac{1}{2}$.\\
        With lemmas \ref{lem:IND-CPA-to-PRP}, \ref{lem:PRP-to-PRF}, and \ref{lem:PRF-RAND} we have:
        \begin{align*}
            \adva{E}{IND-CPA} &\leq 2 \left(\abs{q_0 - q_1} + \abs{q_1 - q_2} + \abs{q_2 - q_3} \right)\\
            &\leq 2\left( \adva[B]{E}{PRP} + \frac{q^2}{2^{n+1}} + \frac{q^2}{2^{n+1}}\right)\\
            &= 2\adva[B]{E}{PRP} + 2\frac{q^2}{2^{n}}\\
            &\leq 2\epsilon + 2\frac{q^2}{2^{n}}
        \end{align*}
        Thus the CTR mode SE scheme based on $E$ is IND-CPA secure with parameters $(q,t,2\epsilon + 2\frac{q^2}{2^{n}})$.
    \end{proof}
\end{theorem}

\begin{lemma}[]\label{lem:IND-CPA-to-PRP}
    With an $(q,t,\epsilon)$-IND-CPA adversary $\adv$ we can construct a PRP adversary $\adv[B]$ able to distinguish between $G_0$ and $G_1$ such that $\abs{P(\hat{b} = b \vert G_0) - P(\hat{b} = b \vert G_1)} = \adva[B]{E}{PRP} \leq \epsilon$. 
    \begin{proof}
        In order to reduce a PRP adversary $\adv$ to a IND-CPA adversary $\adv[B]$, we use $\adv$ as a subroutine for $\adv[B]$. $\adv[B]$ works as follows:\\
        \begin{minipage}{\textwidth}
            \hfill
            \begin{minipage}{.47\textwidth}
                \begin{algorithm}[H]
                    \SetKwFunction{Name}{$\adv[B]$}
                    \SetKwProg{Game}{Adversary}{:}{}
                    \Game{\Name{$\mathcal{A}, SE$}}{
                        $b \progets \{0,1\}$\;
                        $ctr \progets \{0,1\}^n$\;
                        $r \progets F_N(ctr)$\;
                        $(m_0, m_1) \progets \adv()$\;
                        $c \gets m_b \oplus r$\;
                        $\hat{b} \progets \adv (ctr, c)$\;
                        \eIf{$\hat{b} = b$}{
                            $\hat{d} \gets 1$\; 
                        }{
                            $\hat{d} \gets 0$\;
                        }
                        \KwRet $\hat{d}$\;
                    }
                \end{algorithm}
            \end{minipage}
            \hfill
            \begin{minipage}{.47\textwidth}
                \begin{algorithm}[H]
                    $d \progets \{0,1\}$\;
                    $K \progets \{0,1\}^k$\;
                    $\Pi \progets \text{Perms}[\{0,1\}^n]$\;
                    \SetKwFunction{Name}{$F_N$}
                    \SetKwProg{Game}{Oracle}{:}{}
                    \Game{\Name{$x$}}{
                        \eIf{$d = 0$}{
                            $y \gets E(K, x)$\;
                        }{
                            $y \gets \Pi(x)$\;
                        }
                        \KwRet $y$\;
                    }
                \end{algorithm}
            \end{minipage}
        \end{minipage}
        Here $\adv[B]$ uses $\adv$'s output to determine the value of the hidden bit $d$. If $b = 1$, then $c$ will either correspond to $E(K,m_1)$ or to $\Pi(m_1)$. Thus if $\adv$ outputs $\hat{b} = 1$ when being provided $c$, then $d = 0$, otherwise if $\hat{b} = 0$, then $d = 1$. If $b = 0$, the same reasoning applies.\\
        More rigorously, if $d = 0$ $\adv$ plays in $G_0$, otherwise it plays in $G_1$ so:
        \begin{align*}
            P(\hat{b} = b \vert G_0) = P(\hat{b} = b \vert d = 0) = P(\hat{d} = 1 \vert d = 0) \\
            P(\hat{b} = b \vert G_1) = P(\hat{b} = b \vert d = 1) = P(\hat{d} = 1 \vert d = 1) \\
        \end{align*}
        Thus:
        \begin{align*}
            \abs{P(\hat{b} = b \vert G_0) - P(\hat{b} = b \vert G_1)} = \abs{(\hat{d} = 1 \vert d = 1) - P(\hat{d} = 1 \vert d = 0)} = \adva[B]{E}{PRP}
        \end{align*}
        Since $\adv[B]$ has the same runtime and number of queries as $\adv$ (up to a constant) and will only be mistaken whenever $\adv$ is mistaken hence: 
        \begin{align*}
            \adva[B]{E}{PRP} \leq \adva[A]{E}{IND-CPA} = \epsilon
        \end{align*}
    \end{proof}
\end{lemma}

\begin{lemma}[]\label{lem:PRP-to-PRF}
    With an $(q,t,\epsilon)$-IND-CPA adversary $\adv$ we can construct a PRP adversary $\adv[B]$ able to distinguish between $G_1$ and $G_2$ such that $\abs{P(\hat{b} = b \vert G_1) - P(\hat{b} = b \vert G_2)} = \adva[B]{}{PRP/PRF} \leq \frac{q^2}{2^{n+1}}$.
    \begin{proof}
        In this setup, $\adv[B]$ works similarly as in Lemma \ref{lem:IND-CPA-to-PRP}, but this time $d = 0$ implies $A$ is playing in $G_1$ and $d = 1$ $G_2$. Thus using the same notation change and using the advantage rewriting lemma (Lemma \ref{lem:adv-rewrite}), we get:
        \begin{align*}
            \abs{P(\hat{b} = b \vert G_1) - P(\hat{b} = b \vert G_2)} = \abs{(\hat{d} = 1 \vert d = 1) - P(\hat{d} = 1 \vert d = 0)} = \adva[B]{}{PRP/PRF}
        \end{align*}
        Using the PRP-PRF switching lemma (Lemma \ref{lem:prp-prf-switch}), we can bound  $\adva[B]{}{PRP/PRF}$ by $\frac{q^2}{2^{n+1}}$, thus:
        \begin{align*}
            \abs{P(\hat{b} = b \vert G_1) - P(\hat{b} = b \vert G_2)} = \adva[B]{}{PRP/PRF} \leq \frac{q^2}{2^{n+1}}
        \end{align*}
    \end{proof}
\end{lemma}

\begin{lemma}[]\label{lem:PRF-RAND}
    test

    \begin{proof}
        testtt
    \end{proof}
\end{lemma}

\section{IND-CPA security for symmetric encryption}
\begin{definition}[Symmetric encryption scheme]
    A symmetric encryption scheme consists of a triple of efficient algorithms: $SE = (KGen, Enc, Dec)$ each with the following charasteristics: 
    \begin{itemize}
        \item $KGen$: Key generation\\
        Typically selects a key $K$ uniformly at random from a set $\mathcal{K}$. In general we can assume witohout loss of generality that $\mathcal{K} = \{0,1\}^k$.
        \item $Enc$: Encryption\\
        Takes as input a key $K$, a plaintext $m \in \mathcal{M} \subseteq \{0,1\}^\ast$ and produces $c \in \mathcal{C} \subseteq \{0,1\}^\ast$.
        \item $Dec$: Decryption\\
        Takes as input a key $K$, a ciphertext $c \in \mathcal{C} \subseteq \{0,1\}^\ast$ and produces either $m \in \mathcal{M} \subseteq \{0,1\}^\ast$ or an error denoted $\perp$.
        \item Correctness\\
        For all keys $K$, and all plaintexts $m \in \mathcal{M} \subseteq \{0,1\}^\ast$, $Dec(K, Enc(K, m)) = m$.
    \end{itemize}
    $\mathcal{K}$ is the key space, $\mathcal{M}$ the message space, and $\mathcal{C}$ the ciphertext space. $Enc$ is typically randomised, but can also be made deterministic. $Dec$ is assumed to be deterministic. In reality there is a maximum plaintext length $L$ that can be encrypted by a given scheme so $\mathcal{M} \subseteq \{0,1\}^{\leq L}$.
\end{definition}
Intuitively, IND-CPA security for symmetric encryption is the computational version of the perfect security property. Here, the adversary is only required not to be able to efficiently compute anything useful about the plaintexts from the ciphertexts. Intuitively, IND-CPA is defined the inability of any efficient adversary $\mathcal{A}$, given the encryption of one of two equal length messages of its choice, to distinguish which one of the messages was encrypted.

\begin{definition}[IND-CPA security for SE]
    A symmetric encryption scheme $SE$ is said to be $(t,q,\epsilon)$-secure if, for all adversaries $\mathcal{A}$ running in time at most $t$ and making at most $q$ encryption queries, the advantage $\textbf{Adv}_{SE}^{\text{IND-CPA}}(\mathcal{A})$ is at most $\epsilon$. Where
    \begin{align*}
        \textbf{Adv}_{SE}^{\text{IND-CPA}}(\mathcal{A}) = 2 \left\vert P\left( \textbf{Game} \ \texttt{PRP(}\mathcal{A}, E\texttt{)} \Rightarrow \texttt{True}\right) - \frac{1}{2}\right\vert
    \end{align*}
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{algorithm}[H]
                \SetKwFunction{Name}{IND-CPA}
                \SetKwProg{Game}{Game}{:}{}
                \Game{\Name{$\mathcal{A}, SE$}}{
                    $b \progets \{0,1\}$\;
                    $K \progets \{0,1\}^k$\;
                    $\hat{b} \progets \mathcal{A}^{LoR(\cdot, \cdot)}()$\;
                    \KwRet $b = \hat{b}$\;
                }
            \end{algorithm}
        \end{minipage}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{algorithm}[H]
                \SetKwFunction{Name}{$LoR$}
                \SetKwProg{Oracle}{Oracle}{:}{}
                \Oracle{\Name{$m_0, m_1$}}{
                    $c \gets Enc(K, m_b)$\;
                    \KwRet{$c$}
                }
            \end{algorithm}
        \end{minipage}
        \hfill
    \end{minipage}
\end{definition}
\begin{itemize}
    \item It can be proved that CBC and CTR modes of AES meet the IND-CPA security definition if used properly and if they are built using a good block cipher.
    \item The definition says nothing about the integrity of the plaintexts nor does it say anything about different length plaintexts.
    \item No deterministic cipher can meet the IND-CPA security definition because the adversary could ask for the two subsequent queries $LoR(m_0, m_0) = c_0$ then $LoR(m_0, m_1) = c_1$ and output 1 if $c_0 \neq c_1$ and 0 otherwise.
\end{itemize}

\section{PRP-PRF switching lemma}
The PRP-PRF switching lemma states the fact that pseudorandom permutations (PRP) are indistinguishable from pseudorandom functions (PRF). In order to prove the PRP-PRF switching lemma, we need to smaller technical lemmas: the advantage rewriting lemma (Lemma \ref{lem:adv-rewrite}), and the difference lemma (Lemma \ref{lem:diff-lemma}). 
We use these two lemmas to establish that it is possible to consider pseudorandom permutations as pseudorandom gunction without loosing too much to the adversary. 

\begin{definition}[PRF security]
    PRF security is very similar to the PRP definition (\ref{def:prp}), except, in this case, the oracle returns a random function of its input when $b = 1$ instead of a random permutation.\\
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{algorithm}[H]
                \SetKwFunction{Name}{PRF}
                \SetKwProg{Game}{Game}{:}{}
                \Game{\Name{$\mathcal{A}, E$}}{
                    $b \progets \{0,1\}$\;
                    $K \progets \{0,1\}^k$\;
                    $f \progets \text{Funcs}[\{0,1\}^n, \{0,1\}^n]$\;
                    $\hat{b} \progets \mathcal{A}^{F_N}()$\;
                    \KwRet $b = \hat{b}$\;
                }
            \end{algorithm}
        \end{minipage}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{algorithm}[H]
                \SetKwFunction{Name}{$F_N$}
                \SetKwProg{Oracle}{Oracle}{:}{}
                \Oracle{\Name{$x$}}{
                    \eIf{$b = 0$}{
                        $y \gets E(K,x)$\;
                    }{
                        $y \gets f(x)$\;
                    }
                    \KwRet $y$\;
                }
            \end{algorithm}
        \end{minipage}
        \hfill
    \end{minipage}
    Thus  block cipher $E$ is defined to be $(q,t,\epsilon)$-secure as a PRF if for all adversaries $\mathcal{A}$ running in time at most $t$ and making $q$ queries to the oracle $F_N$ the advantage $\textbf{Adv}_{E}^{\text{PRF}}(\mathcal{A})$ is at most $\epsilon$. Where:
    \begin{align*}
        \textbf{Adv}_{E}^{\text{PRF}}(\mathcal{A}) = 2 \left\vert P\left( \textbf{Game} \ \texttt{PRF(}\mathcal{A}, E\texttt{)} \Rightarrow \texttt{True}\right) - \frac{1}{2}\right\vert
    \end{align*}
\end{definition}

\begin{theorem}[PRP-PRF switching lemma]\label{lem:prp-prf-switch}
    Let $E$ be a block cipher. Then for any adversary $\mathcal{A}$ making $q$ queries:
    \begin{align*}
        \abs{\textbf{Adv}_{E}^{\text{PRP}}(\mathcal{A}) - \textbf{Adv}_{E}^{\text{PRF}}(\mathcal{A})} \leq \frac{q^2}{2^{n+1}}
    \end{align*}
    \begin{proof}
        Let $\adv$ be a $(q,t,\epsilon)$ adversary. $\adv$  will be run on three different games which only differ by the function used to answer to $\adv$'s queries by the oracle: 
        \begin{itemize}
            \item $G_0$: choose $f = E(K, \cdot)$, with $K \progets \{0,1\}^k$
            \item $G_1$: choose $f \progets \text{Prems}[\{0,1\}^n]$
            \item $G_2$: choose $f \progets \text{Funcs}[\{0,1\}^n, \{0,1\}^n]$
        \end{itemize}
        With this definition, $G_0$ corresponds to the case $b=0$ in eother the PRF or PRP game, $G_1$ to $b = 1$ in the PRP game, and $G_2$ to $b=1$ in the PRF game. Let $W_i$ be the event that $\adv$ outputs $\hat{b} = 1$ in game $G_i$ and for ease of notation we write $p_i  = P(W_i)$. Thus, by the advantage rewriting lemma (Lemma \ref{lem:adv-rewrite}):
        \begin{align*}
            \abs{P(\hat{b} = 1 \vert G_1) - P(\hat{b} = 1 \vert G_0)} &= \abs{p_1 - p_0}\\
            &= \abs{P(\hat{b} = 1 \vert b = 1) - P(\hat{b} = 1 \vert b = 0)}\\
            &= \adva{E}{PRP}
        \end{align*}
        And:
        \begin{align*}
            \abs{P(\hat{b} = 1 \vert G_2) - P(\hat{b} = 1 \vert G_0)} &= \abs{p_1 - p_0}\\
            &= \abs{P(\hat{b} = 1 \vert b = 1) - P(\hat{b} = 1 \vert b = 0)}\\
            &= \adva{E}{PRF}
        \end{align*}
        Hence
        \begin{align}
            \abs{\adva{E}{PRP} - \adva{E}{PRF}} &= \abs{\abs{p_1 - p_0} - \abs{p_2 - p_0}}\\
            &\leq \abs{p_2 - p_1}\label{eqn:prp-prf-proof-adv-diff}
        \end{align}
        Now, $G_1$ and $G_2$ are indistinguishable unless a repeated value occurs amongst the output of the oracle under different inputs (i.e. $F_N(x_1) = F_N(x_2)$). Let $Z$ denote this event. This implies that the event $(W_1 \wedge Z)$ occurs iff $(W_2 \wedge Z)$ occurs. A basic probability analysis shows that $P(Z) \leq \frac{q^2}{2^{n+1}}$. hence with the difference lemma (Lemma \ref{lem:diff-lemma}) we obtain that:
        \begin{align*}
            \abs{p_2 - p_1} = \abs{P(W_2) - P(W_1)} \leq P(Z) \leq \frac{q^2}{2^{n+1}}
        \end{align*} 
        Thus, with (\ref{eqn:prp-prf-proof-adv-diff}), we obtain:
        \begin{align*}
            \abs{\adva{E}{PRP} - \adva{E}{PRF}} \leq \frac{q^2}{2^{n+1}}
        \end{align*}
    \end{proof}
\end{theorem}

\begin{lemma}[Advantage rewriting lemma]\label{lem:adv-rewrite}
    Let $b$ be a uniformly random bit. Let $\hat{b}$ be the output of some algorithm. Then:
    \begin{align*}
        2 \left\vert P\left(\hat{b} = b \right) - \frac{1}{2} \right\vert = \left\vert P\left(\hat{b} = 1 \vert b = 1\right) - P\left(\hat{b} = 1 \vert b = 0\right)\right\vert
    \end{align*}

    \begin{proof}
        \begin{align*}
            P(\hat{b} = b) - \frac{1}{2} &= P\left(\hat{b} = b \vert b = 1\right)P\left(b = 1\right) + P\left(\hat{b} = b \vert b = 0\right)P\left(b = 0\right) - \frac{1}{2}\\
            &= \frac{1}{2} \left(P\left(\hat{b} = 1 \vert b = 1\right) + P\left(\hat{b} = 0 \vert b = 0\right) - 1\right) \\
            &= \frac{1}{2}\left(P\left(\hat{b} = 1 \vert b = 1\right) - P\left(\hat{b} = 1 \vert b = 0\right)\right)\\
            \Longrightarrow 2 \left\vert P\left(\hat{b} = b \right) - \frac{1}{2} \right\vert &= \left\vert P\left(\hat{b} = 1 \vert b = 1\right) - P\left(\hat{b} = 1 \vert b = 0\right)\right\vert 
        \end{align*}
    \end{proof}
\end{lemma} 
The difference lemma can be used to change games while proving the advatage of the adversary stays close to the advantage in a previous game. In particular if we choose the event $Z$ to be a rare bad event, $W_1$ the provbability of success of an adversary $\mathcal{A}$ in some game $G_1$, and $W_2$ $\mathcal{A}$'s probability of success in some modified game $G_2$. If $Z$ is sufficiently rare, then we can show the advanatage of $\mathcal{A}$ in $G_1$ and $G_2$ are close.  
\begin{lemma}[Difference lemma]\label{lem:diff-lemma}
    Let $Z, W_1, W_2$ be any events defined over some probability space such that:
    \begin{center}
        $(W \wedge \neg Z)$ occurs iff, $(W_2 \wedge \neg Z)$ occurs.
    \end{center}
    Then: 
    \begin{align*}
        \left\vert P(W_2) - P(W_1) \right\vert \leq P(Z)
    \end{align*}

    \begin{proof}
        \begin{align*}
            \abs{P(W_2) - P(W_1)} &= \abs{P(W_2 \wedge Z) + P(W_2 \wedge \neg Z) - P(W_1 \wedge Z) + P(W_1 \wedge \neg Z)}\\
            &= \abs{P(W_2 \wedge Z) - P(W_1 \wedge Z)}\\
            &\leq P(Z)
        \end{align*}
    \end{proof}
\end{lemma}