\setcounter{figure}{0}
\setcounter{Listing}{0}
\setcounter{table}{0}
\setcounter{Equation}{0}

\chapter{Dimension reduction}

\section{Introduction}

\subsection{Data representation}

The data is assumed to be issued from an implicit probability distribution $\nu$ i.e. $\mathbf{x} \sim \nu$. This distribution is unknown, but the sample set $\mathcal{S} = \lbrace \mathbf{x}_i \iid \nu, \  i =  1,2, \dots, s\rbrace$ is known. The true expectation $\mathbb{E}_{\nu}\left[f(\mathbf{x})\right]$ is defined over the distribution and the empirical expectation $\mathbb{E}_{\mathcal{S}}\left[f(\mathbf{x})\right]$ is defined over the sample set and is simply a mean.

The motivation for dimension reduction is to find low-dimension representation for high dimension data. This low-dimensional representation is useful to reach more interpretable representation and to compress the data while preserving relevant information.

\subsection{Auto-encoders}

\begin{figure}[H]
  \centering
  \input{res/1-dimred/tikz/auto-encoder.tex}
  \caption{Graphical representation of an auto-encoder.}
  \label{fig:1-auto-encoder}
\end{figure}

From a Deep neural network standpoint, dimension reduction can be represented with an auto-encoder as shown in Figure \ref{fig:1-auto-encoder}. The objective of an auto-encoder is to reconstruct an input at its output while the dimension is reduced in the middle of the auto-encoder, after the encoder stage. An auto-encoder $f  = (h \circ g): \mathbb{R}^n \mapsto \mathbb{R}^n$ is defined more rigorously as a pair of an encoder $g : \mathbb{R}^n \mapsto \mathbb{R}^m$ and a decoder $h : \mathbb{R}^m \mapsto \mathbb{R}^n$ functions applied on input $\mathbf{x}$. In order to achieve dimension reduction, $m << n$.

The ideal auto-encoder is the identity function. However, this is often impossible to achieve depending on the dimensionality of the data in the sample set. Thus, to evaluate the performance of the auto-encoder, we use a loss function $\ell$ defined in Equation \ref{eq:1-loss} to measure the distortion the auto-encoder applies to the inputs. 

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/loss-fn.tex}
  \caption{Definition of a general loss function.}
  \label{eq:1-loss}
\end{Equation}

Depending on the domain, different loss functions are used. For our example, the quadratic loss $\ell_2$ is a convenient choice and its definition is shown in Equation \ref{eq:1-l2}. 

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/l2.tex}
  \caption{Definition of the quadratic loss function.}
  \label{eq:1-l2}
\end{Equation}

Where $\Vert \cdot \Vert$ is the $\ell^2$-norm i.e. \( \forall \, \mathbf{a} \in \mathbb{R}^k,\ \Vert \mathbf{a} \Vert = \sum_{i=1}^k a_i^2\).

With this loss function we can define the risk. Again, since the data can be both represented as its true distribution or the sample set, we define two risk the true risk $\mathbb{E}_\nu\left[\ell\right]$ and the empirical risk $\mathbb{E}_{\mathcal{S}}\left[\ell\right]$ both defined in equation \ref{eq:1-risks}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/risks.tex}
  \caption{True and empirical risk distribution.}
  \label{eq:1-risks}
\end{Equation}

\section{Linear auto-encoder}

Fort start, we will look into one of the simplest form of auto-encoders. In this simple setup, both the encoder and decoder have a single linear layer. i.e. both $g: \Rn \mapsto \Rm$ and $h : \Rm \mapsto \Rn$ are linear functions. Hence, there exists two $n \times m $ matrices $\mathbf{W}$ and $\mathbf{V}^\top$ such that:

\begin{Equation}[H]
    \centering
    \input{res/1-dimred/eqn/mtx-form.tex}
    \caption{Matrix expression for the linear auto-encoder.}
    \label{eq:1-mtx-form}
\end{Equation}

The objective if the minimization of the empirical risk using the quadratic norm. Here we stress the importance of defining the risk function as a function of both $\mathbf{V}$ and $\mathbf{W}$, not only $\mathbf{P}$ because doing so would reduce the expressiveness of the model as many $\mathbf{V}$ and $\mathbf{W}$ multiply to the same matrix $\mathbf{P}$.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/risk-ae.tex}
  \caption{Risk function for the auto-encoder.}
  \label{eq:1-risk-ae}
\end{Equation}

\subsection{Note on affine auto-encoders}
At first glance we might think that the linearity constraint of $g$ and $h$ yields a loss of expressiveness for the model. This is however not the case when the data is centered, i.e. $\mathbb{E}_{\mathcal{S}}(\mathbf{x}) = 0$. This fact is proved in Equation \ref{eq:1-affinity-useless}. Assuming $\mathbf{a} \in \Rn^\ast$ and $\mathbf{P}:=\mathbf{V}\mathbf{W} \in \mathbb{R}^{n \times n}$ minimizes the risk. Given $\mathbb{E}_{\mathcal{S}}(\mathbf{x}) = 0$, Equation \ref{eq:1-affinity-useless} proves the risk is not minimized.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/affinity-useless.tex}
  \caption{Proof that bias does not increase expressiveness for centered data.}
  \label{eq:1-affinity-useless}
\end{Equation}

Centering the data is simply performed by computing the empirical expectation of the data by taking its mean and subtracting it from every data point. Given the dataset $\mathcal{S}$, the centered data $\mathcal{\tilde{S}}$ is computed as shown in Equation \ref{eq:1-centering}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/centering.tex}
  \caption{Centering the data.}
  \label{eq:1-centering}
\end{Equation}

\subsection{(Non-)Identifiability}
Three main questions arise when conceptualizing the linear auto-encoder with a matrix product. 
\begin{itemize}
  \item Is this representation as a sub-rank matrix $\mathbf{P} = \mathbf{V}\mathbf{W}$ unique ? 
  \item Is the optimal linear reconstruction map $\mathbf{V}$ unique ?
  \item Is the parametrization with weight matrices $\mathbf{V}, \mathbf{W}$ unique ?
\end{itemize}

First we note the last question is simply answered by noticing the fact stated in Equation \ref{eq:1-param-nonunique} for all invertible $\mathbf{A} \in \mathbb{R}^{m \times m}$.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/param-nonunique.tex}
  \caption{Non-uniqueness of the auto-encoder parametrization.}
  \label{eq:1-param-nonunique}
\end{Equation}

Since the weight matrices are non-identifiable, we instead focus on a constrained class of square matrices $\mathbf{P}$ and postpone the question on how to split into two weight matrices.

\subsection{Rank constraint}
The main constraint on $\mathbf{P}$ is its rank. Since $\mathbf{P}$ is still conceptually the product of a $\mathbf{V} \in \mathbb{R}^{n \times m}$ and a $\mathbf{W} \in \mathbb{R}^{m \times n}$ matrix, the rank of $\mathbf{P}$ is constrained as shown in Equation \ref{eq:1-rank-P}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/rank-P.tex}
  \caption{Rank constraint on $\mathbf{P}$.}
  \label{eq:1-rank-P}
\end{Equation}

\section{Projections}
The rank constraint on $\mathbf{P}$ is in dependent of the loss function. At the moment the only limitations are the rank constraint and the linearity of $\mathbf{P}$. Thus, $\im(\mathbf{P})$ is a linear subspace of $\Rn$ of dimension $\leq m$.

\subsection{Optimal linear map}
Given the square loss as our optimality metric, $U \subseteq \Rn$, and $\dim(U) = m$, the optimal linear map $\mathbf{P}^\ast$ is defined in Equation \ref{eq:1-opt-map}. The specific choice of $U$ is postponed.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/opt-map.tex}
  \caption{Definition of the optimal linear map given the subspace $U$.}
  \label{eq:1-opt-map}
\end{Equation}

For any arbitrary subspace $U \subseteq \Rn$, the optimal projection map which minimizes the square loss is the orthogonal projection $\Pi_U : \Rn \mapsto U$. The projection is defined in Equation \ref{eq:1-orth-proj}. This projection is unique as only one $\mathbf{x'} \in U$ minimizes $\Vert \mathbf{x} - \mathbf{x'} \Vert$. Moreover, it is called orthogonal because $\forall \mathbf{x} \in \Rn$, $\Pi_U(\mathbf{x}) - \mathbf{x} \in U^\bot$. Where $U^\bot = (\Rn \setminus U) \cup \{\mathbf{0}\}$.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/orth-proj.tex}
  \caption{Definition of the orthogonal projection.}
  \label{eq:1-orth-proj}
\end{Equation}

\subsubsection{Properties of the orthogonal projection}
The orthogonal projection is idempotent, meaning if $\mathbf{A}$ is the orthogonal projection matrix, then $\mathbf{A}\mathbf{A} = \mathbf{A}$. This is easily explained by highlighting the fact that when a vector is already in $U$, the projection operator behaves as the identity.

The orthogonal projection is homogenous. i.e. $\Pi_U(\alpha \mathbf{x}) = \alpha \Pi_U(\mathbf{x})$. This property arises from the homogeneity of the squared norm which is used for the orthogonal projection definition.

The orthogonal projection is additive. i.e. $\Pi_U(\mathbf{x} + \mathbf{y}) = \Pi_U(\mathbf{x}) + \Pi_U( \mathbf{y})$.

Thus, for a given subspace $U \subseteq \Rn$, the optimal reconstruction map is the matrix $\mathbf{P}$ which represents the orthogonal projection $\Pi_U$ onto $U$.

\subsection{Projection onto a line}
In the simplest case where $U$ is only one-dimentional and is the span of a unit-length vector $\mathbf{u}$, the orthogonal projection matrix $\mathbf{P}$ is simply defined as $\mathbf{P}_{\mathbf{u}} = \mathbf{u}\mathbf{u}^\top$. Note that by associativity $\mathbf{P}_\mathbf{u}\mathbf{x} = \langle \mathbf{u} , \mathbf{x}\rangle\mathbf{u}$. 

\subsection{Projection onto a space}
Let $\{\mathbf{u}_1,\mathbf{u}_2,\dots,\mathbf{u}_m\}$ be an orthonormal basis for $U$ and define $\mathbf{P} =\mathbf{U}\mathbf{U}^\top$ where $\mathbf{U} = [\mathbf{u}_1,\mathbf{u}_2,\dots,\mathbf{u}_m]$. $\mathbf{P}$ is self adjoint i.e. $\mathbf{P}^\top = \mathbf{P}$ and idempotent for the same reasons as for the projection onto a line.

$\mathbf{P}$ is a matrix representation of the projection $\Pi_{U}$. The proof for this claim stems from $\mathbf{P}$ clearly spanning $U$ and the orthogonality is shown in Equation \ref{eq:1-proof-proj}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/proof-proj.tex}
  \caption{Proof that $\mathbf{P}$ is $\Pi_U$.}
  \label{eq:1-proof-proj}
\end{Equation}

\paragraph*{Remark :}It is not necessary for the basis contained in $\mathbf{P}$ to be orthonormal for $\mathbf{P}$ to be the optimal reconstruction map. However, if we constrain $\mathbf{V}$ to be $\mathbf{V} = \mathbf{W}^\top$ with parameter sharing, then the basis is orthonormal.

\subsection{Oblique projection matrices} 
If the basis $\{\mathbf{v}_1,\mathbf{v}_2,\dots,\mathbf{v}_m\}$ is not orthonormal, it is still possible to recover the projection matrix. In that case $\mathbf{P}$ is defined as in the orthonormal case with $\mathbf{V} = [\mathbf{v}_1,\mathbf{v}_2,\dots,\mathbf{v}_m]$. However, $\mathbf{P} = \mathbf{V}\mathbf{V}^\dagger$ is the projection matrix for $U = \spn\{\mathbf{v}_1,\mathbf{v}_2,\dots,\mathbf{v}_m\}$. Where $\mathbf{V}^\dagger$ is the Moore-Penrose left pseudo-inverse defined as $\mathbf{V}^\dagger = (\mathbf{V}^\top\mathbf{V})^{-1}\mathbf{V}^\top $.

A simple corollary of this fact is that for any $\mathbf{V} \in \mathbb{R}^{n \times m}$, the left Moore-Penrose pseudo-inverse is the optimal choice for $\bW$ which minimizes the squared loss.

\section{Optimal subspace}
Until now, we looked at the optimal projection onto a given subspace, now is the time to find the optimal subspace to project onto. The goal now is to minimize the expectation $\mathbb{E}\Vert \bP\bx - \bx \Vert^2$, with $\bP = \bU\bU^\top$.

The expected loss $\mathbb{E}\Vert \bP\bx - \bx \Vert^2$ can be expressed the difference between the variance of the data and the variance of the projected data. Equation \ref{eq:1-loss-as-var} shows a proof of this statement.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/loss-as-var.tex}
  \caption{Proof showing loss can be express as variance difference.}
  \label{eq:1-loss-as-var}
\end{Equation}

This means minimizing the loss of the projection is the same as maximizing the variance of the projected data. The optimal auto-encoder is the projection $\bP$ that maximizes the variance of the projected data.

In order to evaluate the performance of the auto-encoder the variance of the projected data is the sufficient statistic. The variance can be obtained as shown in Equation \ref{eq:1-var-tr} with the data covariance matrix $\mathbf{\Sigma} = \E[\bx\bx^\top]$.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/var-tr.tex}
  \caption{Expression for the variance.}
  \label{eq:1-var-tr}
\end{Equation}

\subsection{Principal Component Analysis (PCA)}

\paragraph*{PCA theorem} The rank $m$ projection minimizing the squared reconstruction loss on centered data and maximizing the variance of the projected data is given by the projection matrix formed by $m$ orthogonal principal eigenvectors of the data covariance matrix $\mathbf{\Sigma} = \E[\bx\bx^\top]$.

This theorem is proved in the rest of this section.

\subsection{Diagonal covariance matrix PCA}

In the simplest case where the data covariance matrix is diagonal, i.e. $\mathbf{\Sigma} = \E[\bx\bx^\top] = \mathbf{\Lambda} = \text{diag}(\lambda_1, \lambda_2, \dots, \lambda_n)$, where $\lambda_1 \geq \lambda_2 \geq \dots \geq \lambda_n \geq 0$. A set of $m$ orthogonal principal eigenvectors is given by the unit vectors $\{\be_1, \be_2, \dots, \be_m\}$.

\paragraph*{Proof} The unit vectors are orthogonal, they are eigenvectors of any diagonal matrix ($\bLam\be_j = \lambda_j\be_j$), the decreasing ordering of the eigenvalues induces the precedence of the unit vectors. However, due to multiplicity, the choice of eigenvectors might be not unique (for this, the degenerate case is $\bLam = \alpha\bI$ where all eigenvalues are the same thus any choice of unit vectors yields the same loss).

Then, in this special case, the optimal rank $m$ projection matrix which minimizes the reconstruction loss and maximizes the variance of the projected data is shown in Equation \ref{eq:1-opt-diag-proj}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/opt-diag-proj.tex}
  \caption{Optimal rank $m$ projection matrix for diagonal covariance matrices.}
  \label{eq:1-opt-diag-proj}
\end{Equation}

\subsection{Positive semi-definite symmetric matrices}

Let us first note that $\mathbf{\Sigma} = \E[\bx\bx^\top]$ is by definition positive semi-definite and symmetric. 

The spectral theorem states that any (positive semi-definite) symmetric matrix $\bA$ can be (positively) diagonalized into $\bA = \bQ\bLam\bQ^\top$. Where $\bLam = \text{diag}(\lambda_1, \lambda_2, \dots, \lambda_n)$ and $\bQ$ is an orthogonal matrix.

Using the spectral theorem, the best rank $m$ projection matrix $\bP$ is defined as shown in Equation \ref{eq:1-pca-gen} using the spectral decomposition $\mathbf{\Sigma} = \E[\bx\bx^\top] = = \bQ\bLam\bQ^\top$.


\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/pca-gen.tex}
  \caption{Optimal rank $m$ projection matrix for symmetric covariance matrices.}
  \label{eq:1-pca-gen}
\end{Equation}

\section{Learning algorithms}
Many exact algorithms exist to diagonalize matrices and are implemented in readily available libraries for nearly every possible programming language. These algorithms are however not particularly efficient, specially for large matrices. This is why other iterative methods are possible to approach the exact diagonalization.

\subsection{Power method}
The first iterative approach to matrix diagonalization is the power method. It is a method to find the largest eigenvector and eigenvalue. Once the largest is found, the process can be repeated for the second eigenvector once the direction of the eigenvector is nulled. This method functions by initializing a random vector $\bv^{(0)} \sim \mathcal{N}(\mathbf{0}, \bI)$ and then iterating the formula shown in Equation \ref{eq:1-pwr-method}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/pwr-method.tex}
  \caption{Iteration of the power method.}
  \label{eq:1-pwr-method}
\end{Equation}

An intuitive explanation as to why this process approaches the principal eigenvector is because at each step, the component of the vector that is in the direction of the principal eigenvector is stretched the most while the other directions are stretched less. Over time, since the matrix vector product is normalized at each step, $\bv$ approaches the principal eigenvector at each iteration.

More formally, given $\bu_1 \in \Rn$ the principal eigenvector of a diagonalizable matrix $\bA \in \mathbb{R}^{n \times n}$ with eigenvalues $\lambda_1 > \lambda_2 \geq \dots \geq \lambda_n$, if $\langle\bv^{(0)}, \bu_1 \rangle \neq 0$ then 

\begin{align*}
  \lim_{t \rightarrow \infty} \bv^{(t)} = \bu_1
\end{align*}

Because by expressing $\bv^{(0)}$ in the eigenbasis we have $ \bv^{(0)} = \sum_{i} \alpha_i\bu_i$ for some appropriate $\alpha_i$. Using this fact, Equation \ref{eq:1-pf-pwr} shows the components of $\bv^{(k)}$ not collinear to $\bu_1$ vanish as $k$ grows.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/pf-pwr.tex}
  \caption{Only the components of $\bv^{(k)}$ collinear to $\bu_1$ are kept.}
  \label{eq:1-pf-pwr}
\end{Equation}

One of the big issues with the power method is its cost for large datasets with high dimensionality.

\subsection{Gradient descent}

Gradient descent is one of the most generic and scalable method for optimization. Generally speaking the idea behind gradient descent is to start with a random guess, then to compute the loss and the gradient of the loss, and finally update the initial guess in the direction indicated by the gradient. After several iterations, if the objective is convex, the optimum is attained. If the objective is not convex, a local minimum is found, but there are no guarantees the global minimum is attained.

One big advantage of the gradient descent is its stochastic version. With stochastic gradient descent, the loss is only computed on a subset of the dataset instead of the whole dataset and the subset is different on each iteration. This version is extremely scalable.

\subsubsection{Gradient computation}

Here the loss is defined as in Equation \ref{eq:1-loss2}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/loss.tex}
  \caption{$\ell_2$ loss for linear auto-encoder.}
  \label{eq:1-loss2}
\end{Equation}

With the differentiation rules shown in Equation \ref{eq:1-diff-rules} the gradient of the loss with respect to $\bP$ is shown derived in Equation \ref{eq:1-grad-loss}

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/diff-rules.tex}
  \caption{Matrix differentiation rules.}
  \label{eq:1-diff-rules}
\end{Equation}

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/grad-loss.tex}
  \caption{Gradient of the $\ell_2$ loss with respect to $\bP$.}
  \label{eq:1-grad-loss}
\end{Equation}

Now in order to find the gradients with respect to $\bV$ and $\bW$ we use the chain rules for matrix differentiation which yields the two gradients shown in Equation \ref{eq:1-grad-loss-vw}.

\begin{Equation}[H]
  \centering
  \input{res/1-dimred/eqn/grad-loss-vw.tex}
  \caption{Gradient of the $\ell_2$ loss with respect to $\bV$ and $\bW$.}
  \label{eq:1-grad-loss-vw}
\end{Equation}

At each step of the stochastic gradient descent both $\bV$ and $\bW$ are updated by subtracting a fixed fraction of their respective gradient with respect to a random data point. This fixed fraction is called the learning rate and is often denoted $\eta$. It is used to slow the descent to avoid oscillating around the optimum.

