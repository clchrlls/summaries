\setcounter{figure}{0}
\setcounter{Listing}{0}
\setcounter{table}{0}
\setcounter{Equation}{0}

\chapter{Matrix approximation}

Given a matrix $\bA$ whose entries are only partially observed, i.e. some entries are unobserved, the goal of Matrix approximation is to reconstruct the missing entries from the observed ones. Ideally to match the values of the unobserved entries. The classical application for this problem is to build recommender systems. 

The recommender system is a problem where there is a collection of user ratings on items and the goal is to recommend the best item to users given their previous ratings and the ratings of other users on yet unrated items. In this instance, the ratings are represented with a matrix containing the ratings and whose columns and rows represent the users and the items.

\section{Problem setup}

\subsection{Assumptions}
The first naive assumption would be to assume independence of the entries of the matrix with respect to all other entries. This assumption is however not useful because under it there would be no way to reconstruct any other entry because there would not be any mutual information between any pair of entries. Thus, more assumption are needed for the completion of this task.

One of the most minimal assumptions is that entries in the same columns depend on each other and same for the rows. In the item recommender scenario it makes sense that users have preferences thus the ratings of a user are not independent of each other. The same goes with the items which are inherently more or less bad. This conditional independence can be formulated more precisely as in Equation \ref{eq:2-cond-indep}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/cond-indep.tex}
  \caption{Minimal conditional independence assumption.}
  \label{eq:2-cond-indep}
\end{Equation}

It is important to note that conditional independence does \textbf{not} imply marginal independence between $a_{ij}$ and $a_{kl}$. In the matrix completion case, most of the matrix entries are unknown. It is thus impossible to perform marginalization.

\subsection{Problem formulation}

Given a set of $n$ users and $m$ items we construct a data matrix $\bA \in \Rnm$ matrix. The users are represented using the rows and the items the columns of the matrix. The observation matrix $\mathbf{\Omega} \in \{0,1\}^{n \times m}$ represents the observations of the matrix. i.e. $\omega_{ij} = 1 \Leftrightarrow a_{ij}$ is observed and $\sum_{ij} \omega_{ij}$ is the number of observations. In typical instances, the number of observed ratings (or the sparseness) is less than 1\%.

\subsection{Data standardization}
It is common for many approaches to solve this problem to expect centered and unit variance data. There are however different ways the dataset can be standardized. This practice is also useful for pre-training, or simply comparing ratings with different rating systems or scales. This process is very simply reversible when the prediction are made.

The dataset can be centered using the global observed ratings means or by user (rows) or by item (columns). Equation \ref{eq:2-centering} shows the different means computations. Centering is then done by subtracting the chosen mean from all entries of the matrix. In the user-item rating example both centering by user and item make sense. Centering by user can get rid of the user overall tendency to rate items higher or lower. For items this also makes sense since to account for the overall global quality of the item.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/centering.tex}
  \caption{Different centering means.}
  \label{eq:2-centering}
\end{Equation}

Similarly to the centering, variance normalization is also a useful and common practice. In order to normalize the variance it is brought out to unit variance by simply dividing the entries of the matrix by the variance of the data. This can also be carried out using the global, row, or column variance.

Overall the normalized data (or $z$-score) $\tilde{a}_{ij} = (a_{ij} - \mu) / \sigma$, is computed using the entries $a_{ij}$ of the data matrix and with $\mu$ and $\sigma$ the corresponding chosen mean and variance (either global, row-, or cloumnwise).

In practice, it turns out normalization by items perform better than global or user normalization.


\section{Minimal model: rank 1 model}

One of the most simple models is a bilinear outer product model where each user and item are represented by a single scalar parameter. All users can then be represented in a single vector $\bu \in \Rn$ and all users in another vector $\bv \in \Rm$.

The reasoning behind this model is each scalar for each item represents their overall quality and each scalar for each user their preference.

Using this model we can reconstruct the matrix as shown in Equation \ref{eq:2-bilin-reconstr}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/bilin-reconstr.tex}
  \caption{Reconstruction of the data matrix $\bA$ using user and item vectors $\bu$ and $\bv$}
  \label{eq:2-bilin-reconstr}
\end{Equation}

The parameters for this model are non-identifiable since a simple scaling of $\bu$ by a scalar and $\bv$ by its inverse produce the same outer product. 

\subsection{Model evaluation}

For this problem the objective is to minimize the squared loss between the predictions and the ground truth. For this we use the Frobenius norm which is simply the squared sum pf the entries of a matrix. Equation \ref{eq:2-objective} shows the mathematical formulation of the objective. Here $\Pi_{\mathbf{\Omega}}(\bR) = \Omega \odot \bR$ where $\odot$ is the Hamard (pairwise entry) product. The projection is needed because not all the entries of $\bA$ are observed, so it is only possible to compute the loss for observed entries.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/objective.tex}
  \caption{Objective for the bilinear outer product model.}
  \label{eq:2-objective}
\end{Equation}

This model is called a rank 1 model because $\bA = \bu\bv^\top$ is rank 1. 

\subsection{Simple case: gradient descent on scalar}

In this simpler setup, the data is only a single scalar and gradient descent is used to find the solution to this problem. Here, the gradient is defined with respect to the model parameters $u$ and $v$, and the loss function is the object of the derivation. The gradient is derived in Equation \ref{eq:2-scalar-grad}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/scalar-grad.tex}
  \caption{Gradient of the loss w.r.t. the parameters.}
  \label{eq:2-scalar-grad}
\end{Equation}

This gradient is then used to update $u$ and $v$, so the loss is minimized. To do so, at each iteration, the values of u and v are updated in the opposite direction of the gradient. This makes the values of $u$ and $v$ converge to a minimum.

\subsubsection{Convexity}

In order for the gradient descent method to converge towards the global minimum, it is necessary for the problem to be convex. In the case at hands this is not the case. Indeed, as shown on the plot in Figure \ref{fig:2-scalar-plot-grad}, the minima is a two branch hyperbola and there is a saddle point at the origin. (NB: for the plot $a = 1$ was used.)

\begin{figure}[H]
  \centering
  \input{res/2-mtx/tikz/scalar-plot-grad.tex}
  \caption{Gradient field of the loss.}
  \label{fig:2-scalar-plot-grad}
\end{figure}

This means gradient descent will converge to any point on the red hyperbola provided the chosen starting point is not $(0,0)$, otherwise the gradient descent method will not function.

Convexity is defined as shown in Equation \ref{eq:2-convexity}. A function $f$ is convex over a domain $R$ if $\forall x_0, x_1 \in R$ and $\forall t \in [0,1]$, if and only if Equation \ref{eq:2-convexity} holds. 

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/convexity.tex}
  \caption{Definition of convexity.}
  \label{eq:2-convexity}
\end{Equation}

This definition essentially means that for any point $p$ between $x_0$ and $x_1$ ($x_0 \leq p \leq x_1$, assuming $x_0 \leq x_1$ w.l.o.g.), $f(p)$ is lower or equal to the line between $f(x_0)$ and $f(x_1)$ evaluated at $p$.

If the function $f$ is once differentiable, this definition is equivalent to the condition shown in Equation \ref{eq:2-convexity-diff} $ \forall x_0, x_1 \in R$. 

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/convexity-diff.tex}
  \caption{Definition of convexity for differentiable functions.}
  \label{eq:2-convexity-diff}
\end{Equation}

The plot in Figure \ref{fig:2-convexity-diff} shows a graphical representation of what the differentiable function convexity definition means.

\begin{figure}[H]
  \centering
  \input{res/2-mtx/tikz/convexity-diff.tex}
  \caption{Graphical representation of the convexity definition for differentiable functions.}
  \label{fig:2-convexity-diff}
\end{figure}

For twice differentiable functions, the condition is equivalent to the hessian being positive semi-definite $\forall x \in \text{int}(R)$. i.e. the hessian is symmetric which is always the case and all its eigenvalues are real and positive.

In the current example, the non convexity of the loss function is easily shown by showing the hessian is indefinite as the eigenvalues are $\pm a$ at $(0,0)$. This fact is derived in more details in Equation \ref{eq:2-is-nonconvex}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/is-nonconvex.tex}
  \caption{Proof the loss is not convex.} 
  \label{eq:2-is-nonconvex}
\end{Equation}

Even if matrix approximation is a non-convex optimization problem, it is nonetheless approximated well in practice and exactly in specific cases.

\subsection{Rank-1 model in general}

Applying the same method as with the scalar case for a fully observed $\bA$, the gradients are derived using the Frobenius derivation rule as shown in Equation \ref{eq:2-rank1-grad}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/rank1-grad.tex}
  \caption{Gradient computation for rank-1 model.}
  \label{eq:2-rank1-grad}
\end{Equation}

The Hessian of the loss is computed in Equation \ref{eq:2-rank1-hess}. Its evaluation at $(\bzer, \bzer)$ shows the problem is non-convex except when $\bA$ is nilpotent (i.e. all its eigenvalues are 0). Thus, in general this problem is non-convex for all dimensions $m,n$.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/rank1-hess.tex}
  \caption{Hessian computation for rank-1 model.}
  \label{eq:2-rank1-hess}
\end{Equation}

\subsection{Gradient dynamics}

Even if the problem is non-convex, we show in this section it is still possible to use gradient descent in order to find one of the minima. This fact is however conditioned on the starting point is not being exactly at the saddle point.

In the simpler case of $m = n = 1$ and using $a > 0$ for convenience, we can model the evolution of $u$ and $v$ by expressing them as a function of time $t$ with a random initialization $u(0) = v(0) = c \in \mathbb{R} \setminus \{0\}$. Setting $u = v$ allows us to study them in an easier way since they will evolve in sync. Thus, we can focus on $x(t) = u(t)v(t)$. We first begin to define $u(t)$ and $v(t)$ with differential equations as their negative gradient flow relates the rate of change of a variable to its negative derivative as shown in Equation \ref{eq:2-gradflow-diffeq-def}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/gradflow-diffeq-def.tex}
  \caption{Definition as differential equations of $u$, $v$, and $x$.}
  \label{eq:2-gradflow-diffeq-def}
\end{Equation}

Solving this ODE yields the expression for $x(t)$ shown in Equation \ref{eq:2-gradflow-diffeq-sol}. Using $a = 1$ and $c = 0.01$ the plot of $x(t)$ is shown in Figure \ref{fig:2-gradflow}.


\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/gradflow-diffeq-sol.tex}
  \caption{Explicit definition of $x(t)$.}
  \label{eq:2-gradflow-diffeq-sol}
\end{Equation}

\begin{figure}[H]
  \centering
  \input{res/2-mtx/tikz/gradflow.tex}
  \caption{Evolution of $x(t)$ for $a=1$ and $c = 0.01$.}
  \label{fig:2-gradflow}
\end{figure}


\subsection{Fully observed matrix}

For the full rank-1 model over a fully observed matrix, we are interested in minimizing the objective shown in Equation \ref{eq:2-full-rank1-loss}. A useful identity is also used to express the loss function.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/objective-rank1.tex}
  \caption{Objective function which is to be minimized.}
  \label{eq:2-full-rank1-loss}
\end{Equation}

The constant term can be disregarded for finding a minimum and the directionality of $\bu$ and $\bv$ are only determined by the second term, so it is possible to split the problem into two separate steps. First, the optimal directions of the vectors are found by solving for unit vectors and then, their magnitude is determined.

In order to solve for the direction of $\bu$ and $\bv$, we use Lagrange multipliers to maximize $\bu^\top\bA\bv$ with the unit norm constraints $\Vert \bu \Vert = 1$ and $\Vert \bv \Vert = 1$. The Lagrangian and its gradients are shown in Equation \ref{eq:2-lagrange}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/lagrange.tex}
  \caption{Lagrangian and its gradients w.r.t $\bu$ and $\bv$.}
  \label{eq:2-lagrange}
\end{Equation}

Putting both these optimality conditions together implies $\bu$ is proportional to the principal eigenvector of $\bA\bA^\top$ because $\bu \propto (\bA\bA^\top)\bu$ and $\bv$ proportional to the is the principal eigenvector of $\bA^\top\bA$ because $\bv \propto (\bA^\top\bA)\bv$. The values for both can be computed using the results in Equation \ref{eq:2-lagrange} and the power method in alternation.


\section{Singular value decomposition (SVD)} 

Here we generalize the results obtained in the previous section using singular value decomposition (SVD). The precise singular value theorem states for all $\Rnm$ matrix $A$, there exists two orthogonal matrices $\bU \in \Rnn$ and $\bV \in \Rmm$ such that $\bA$ can be written as shown in Equation \ref{eq:2-svd-thm}.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/svd-thm.tex}
  \caption{SVD theorem.}
  \label{eq:2-svd-thm}
\end{Equation}

$\bSig \in \Rnm$ is a rectangular diagonal matrix. The singular values $\sigma_i$ are unique, but the left and right singular vectors are unique to a sign flip. With singular value multiplicities, the vector space generated by the singular vectors is unique, but not their basis vectors.

\subsection{SVD and Frobenius norm}

Let $\bA \in \Rnm$ be given by its SVD decomposition $\bA = \bU\bSig\bV^\top$ then 

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/svd-frob.tex}
  \caption{Relationship between the Frobenius norm and the SVD.}
  \label{eq:2-frob-svd}  
\end{Equation}

\subsection{SVD and spectral norm (or Euclidean norm)}

Let $\bA \in \Rnm$ be given by its SVD decomposition $\bA = \bU\bSig\bV^\top$ then 

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/svd-spec.tex}
  \caption{Relationship between the spectral norm and the SVD.}
  \label{eq:2-spec-svd} 
\end{Equation}

\subsection{Eckart-Young theorem} 
The Eckart-Young theorem is a theorem fundamental to many low rank matrix approximation problems. 

Let $\bA \in \Rnm$ whose SVD is $\bA = \bU\bSig\bV^{\top}$. The best rank $k \in \{ 1,\dots,\min\{m,n\}\}$ matrix approximation for $\bA$ is achieved by the matrix $\bA_k = \bU\bSig_k\bV^\top$. Where $\bSig_k = \text{diag}(\tilde{\sigma}_1, \dots, \tilde{\sigma}_{\text{min}(n,m)})$ is a diagonal $\Rnm$ matrix with $\tilde{\sigma}_i = \sigma_i \ \forall i \leq k$ and $\tilde{\sigma}_i = 0 \ \forall i > k $.

This statement is however the simpler version because multiple best approximations can exist with singular value multiplicities. In that case any choice of the largest singular value yields the same Frobenius norm loss.

This theorem means the best rank $k$ approximation in terms of the Frobenius norm of any matrix can simply be read off the SVD of the matrix to approximate.

A corollary here is the loss of the best rank $k$ approximation is simply the sum of the zeroed singular values. i.e.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/ey-fb-loss.tex}
  \caption{Frobenius loss of a rank $k$ approximation.}
  \label{eq:2-ey-fb-loss}
\end{Equation}

The optimal matrix for the spectral norm is also found using this method, because $A_k$ also minimizes the loss with respect to the spectral norm since 

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/ey-sp-loss.tex}
  \caption{Spectral loss of a rank $k$ approximation.}
  \label{eq:2-ey-sp-loss}
\end{Equation}

\subsection{SVD and PCA}

SVD is closely related to the eigendecomposition already seen with PCA. 
\begin{itemize}
  \item If $\bA$ is square and symmetric, then $\bU$ and $\bV$ have equal columns up to sign difference.
  \item If $\bA$ is positive semi-definite, then the SVD is equal to the eigendecomposition.
  \item For squares of $\bA \in \Rnm$, $\bA^\top\bA \in \Rmm$ and $\bA\bA^\top \in \Rnn$,
  \begin{align*}
    \bA\bA^\top = \bU\bSig\bSig^\top\bU^\top = \bU\text{diag}(\sigma_1^2,\dots,\sigma_n^2)\bU^\top \\
    \bA^\top\bA = \bV^\top\bSig^\top\bSig\bV = \bV^\top\text{diag}(\sigma_1^2,\dots,\sigma_m^2)\bV
  \end{align*}
\end{itemize}

It is then possible to use SVD in order to identify the principal eigenvectors of a covariance matrix.

\subsection{SVD and matrix completion}

With completely observed matrices, we can reach a better reconstruction using rank-$k$ approximations instead of rank 1. The rank-$k$ approximation can be seen as the addition of $k$ rank-1 approximations. Using this result we can see that it is possible to find an exact solution in $\bigo(\min\{n^2m, nm^2\})$ for non-convex problems.

For non-completely observed matrices, it is however impossible to use SVD. A first naive approach, SVD with imputations, is performed by filling the unobserved values with averages and then using SVD on the expanded data. This approach is however debatable because the filling of the data actually changed the variance very much and add a lot of false information to the data, specially with very sparsely observed matrices. 

Another idea would be to use a weighted form of the Frobenius norm where only the observed values are counted $\bA_k = \argmin_{\bB} \{\sum_{i,j}w_{ij}(a_{ij} - b_{ij}^2) : \text{rank}(\bB) = k\}$. This problem is however NP-hard even for rank-1.

The only viable option is to resort to approximation algorithms. A question that remains is to know whether the NP-hardness still holds for rank $k$ matrices $\bA$.

\section{Alternating least squares (ALS)}\label{sec:ALS}
With alternating least squares (ALS), the goal is to find $\bu \in \Rnk$ and $\bV\in\Rkm$ that minimize the objective shown in Equation \ref{eq:2-als-obj}. $\lambda \in \mathbb{R}^{+\ast}$ is the regularization parameter that penalizes too large values in $\bU$ and $\bV$ to increase numerical and statistical stability.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/als-obj.tex}
  \caption{ALS loss function.}
  \label{eq:2-als-obj}
\end{Equation}

The parameters to optimize is a polynomial of degree 4 with the components shown in Equation \ref{eq:2-als-param-inter}. The important fact to get from this is in the higher order term involve only one row $i$ of $\bU$ and one column $j$ of $\bV$. 

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/als-param-inter.tex}
  \caption{ALS parameter interactions.}
  \label{eq:2-als-param-inter}
\end{Equation}

This independence on other rows of $\bU$ and other columns of $\bV$ means it is possible to formulate the problem and the objective row by row for $\bU$ and column by column for $bV$.

Treating $\bU$ as a parameter, Equation \ref{eq:2-als-by-col} rewriting the objective for a single column $\bv_j$ of $\bV$.

\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/als-by-col.tex}
  \caption{Objective for a column $\bv_j$ of $\bV$ treating $\bU$ as a parameter.}
  \label{eq:2-als-by-col}
\end{Equation}

With $\bU$ a fixed parameter, this problem is easily solved using least-squares and the solution is shown in \ref{eq:2-als-ls-sol}.
 
\begin{Equation}[H]
  \centering
  \input{res/2-mtx/eqn/als-ls-sol.tex}
  \caption{Least square optimum for $\bv_j$.}
  \label{eq:2-als-ls-sol}
\end{Equation}

This process can also be analogously carried out for rows $\bu_i$ of $\bU$.

From the computational standpoint, this process is thus relatively efficient because it only involves $n$ inversions of a $k \times k$ matrix for each the complete $\bU$ and $m$ inversions for $\bV$.

The full ALS algorithm thus functions by initializing $\bU$ randomly, optimizing for $\bV$ with $\bU$ fixed in $\bigo(mk^3)$, and then fixing $\bV$ and optimizing for $\bU$ in $\bigo(nk^3)$. One important factor to note here is the two optimization steps can be easily parallelized in order to speed up the process. Equation \ref{eq:2-als-process} shows the two optimization steps.

\begin{Equation}[H]
  \input{res/2-mtx/eqn/als-process.tex}
  \caption{Iteration steps for ALS.}
  \label{eq:2-als-process}
\end{Equation}


\subsection{ALS vs. gradient descent}

It would be possible and indeed quite easy to use gradient descent to optimize the objective in Equation \ref{eq:2-als-obj}, as it is quite easily computed. ALS has however one big advantage as it is an iterative algorithm that monotonically improves the objective and converges to a fixed point. Gradient descent can however be finicky because of its initialization and step size and has no monotonic guarantee.

The fixed point ALS reaches is first order optimal meaning the gradient of the loss is $\bzer$, but it may not be the global minimum. It still depends on the initialization.

Another big advantage of ALS is it is easily updated for additional dimensions when a new user or item is added. There is only a need for another row or column optimization, the model does not need to be fully updated.

\section{Projection algorithms}
Projection algorithms is another approach, which is a little more expensive than ALS, thus it is more rarely used.

The idea behind projection algorithms is to relax the rank constraint which makes the problem non-convex because the set of matrices of rank $k$ is non-convex. Projection algorithms thus do the approximation without this rank constraint and then project the approximation onto the space of rank $k$ matrices. This makes the problem convex. These often use SVD as a subroutine.

\subsection{Singular value projection}

This method is an instance of projection algorithms. The gradient is computed in the unconstrained set of matrices, and the result is then projected onto the space of rank $k$ matrices. The iterations of projected gradient descent is described in Equation \ref{eq:2-proj-iters}. Here $\eta > 0$ is the learning rate, and $[\cdot]_k$ represent the best rank $k$ projection, often performed using SVD. The SVD step is quite expensive because it is an SVD of a full rank matrix that is always changing.

\begin{Equation}[H]
  \input{res/2-mtx/eqn/proj-iters.tex}
  \caption{Projected gradient descent steps.}
  \label{eq:2-proj-iters}
\end{Equation}

\subsection{Nuclear norm relaxation}

Another idea is to use the nuclear norm instead of the Frobenius norm to express the loss function. With this approach, the goal is to use a convex set of matrices containing all rank-$k$ matrices, but not containing too many of matrices whose rank is bigger than $k$ in the considered set. The nuclear norm relaxation is one that provides such a convex hull.

The nuclear norm is defined as shown in Equation \ref{eq:2-nucl-norm} and is simply the sum of the singular value, not the squared sum as with the Frobenius norm.

\begin{Equation}[H]
  \input{res/2-mtx/eqn/nucl-norm.tex}
  \caption{Nuclear norm definition}
  \label{eq:2-nucl-norm}
\end{Equation}

This approach is a generalization of the idea of using the 1-norm to find sparse solutions as for Lasso for instance. In this context, the sparseness of the singular values is synonymous with a low rank.

\subsubsection{Convex envelope theorem}

The convex envelope of a function $f: R \mapsto \mathbb{R}$, is the largest convex function $g$ such that $g \leq f$ on $R$. 

Fazel et al. showed the convex envelope of the rank of a matrix $\bA$ on $R = \left\lbrace \bA : \Vert\bA\Vert_2 \leq 1 \right\rbrace$ is the nuclear norm $\Vert\bA\Vert_{\ast}$. 

In other words, the nuclear norm is the convex envelope of not only rank constrained, but also spectral norm constrained matrices. This theorem is the motivation for the use of nuclear norm as a surrogate for the Frobenius norm.

\subsubsection{Shrinks}

Cai et al. define the concept of a shrink of a matrix, let $\bA = \bU\text{diag}(\sigma_i)\bV^\top$ then the shrink of $\bA$, $\text{shrink}_{\tau}(\bA)$ is defined in Equation \ref{eq:2-shrink}.

\begin{Equation}
  \input{res/2-mtx/eqn/shrink.tex}
  \caption{Definition of the shrink of $\bA$.}
  \label{eq:2-shrink}
\end{Equation}

Then $\text{shrink}_{tau}(\bA) = \bU\text{diag}(\sigma_i - \tau)_{+}\bV^\top$. $\tau$ can be used as a tradeoff parameter to penalize more the rank of $\text{shrink}_{tau}(\bA)$ or the Frobenius loss.

\section{Shrunk algorithm}
Putting the nuclear norm relaxation and the spectral projection algorithm is simply substituting the projection onto rank-$k$ matrices of the gradient with simply using the shrink of $\bA^{t}$ in the gradient. The algorithm is shown in Equation \ref{eq:2-nucl-algo}.

\begin{Equation}[H]
  \input{res/2-mtx/eqn/nucl-algo.tex}
  \caption{Shrunk algorithm steps.}
  \label{eq:2-nucl-algo}
\end{Equation}

With a suitable step size schedule $\eta_t$, the convergence of the algorithm is shown in Equation \ref{eq:2-shrunk-conv}.

\begin{Equation}[H]
  \input{res/2-mtx/eqn/shrunk-conv.tex}
  \caption{Convergence of the algorithm.}
  \label{eq:2-shrunk-conv}
\end{Equation}

A huge difference this algorithm makes is it keeps the sparseness of $\bA$ at all update steps which hugely reduces computational costs. Conceptually, this approach brings the projection inside the steps instead of being performed on the whole step. 

\section{Exact matrix reconstruction}

Given $\bA$ is indeed a low rank rank-$k$ matrix, under which observation condition is it possible to exactly reconstruct $\bA$ ?

\subsection{Information theoretic lower bound}

The information theoretic lower bound on the number of observations in order to fully determine a rank $k$ matrix can be derived as follows. In order to fully determine a rank-$k$ matrix, $k$ singular values are needed as well as $k$ row vectors in $\bU$. However, for each of these vectors, the number of necessary observation decrease with each subsequent vector. For the first row vector $\bu_{1} \in \Rn$, only $(n-1)$ entries are necessary since the vectors are unit length thus all entries are fully determined by the other entries of the vector. For subsequent row vectors $\bu_{i}$ the number of necessary observations is $(n-i)$ since they are unit length but also orthogonal to the previous $i-1$ vectors. there are only $n-i$ degrees of freedom. The same goes for the columns of $\bV$. Altogether the degrees of freedom for the complete characterization of a rank-$k$ matrix $\bA$ is shown in Equation \ref{eq:2-mtx-deg-fdm}.

\begin{Equation}[H]
  \input{res/2-mtx/eqn/rank-k-deg-fdm.tex}
  \caption{Degrees of freedom of a rank-$k$ matrix.}
  \label{eq:2-mtx-deg-fdm}
\end{Equation}

The information theoretic lower bound is then $2kn - k^2$ thus  the full rank-$k$ matrix reconstruction is impossible without at least $2kn - k^2$ observations.

\subsection{Probabilistic analysis}
The previously derived lower bound is however not a guarantee the matrix can be fully reconstructed, because it is possible (and likely) the random observations share common factors and are thus not revealing information about all the necessary values in the rows of $\bU$, columns of $\bV$ or the singular values. They could for instance all lie in the same vector subspace.

The problem that characterizes this question is the coupon collector problem where the question is the number $t$ of coupons necessary to acquire from a set of $n$ different coupons to ensure all the different $n$ coupons are acquired on average. 

Let $t_i$ be the number of necessary coupons necessary to acquire a new coupon given $i-1$ other coupons have already been acquired. This distribution is geometric with $P = (n-i + 1) / n$. The number of coupons necessary to acquire in order to get them all is $T = \sum_{i=1}^{n} t_i$. Both the expectation of $t_i$ and $T$ are shown in Equation \ref{eq:2-exp-geom}.
\begin{Equation}
  \input{res/2-mtx/eqn/exp-geom.tex}
  \caption{Expectation for $t_i$.}
  \label{eq:2-exp-geom}
\end{Equation}

Thus, back in the context of matrix observation the number of necessary observations is $\bigo(\log(2kn - k^2))$.

There are however worst cases. For instance if $\bu_1 = \be_1$ and $\bv_1 = \be_1$, then one has to sample $a_{11}$ in order to recover $\sigma_1$, otherwise the reconstruction is impossible. The coupon collector assumes an even distribution of the information across the entries of the matrix.