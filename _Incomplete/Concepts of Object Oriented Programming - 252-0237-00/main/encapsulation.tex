\chapter{Information Hiding and Encapsulation}
In order for independent parts of programs to cooperate efficiently and securely, their interface must be defined rigorously. This can be done with the types, but a way to provide or to deny access to certain internal functionalities of code is required. The mechanisms that allow for the access control are information hiding.

\section{Information hiding}
Information hiding is the technique to reduce the interdependencies of the modules between each other. The client of a module should be provided with all the necessary information to use the module correctly without needing information that is not private. Contracts as well as method signature and names are part of the exported interfaces. Information hiding focuses on the static aspects of the program, i.e. considerations about code and not about program executions, respect of invariants or other aspects which are dealt with by encapsulation \ref{sec:encapsulation-encapsulation}.\\
Information hiding enables the easy renaming of elements because the scope where the element could be accessed or used is limited and clearly defined. It also allows for easy exchange of implementation since all that is required is not to change the interface. Apart from the interface the implementation can be changed at will as long as the interface remains unchanged. NB: the interface include the contracts and documentation.

\subsection{Interfaces of a class}
The objectives of information hiding is to provide strict interfaces which hide the implementation details to reduce the dependencies between modules. With that, classes can be studied reviewed and tested in isolation because they interact in simple well-defined ways. 
\paragraph{Client interface} The client interface of a class included its name, its type parameters and bounds, the superclasses it extends and interfaces it implements, the signatures of exported methods and fields, and the client interface of the direct superclass.
\paragraph{Subclass interface} As well as all the information provided to the client interface, the subclass interface provides efficient access to superclass fields and to auxiliary methods.
\paragraph{Friend interface} The friend interface provides all the information the client interface provides plus the mutual access to implementations of cooperating classes and hides auxiliary classes. \\
Other interfaces are also possible in different languages.\\
In Java the information hiding is provided by access modifiers: \texttt{public} which indicates the belonging of an element to the public interface, \texttt{protected} for the subclass and friend interface, the default access modifier for the friend interface and \texttt{private} for the hidden implementation.\\
Eiffel provides information hiding through the clients' clause in feature declaration which allow the programmer to indicate to which types the implementation is visible: \texttt{feature \{ANY\}} for the client interface, \texttt{feature \{T\}} for the friend interface for class \texttt{T} and its subclasses, and \texttt{feature \{NONE\}} for the implementation. With \texttt{feature \{NONE\}} the element is only available to the \texttt{this} object not to other objects of the same class. All the exports also include by default the subclasses.

\subsection{Method selection in Java JLS 1}
In Java, the rules for overriding methods regarding the access control is that it is only authorized to relax the access control (default being the stricter, then protected, then public). A subclass can only override a method if it is visible from the subclass. If it is not accessible by the subclass, the definition of a method of the same signature in the subclass will hide the method in the superclass. Private elements cannot be overridden since they are not visible to the subclass.\\
The method selection in Java language specification 1 (JLS 1) works as follows. At compile time, the compiler determines the static declaration of a method, then checks its availability. Finally, it determines the invocation (virtual/nonvirtual). Then at runtime, the JVM computes the receiver reference and locates the method to invoke based on the dynamic type of the receiver object.
However, the method selection has a problem. Example \ref{ex:encapsulation-jls1-problem} shows an instance where a subclass overrides a method it should not be able to override, which results in the choice of the method of the subclass where the superclass method should have been chosen art runtime according to the rules.

\begin{example}[Problems with default access methods in JLS1]\label{ex:encapsulation-jls1-problem}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    package PT;
    public class T {
        void m() {...}
    }

    public class Test {
        public static void main(String[] args){
            T t = new PS.S();
            t.m() // S.m() is called but it does not override T.m()
        }
    }
        \end{lstlisting}
    \end{minipage}

    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    package PS;
    public class S extends PT.T {
        public void m() {...}
    }
        \end{lstlisting}
    \end{minipage}
\end{example}

\subsection{Method selection in Java JLS 2}
Java JLS2, the problem described above is fixed by specifying that the last step of the method selection at runtime should be to locate the method to invoke \textit{that overrides statically the determined method}. This new definition still has problems however which allows to break the information hiding. Example \ref{ex:encapsulation-jls2-problem} shows an instance where a method is selected when it should not have been accessible by the caller. This is due to the fact that \texttt{protected} does not always provide as much access as \texttt{protected} when it is used in another package.

\begin{example}[Problems with protected access methods in JLS2]\label{ex:encapsulation-jls2-problem}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    package PT;
    public class T {
        protected void m() {...}
    }

    public class Test {
        public static void main(String[] args){
            T t = new PS.S();
            t.m() // S.m() is called but should not be visible in PT
        }
    }
        \end{lstlisting}
    \end{minipage}

    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    package PS;
    public class S extends PT.T {
        protected void m() {...}
    }
        \end{lstlisting}
    \end{minipage}
\end{example}

\section{Encapsulation}\label{sec:encapsulation-encapsulation}
Encapsulation mainly deals with the dynamic aspects of the program. It is a technique to structure the state space of the executed programs. The goal is to guarantee data and structural consistency by establishing capsules with clearly defined interfaces. The capsules can be individual objects, object structures (linked list), a class with all its objects, all classes of a subtype hierarchy, or a package with all its classes and objects. Encapsulation requires a definition of the boundaries of the capsule and its interfaces.\\
The consistency of objects is achieved by only information hiding and defined by the constraints on their fields. It should not be possible for any accessor (client, friend, other object, or subclass) to make changes that violate the consistency of the object. In short the consistency of objects can be achieved by 
\begin{enumerate}
    \item Applying information hiding to hide the internal representation of objects wherever possible.
    \item Make the consistency criteria explicit by using contracts or informal documentation to express the criteria (e.g. invariants).
    \item Check the interfaces to ensure all exported operations of an object, including subclass methods, preserve the documented criteria.
\end{enumerate}
Object consistency is however not totally achievable in some languages. Java, contrarily to Eiffel with \texttt{feature \{NONE\}}, does not provide a way to hide elements to objects of the same class. \\
The invariants must hold in the visible states of the objects, temporary violation of the invariants in invisible states is fine. The invariants can thus only be expressed on \texttt{private} fields in Java or \texttt{feature \{NONE\}} fields in Eiffel. For each invariant we have to show that all exported methods and constructors preserve the invariants of all objects of the class, and that all constructors also establish the invariant of the new objects to ensure object consistency.