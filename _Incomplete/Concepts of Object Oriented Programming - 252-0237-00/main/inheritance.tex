\chapter{Inheritance}
In order to reuse the code as frequently as possible thus avoiding reinventing the wheel each time, two main solutions exits: Aggregation and Inheritance. Aggregation establishes a \textit{has-a} relation between two objects. Aggregation allows for the other object owned to change at runtime and establishes no subtype relationship between the two types. Inheritance establishes an \textit{is-a} relationship. This relationship is fixed at compile time and is often coupled with subtyping.

\section{Inheritance and Subtyping}
\begin{definition}[Subtyping]
    Subtyping expresses classification. It can allow for polymorphism and must comply to Liskov substitution principle \ref{thm:liskov}.
\end{definition}
\begin{definition}[Inheritance]
    Inheritance is only a means of code reuse. Inheritance without subclassing is also called \textit{private inheritance}.
\end{definition}
\begin{definition}[Subclassing]
    Subclassing is both inheritance and subtyping. 
\end{definition}

Subclassing is the norm amongst the most popular object oriented languages since when inheritance takes place, it implies a structural subtype relation, and, even in a nominal typesystem, inheritance provides with default methods. Even if not available out of the box, subclassing can often be simulated using a combination of subtyping and aggregation (Example \ref{ex:subclass-sim-code} simulates \texttt{Student} subclassing \texttt{Person}, the hierarchy is shown in Figure \ref{fig:subclass-sim}). This pattern can be very useful in single inheritance programming languages to simulate multiple inheritance. 

\begin{example}[Simulation of subclassing]\label{ex:subclass-sim-code}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    interface Person {void print();}
    interface Student extends Person {int getRegNum();}

    class PersonImpl implements Person{
        String name;
        PersonImpl(String n) {name = n;}
        void print() {...}
    }

    class StudentImpl implements Student {
        Person p;
        int RegNum;
        StudentImpl(String n, int rn) {
            p = new Person(n); 
            RegNum = rn;
        }
        void getRegNum() {return RegNum}
        void print() {p.print(); ...}
    }
        \end{lstlisting}
    \end{minipage}
\end{example}
\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}
        \node[draw, rectangle, minimum width=3cm] (P) {Person};
        \node[draw, rectangle, below left of=P, node distance=2cm, minimum width=3cm] (S) {Student};
        \node[draw, rectangle, below of=S, node distance=1.5cm, minimum width=3cm] (SI) {StudentImpl};
        \node[draw, rectangle, right of=SI, node distance=5cm, minimum width=3cm] (PI) {PersonImpl};

        \draw[-stealth,thick] (S) -- node [left] {<:} (P);
        \draw[-stealth,thick] (SI) -- node [left] {<:} (S);
        \draw[-stealth,thick] (PI) -- node [above] {<:} (P);

        \draw[-stealth,thick, blue] (SI) -- node [above] {\textit{Has-a}} (PI);

    \end{tikzpicture}
    \caption{Simulation of \texttt{Student} subclassing \texttt{Person} with only subtyping and aggregation}
    \label{fig:subclass-sim}
\end{figure}

Often however classes and their hierarchies have divergent subtyping and inheritance requirements. For instance, when modelling a circle and an ellipse with the interface specified in Example \ref{ex:subclass-circle-ellipse}, the circle is a special kind of ellipse, thus it would be natural for circle to subtype ellipse. However, the ellipse has a wider interface, thus should in principle inherit from circle.

\begin{example}[Circle and Ellipse interfaces]\label{ex:subclass-circle-ellipse}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style = style, language = Java]
    class Circle {
        Point center;
        double radius;
        double getRadius() {...}
        double area() {...}
    }

    class Ellipse {
        Point center;
        double radius, radius2;
        double getRadius(){...}
        double getRadius2(){...}
        double area() {...}
        void rotate(){...}
    }
        \end{lstlisting}
    \end{minipage}
\end{example}

According to subtyping, \texttt{Circle} should be a subtype of \texttt{Ellipse}, but reuse would indicate to inherit in \texttt{Ellipse} the methods of \texttt{Circle}, override \texttt{area}, and add a \texttt{getRadius2()} method. Whenever this happens in a language where only subclassing is available, the programmer is left with two choices: make \texttt{Circle} subclass \texttt{Ellipse} and constraint the radii ($r_A = r_B$), which require a non-modular check since subclasses cannot strengthen invariant; or to use aggregation to combine them.\\
A problem of the same nature arises when defining the relationship between a \texttt{Set} and a \texttt{BoundedSet}. Neither of these can be subtypes of each other since doing so would break behavioral subtyping. A hacky solution which works is to treat \texttt{Set} as a bounded set with infinite capacity, but this solution is also problematic since it requires foresight of the \texttt{Set} class developer. In this case since no subtype relationship exist between the two classes, but code reuse is still desirable, it is possible to achieve code reuse without subtyping in language that only support subclassing by using statically bound calls (see definition \ref{def:static-bound}) as in Example \ref{ex:subclass-reuse-wo-subtyping} while still conforming to behavioral subtyping. This comes at the loss of polymorphism, but in this case, since no subtype relation exist, it does not really matter.

\begin{example}[Reuse without subtyping with statically bound calls]\label{ex:subclass-reuse-wo-subtyping}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style = style, language= Java]
    abstract class AbstractSet {
        ... 
        // requires true
        // ensures contains(o)
        final void doAdd(Object o) {/*add o to set*/}

        // requires false
        // ensures true
        void add(Object o) {doAdd(o);}
    }

    class BoundedSet extends AbstractSet {
        ... 
        //requires true
        //ensures old(size - capacity) => contains(o)
        void add(Object o) {
            if (size < capacity) {
                doAdd(o); size ++;
            } 
        }
    }
        \end{lstlisting}
    \end{minipage}
\end{example}
Some languages even provide a syntactic way to specify these static contracts. Instead of declaring both \texttt{add} and \texttt{doAdd} in \texttt{AbstractSet} we could declare only \texttt{add} with the contract being \texttt{requires false, ensures true} for the overrides to conform to behavioral polymorphism, and the static contract being \texttt{requires true, ensures contains(o)} which is applied whenever \texttt{add} is statically called (for instance with \texttt{super.add(o)}) in the subclasses. Other languages such as C++ and Eiffel allow for inheritance without subtyping, however, in C++, inheritance comes with \textit{private inheritance} which establishes a subclass relation only known to the subclass. Thus, one needs to be careful not to treat \texttt{Set} and \texttt{BoundedSet} polymorphically in \texttt{BoundedSet} or in its subclasses otherwise the type problems resurface, and we are back to square one.

\section{Dynamic Method Binding}
\begin{definition}[Static binding]\label{def:static-bound}
    The method declaration is chosen at compile time for each call, based on the static type of the receiver expression.
\end{definition}
\begin{definition}[Dynamic binding]
    The method declaration is chosen at run time for each call, based on the dynamic type of the receiver expression.
\end{definition}
While dynamic method binding is less performant than static method binding (since the method lookup takes time)and poses versioning problems, dynamic method binding allow for subtype polymorphism and specialization. Static binding is the default in C++ and C\#, and dynamic binding is in Eiffel, Java, Scala, and all dynamically typed languages.

\subsection{Fragile base class scenario} 
When software evolves for maintenance, bug fixing or reengineering, dynamic calls can be the cause of many issues. These problems always arise after this sequence of modifications: A class is written, then someone implements a subclass, and finally someone changes the superclass. The problems can be caused by multiple factors: by the incomplete overriding of methods in subclasses, by unjustified assumptions about a dynamic call in the superclass, by the introduction of mutual recursion, or by the addition of supplementary methods in the superclass who happen to have the same name as an additional method in the subclass which now are overridden. \\
In order to avoid such problems, a few rules of thumb can help to avoid errors. The implementer of the superclass should not change calls to dynamically bound methods (even new methods) and should only rely on the documentation of the dynamically bound calls, never to their actual implementation. The implementer of the subclass should override all methods that could break invariants declared in the subclass (even if it is to have the same method in the super- and subclass); should only rely on the documentation of the method overridden, never to their implementation; avoid overriding methods in classes that are supposed to change (the \texttt{Collections} API in Java is probably fine, but a library in alpha version 0.1.2 is not).\\
Example \ref{ex:subclass-fragile-assumptions} shows an example where unjustified assumptions about the \texttt{sqrt} method in \texttt{Super} lead to a fragile base class scenario after the override of \texttt{sqrt} in \texttt{Sub}. Where most likely an exception is thrown because the precondition of \texttt{sqrt} in \texttt{Super} is not complied to. In this case the implementer of the new version is to blame for the mistake since they should not have added dynamically bound calls nor expected anything else than what the documentation stated about \texttt{sqrt} (Note that \texttt{sqrt} in \texttt{Sub} does not even change the contract from the method it overrides).
\begin{example}[Fragile baseclass because of unjustified assumptions]\label{ex:subclass-fragile-assumptions} 
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language = Java]
    class Super {
        // requires f >=0
        // ensures result^2 = f
        float sqrt(float f) {...}

        /* previously: */
        // requires f >=0
        // ensures result^4 = f
        float fourthrt(float f) {...} // direct computation

        /* new verison: */ 
        // requires f >=0
        // ensures result^4 = f
        float fourthrt(float f) {return sqrt(sqrt(f));}

    }
    class Sub extends Super {
        // requires f >=0
        // ensures result^2 = f
        float sqrt(float f) {return -super.sqrt(f);}
    }

        \end{lstlisting}
    \end{minipage}
\end{example}

Even without explicit overrides, some languages such as Java always choose the most specific method declaration, which might still be the cause of fragile base class scenarios as in example \ref{ex:subclass-fragile-no-override} where \texttt{foo("abc")} will result in the call of \texttt{foo(String s)}, not of the expected \texttt{foo(Object o)}. In C\# however, the methods are selected based on the most specific method in the class of the receiver, then in the superclass, etc. In this C\#, this scenario could not happen.

\begin{example}[Fragile baseclass without overrides]\label{ex:subclass-fragile-no-override}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style = style, language = Java]
    class Super {
        // Added after implementation of foo(Object) in Sub
        void foo(String s) {...} 
    }
    class Sub extends Super {
        void foo(Object o) {...}
    }

    Sub s = new Sub();
    s.foo("abc");
        \end{lstlisting}
    \end{minipage}
\end{example}

In summary, for achieving proper subclassing one must only use subclassing when proper subtyping relation exist (syntactic and behavioral); never relay on any implementation details, but only on the documentation; when changing super class which might have been subclassed; never mess with dynamic calls (nor deletion, nor addition, nor reordering); and never specialize superclasses which might change.

\subsection{Binary methods}
Binary methods are methods which take the receiver and an additional argument. It is often necessary to specify the behavior of the method depending on the dynamic type of bot the receiver and the argument. Several solutions are possible to do so: explicit type checks and type casts; double invocation pattern (Example \ref{ex:subclass-double-invocation}); overloading plus dynamic i.e. disabling the static type checking for the call (Example \ref{ex:subclass-overload-dyn}); or multiple dispatch.\\
The double invocation pattern uses dynamic method binding to dispatch the call to the most specific method by invoking a method from another and reversing the arguments order. This is also called the visitor pattern.
\begin{example}[Double invocation]\label{ex:subclass-double-invocation}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style = style, language = Java] 
    class Shape {
        Shape intersect(Shape s) {
            return s.intersectShape(this);
        }
        Shape intersectShape(Shape s) {
            // generic intersection
        }
        Shape intersectRectangle(Rectangle r) {
            return this.intersectShape(s)
        }
    }
    class Rectangle extends Shape {
        Shape intersect(Shape s) {
            return s.intersectRectangle(this);
        }

        Shape intersectRectangle(Rectangle r){
            // efficient intersection
        }
    }
        \end{lstlisting}
    \end{minipage}
\end{example} 
By overloading and combining dynamic type checking, the desired feature can also be achieved. In languages that support turning off the static type checking for certain methods, like C\#, doing so enables the choice of the method to happen with the dynamic type of the argument and the receiver so the best method is selected.
\begin{example}[Overloading and dynamic]\label{ex:subclass-overload-dyn}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style = style, language={[Sharp]C}] 
    class Shape {
        Shape intersect(Shape s) {
            // generic intersection
        }
    }
    class Rectangle extends Shape {
        Shape intersect(Rectangle r) {
            // efficient intersection
        }
    }

    static Shape intersect(Shape s1, Shape s2) {
        return (s1 as dynamic).intersect(s2 as dynamic);
    }
        \end{lstlisting}
    \end{minipage}
\end{example} 
Multiple is yet another language feature which allows for these features while still being typesafe. It allows the developer to specify both a static type and a dynamic type for an argument. This will allow the program to consider the method whenever the method is called with an argument of the static type, and only choose this one if its dynamic type corresponds to the dynamic type specified. 

\section{Multiple Inheritance}
Multiple inheritance can tremendously increase the amount of code reuse and allow types to be subtypes of several others. This feature is not available in all programming languages but when it is not available, it can be simulated via aggregation and delegation. In languages that support the feature, multiple inheritance can be a challenge in two situations: when there are ambiguities between the methods or fields of supertypes, and when the inheritances repeat thus the inheritance graph is diamond shaped.

\subsection{Ambiguity resolution}
When inheriting from two classes which have two methods of the same signature or fields of the same type and name, the subclass has to provide a way to decide which method is called. In C++, the ambiguity can be resolved by using scope operators, but this requires the knowledge of which class were inherited from in the subclass by the user of the subclass. The methods can also be both overridden in the subclass to avoid the need of the user to know implementation details. This is also done by using scope operators, but this time in the subclass.\\
Sometimes, the methods cannot be merged since their behavior is different and merging them is nonsensical. In this situation, a proposed solution by the Eiffel language is to rename the inherited methods. In this case, dynamic binding can even take account of this change to bind the correct method. 

\subsection{Repeated inheritance}
Repeated inheritance i.e. multiple inheritance from the same class, also raises some questions: if the multiple times inherited class has fields, how many copies of these fields is necessary in the inheriting subclass? Who initializes the super classes and in which order ?
\subsubsection{C++ solution}
C++ solves these problems by \textit{virtual} inheritance. If classes \texttt{B} and \texttt{C} both inherit from class \texttt{A}, and class \texttt{D} inherits from both \texttt{B} and \texttt{C}, the default behavior (nonvirtual inheritance) is to create two copies of the super class. In this setup the fields and methods are duplicated and \texttt{A} is initialized once from \texttt{B} once from \texttt{C}.\\
\texttt{B} and \texttt{C} can also be marked to virtually inherit \texttt{A}. When they both do, \texttt{A} is not duplicated in \texttt{D} and, since it is unclear whether \texttt{B} or \texttt{C} should initialize \texttt{A}, \texttt{D} initializes \texttt{A} in its constructor.\\
Virtual inheritance however imposes a runtime overhead and requires foresight by the developers thus nonvirtual inheritance is the default. Moreover, with virtual inheritance, when classes inherit virtually from a superclass, they cannot take anything for granted at initialization. Indeed, the initialization of the superclass might be left to a subclass, thus it is impossible to derive invariants from the initialization locally done of the superclass.

\subsubsection{Eiffel solution}
The solution implemented in Eiffel for that issue is not to constraint the developer of a subclass to call any super constructor. The programmer is free either to call the super constructor, or to initialize the fields manually directly in the subclass. This freedom is good, but both approaches have each their disadvantages. If the super constructors are automatically called, then, in the diamond layout the superclass (\texttt{A} in the previous example) the constructor will get called twice. This poses problems when the constructor have side effects or if the arguments of the two different calls are different. If on the other hand, the decision is taken never to call super constructors, a lot of code duplication in the constructors will happen, and the subclasses need to know implementation details to initialize the superclass properly.

\section{Traits and linearization}
Linearization is another solution to the multiple inheritance problem. In Scala, it is only allowed to have multiple inheritance with traits. Only one superclass is possible, but many super traits are allowed.
\subsection{Traits and mixins}
Traits in Scala (or mixins in other languages) are a kind of abstract class that are used to augment the feature of another class. By in inheriting from a class the trait enriches it can then override methods, create new fields, and declare new methods. Traits are not instantiable as such but only as an addition to the class they extend. For instance the traits \texttt{Backup} inheriting from the \texttt{Cell} class augments it with an ability to recover the  old value of the cell after an update of the cell's value took place. This trait can only be used in conjunction with a \texttt{Cell}. It can only be instantiated as follows \texttt{new Cell with Backup} and the combination \texttt{Cell with Backup} defines a new type. 
\subsubsection{Ambiguity resolution}
The ambiguities which arise when a class inherits from two traits with methods or fields with the same name or signature is always resolved by merging in Scala. The implementer of the subclass must override the ambiguous methods. This method is however not suited when the two ambiguous methods or fields have different behaviors, moreover, this method does not work for mutable fields.\\
In diamond shapes, the bottom class does not have to merge the methods if they are both overrides of a method in the common superclass. In this case the process through which the method is selected is linearization.
\subsection{Linearization}
Linearization is the process by which the compiler resolves ambiguities and the repeated inheritance problems. When a class inherits from a class with multiple traits and the resulting inheritance graph is a diamond, linearization turns the diamond into a sequence.
\begin{definition}[Linearisation]\label{def:linearisation}
    For a class $C$ defined as $C$ extends $C'$ with $C_1$ with $C_2 \dots$ with $C_n$, the linearization of the class $C$, $L(C)$ is computed as follows 
    \[
        L(C) = C, L(C_n) \bullet L(N_{n-1}) \bullet ... \bullet L(C_1) \bullet L(C')
    \]
    Where $\bullet$ is the operation which removes duplicates to only keep the leftmost instance. It is formally defined as follows, where $A$, $B$ are sequences, $a$ an element, and $\epsilon$ the empty sequence.
    \begin{align*}
        \epsilon \bullet A &= A\\
        (a,A) \bullet B &= 
        \begin{cases}
            a, (A \bullet B) & a \notin B \\
            A \bullet B & a \in B
        \end{cases}
    \end{align*}
\end{definition}
In order to resolve the conflicts which arise from multiple inheritance, Scala first linearizes the inheritance graph and uses the linearization to resolve conflicts and to bind methods. Thus, in repeated inheritance, only one copy of the common superclass is instantiated. By using the linearization, the super calls refer to the successor in the linear order; the objects are initialized in the inverse linear order; the overrides override methods from their linearization successor; and the parameters for the initialization is provided by the immediate predecessor class (not trait).\\
The linearization order for the super calls allows composing traits to achieve stackable specialization. Without it, it would be impossible for a trait to call the same method of another trait. With linearization, a simple super call does the job.\\
Although traits long with linearization are a very expressive tool, it can be hard to work with them since they are highly dynamic types. More particularly, a trait cannot know how their super class is initialized, where their super calls are bound to, and which methods they override.