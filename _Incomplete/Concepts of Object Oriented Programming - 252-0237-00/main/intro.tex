\chapter{Introduction}
The different kinds and flavors of existing and possible Object Oriented Programming languages can be specified more cleanly by making the distinction between requirements, Core concepts, Language concepts, and language design components. Figure \ref{fig:intro-intro} provides an example of these categories and what their content is.
\begin{figure}[!ht]
    \centering
    \resizebox*{\textwidth}{!}{
    \begin{tikzpicture}
        \tikzstyle{bf} = [text width=3cm,text centered,font=\bfseries]
        \tikzstyle{square} = [draw,rectangle,text width=3cm,text centered]
        \node[bf] at (0,0) {Requirement};
        \node[bf] at (4,0) {Core concept};
        \node[bf] at (8,0) {Language concept};
        \node[bf] at (12,0) {Language design};
        \node[square] at (0,-1.5) {Highly Dynamic Execution Model};
        \node[square] at (0,-3) {Cooperating Program Parts with Interfaces};
        \node[square] at (0,-4.5) (CS) {Classification and specialization};
        \node[square] at (0,-5.5) {Correctness};
        \node[square] at (4, -1.5) {Object Model};
        \node[square] at (4, -3) {Interfaces and encapsulation};
        \node[square] at (4, -4.5) (CP) {Classification and polymorphism};
        \node[square] at (8, -1.5) (C) {Classes};
        \node[square] at (8, -2.5) (I) {inheritance};
        \node[square] at (8, -3.5) (S) {Subtyping};
        \node[square] at (8, -4.5) (DB) {Dynamic binding};
        \node[square] at (12, -1.5) (SI) {Single inheritance};
        \node[square] at (12, -2.5) (MI) {Multiple inheritance};
        \node[square] at (12, -4) (IW) {nheritance without subtyping};

        \draw (CS.east) -- (CP.west);
        \draw (CP.east) -- (C.west);
        \draw (CP.east) -- (I.west);
        \draw (CP.east) -- (S.west);
        \draw (CP.east) -- (DB.west);
        \draw (I.east) -- (SI.west);
        \draw (I.east) -- (MI.west);
        \draw (I.east) -- (IW.west);

    \end{tikzpicture}
    }
    \caption{Categories and their content}
    \label{fig:intro-intro}
\end{figure}
\section{Requirements}
At some point, procedural and imperative languages started to be too simple or ill fitted to the new nature of the software development industry. A few new requirements which could not be fulfilled satisfyingly by procedural and imperative languages emerged: 
\begin{itemize}
    \item Code reuse\\
    Find new methods to reuse code efficiently. For instance, C structs are good, but they can only be used up to a certain extent. An efficient code reuse mechanism would allow for better quality code, easier to extend and adapt, and easier to use through documented interfaces.
    \item Computation as simulation\\
    Modeling real world entities and describing their behavior is very natural using Object Oriented Programming. The simulations are then easily runnable by simply running the code.
    \item Distributed programming\\
    Data and its processing needs to be distributed, communication is needed between the nodes, and concurrency is required.
    \item GUIs\\
    Concurrency and adaptable standard functionalities are required.
\end{itemize}
\section{Core concepts}
Core concepts are concepts that encapsulate a very basic idea of how the programming language. They differ from the language concepts by the fact that these are general and abstract concepts created meet the requirements and which apply broadly to all Object Oriented programming languages.
\subsection{The Object Model}
The Object Model states a few principles:
\begin{itemize}
    \item A software system is a set of cooperating objects.
    \item Objects have abilities and states.
    \item Objects interact by sending messages to one another.
\end{itemize}
Objects have a state, an identity, a lifecycle, a location, and a behavior. Compared to imperative programming, they lead to a different program structure and execution model.
\subsection{Interfaces and encapsulation}
Objects have well-defined interfaces with publicly accessible methods and fields. Their implementation is hidden behind the interface in order to achieve encapsulation and hide unnecessary information. Interfaces are the basis with which the behavior of the objects are described.
\subsection{Classification \& subtyping}
Classification is the hierarchical structure the objects are structured by. Objects can belong to multiple classes simultaneously. The rule that defines whether a class is a subclass of another one is the Liskov substitution principle.
\begin{theorem}[Liskov substitution principle]\label{thm:liskov}
    A class \texttt{A} is a subclass of another class \texttt{B} whenever an object of the class \texttt{A} can be used wherever an object of the superclass \texttt{B} is expected.
\end{theorem}
\begin{remark}
    Other types of polymorphism exist:
    \begin{itemize}
        \item Parametric polymorphism (generic types)
        \item Ad-hoc polymorphism (method overloading)
    \end{itemize}
\end{remark}
The relationship subclass have to their parent classes is a specialization relationship. The classes at the very top of the inheritance hierarchy are the most general, whereas the ones at the bottom are very specialized. They can extend the capabilities, attributes, and behavior. They only need to fulfill the Liskov substitution principle, i.e. comply to the inherited behaviors and attributes.
\section{Language concepts}
Language concepts are the translation of the Core concepts onto a specific language. These specify the way the aforementioned Core concepts are interpreted, and the choices made to comply to this interpretation of the core concepts. It is worth to note that many interpretations of a class can be defined which all comply to the concepts. One has to be chosen, and this choice is defined in the Language concepts. 
\subsection{Dynamic method binding}
This concept represents the ability of a method call on an object of a class \texttt{A} to be, at runtime, bound to the method of the subclass \texttt{B} from which the object on which the method is called is a member of. 
For instance, for the hierarchy shown in Figure \ref{fig:intro-hierarch}, if a method \lstinline{print()} is defined as a method of \lstinline{Person} and if \lstinline{Student} redefine it to specify its behavior, the call to the method \lstinline{print()} on an object of class \lstinline{Student} results in the call of the specified \lstinline{print()} method.
\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[align=center]
        \node[align=center,text width=1.5cm] at (0,0)   (P)  {Person};
        \node[align=center,text width=1.5cm] at (-2,-2) (S)  {Student};
        \node[align=center,text width=1.5cm] at (0,-2)  (A)  {Assistant};
        \node[align=center,text width=1.5cm] at (2,-2)  (Pr) {Professor};
        \node[align=center,text width=1.5cm] at (-2,-4) (B)  {Bachelor Student};
        \node[align=center,text width=1.5cm] at (0,-4)  (M)  {Master Student};
        \node[align=center,text width=1.5cm] at (2,-4)  (D)  {PhD Student};

        \draw (P) -- (S); 
        \draw (P) -- (A); 
        \draw (P) -- (Pr); 
        \draw (S) -- (B); 
        \draw (S) -- (M); 
        \draw (S) -- (D); 
        \draw (A) -- (D); 
    \end{tikzpicture}
    \caption{Sample hierarchy}
    \label{fig:intro-hierarch}
\end{figure}
\section{Language design}
The language design is mainly driven by the general design goals for a language, which are:
\begin{itemize}
    \item Simplicity \\
    A language whose syntax and semantics is easy to understand by learners, users, and implementers of the language.
    \item Expressiveness \\
    Ability to (easily) express complex processes and structures. It often conflicts with simplicity.
    \item (Static) safety \\
    Ability to make errors hard to achieve and allows them to be discovered and reported early, ideally at compile time. It often conflicts with expressiveness and performance.
    \item Performance\\
    Ability to run the code efficiently. It often conflicts with safety, productivity, and simplicity.
    \item Modularity\\
    Ability to compile modules (parts) separately. It often conflicts with expressiveness and performance. 
    \item Productivity\\
    Ability to develop at low development cost (i.e. quickly). It is related to expressiveness and often conflicts with static safety.
    \item Backwards compatibility\\
    Ability for the programs to interface between older and newer versions of the language. Often conflicts with simplicity, performance, and expressiveness.
\end{itemize}