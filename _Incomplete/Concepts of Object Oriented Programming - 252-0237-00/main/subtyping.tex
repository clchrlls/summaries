\chapter{Types and subtyping}
Objects interact with one another (accessing other's fields and methods) by sending each other messages. A safe language detects and reports (ideally at compile time) the accesses in which the recipient does not have the required method or field. Type systems are used to detect such errors. For instance, Java forces the user to interact with the system resources through an API which implements access control and other security mechanisms. In this case, the code must be prevented from bypassing the API.

\section{Type system}
\begin{definition}[Type]\label{def:type}
    A type is a set of values sharing some properties. A value $v$ has type $T$ if $v$ is an element of $T$.
\end{definition}

\begin{definition}[Type system]
    A type system is a tractable syntactic method for proving absence of certain program behaviors by classifying phrases according to the kinds of values they compute.\cite{pierce2002types}
\end{definition}

Here \textit{Syntactic} means the rules based on the form not the behavior, \textit{phrases} are expressions, methods, etc. of a program and the \textit{kinds of values} are the types.

\subsection{Weak and strong typing}
Programming languages can be split into three categories according to the strength of their type system.\\
Untyped languages do not classify values into types, values can be interpreted as any conceivable type. It is the programmer's responsibility to ensure the processed data is in a format suitable for its use. Assembly languages are untyped languages.\\
Weakly-typed languages classify values into types but do not enforce any supplementary restrictions. For instance in C or C++, it is possible to cast any pointer to a \lstinline{void} pointer and to cast this pointer into a pointer of any type afterwards. It is also the programmer's responsibility to ensure no cast leads to invalid data.\\
Strongly-typed languages classify values into types and force the programmer to use values only where a value of a corresponding type is expected. I.e. it ensures all operations are applied to arguments of a suitable types. C\#, Java, Python, Scala, Smalltalk, and Eiffel use strong types.\\
The stronger the typesystem is, the easier it is to detect erroneous or undesirable program behaviors.

\subsection{Nominal and structural types}
Another way of classifying the type systems is to look into what makes two objects of the same types. There are essentially two ways to go about that: nominal and structural types.\\
Nominal types consider two objects to be of the same type if some syntactic equivalence exists between them. With this scheme, only two instances of the same class would be type equivalent. \\
Structurally typed languages use the structure of the object to be equivalent. If two distinct classes have methods and fields of the same name, a structurally typed language would consider an instance of one class and an instance of the other to be type equivalent. Structural types are a superset of the nominal types.

\subsection{Static and dynamic type checking}
In strongly typed languages, in order to prevent the strong type requirement to be disregarder by the programmer, the type of the arguments are checked to verify they comply to the types declared explicitly by the programmer or inferred by the compiler. \\
In statically type-checked languages, this check is performed at compile time whereas this check is performed at run time by dynamically type-checked languages. 

\subsubsection{Static type safety}

\begin{definition}[Static type safety]
    A programming language is called type-safe if its
design prevents type errors.
\end{definition}
\begin{theorem}
    A statically type-safe object oriented language guarantees the following type invariant: In every execution state, the type of the value held by variable $v$ is a subtype of the declared type of $v$.
\end{theorem}
Type-safety guarantees the absence of certain run-time errors. This is however done at the expense of an amount of expressiveness. A statically type-safe language cannot allow for code generation at run-time and is much more conservative in the return types of its functions for instance. Some languages (e.g. Scala, C\#) allow for ways to bypass static checks at compile time (the keyword \lstinline{dynamic} allows for that in C\#). The advantages of both flavors of typing are described below:
\begin{itemize}
    \item Advantages of static checking
    \begin{itemize}
        \item Static safety: many errors are found at compile-time 
        \item Readability: types are excellent documentation
        \item Efficiency: Type information allows for optimization 
        \item Tool support: types allow for auto-completion, refactoring, etc.
    \end{itemize}
    \item Advantages of dynamic checking
    \begin{itemize}
        \item Expressiveness: no correct program is rejected by the type checker
        \item Simplicity: static type systems are more complex
        \item Low overhead: type annotations are useless
    \end{itemize}
\end{itemize}
\section{Syntactic subtyping}
Subtypes are types who satisfy the Liskov substitution principle (Theorem \ref{thm:liskov}). The subtype relation corresponds to the subset relation on the values of a types (see Definition \ref{def:type}). It is possible to classify the different flavours of subtyping according to many different heuristics and from which different classes of subtyping arise. 

\begin{definition}[Subtyping noatation]
    Whenever the type $A$ is a subtype of the type $B$ we note $A$ \texttt{<:} $B$, or equivalently, $B$ \texttt{:>} $A$.
\end{definition}

\paragraph{Syntactic vs. behavioural}
In syntactic classification, the subtype objects can at least understand the messages the supertype understands. In behavioural subtyping, the subtype objects provide at least the behaviour of the supertype.

\paragraph{Nominal vs. structural}
Nomnal subtyping systems base the subtype relationship on the type names. The subtype relation is based on explicit type declaration (\texttt{class B extends A} in Java). On the other hand, structural subtying systems are base the decision of a type being a subtype or not depends on the type structures. If a type methods and parameters is a super set of another, it is a subtype.

\subsection{Variance}
Variance is the concept foe naming the subtype relationship of ttypes which contain another type as a parameter noted as. Generics (\texttt{T<$\cdot$>}) or functions types (\texttt{F<A,B> = A $\rightarrow$ B}) are instances of this sort of types.

\begin{definition}[Covariance]\label{def:covariance}
    For a type \texttt{T<$\cdot$>} with other types as parameters, the type \texttt{T<$\cdot$>} is covariant if, given two types \texttt{A} and \texttt{B} with subtype relation \texttt{B <: A}, then, \texttt{T<B> :> T<A>}.
\end{definition}
\begin{definition}[Contravariance]\label{def:contravariance}
    For a type \texttt{T<$\cdot$>} with other types as parameters, the type \texttt{T<$\cdot$>} is contravariant if, given two types \texttt{A} and \texttt{B} with subtype relation \texttt{B <: A}, then, \texttt{T<A> :> T<B>}.
\end{definition}
\begin{definition}[Nonvariance]\label{def:nonvariance}
    For a type \texttt{T<$\cdot$>} with other types as parameters, the type \texttt{T<$\cdot$>} is nonvariant if, given two types \texttt{A} and \texttt{B} with subtype relation \texttt{B <: A}, then, neither \texttt{T<A> :> T<B>} nor \texttt{T<B> :> T<A>}.
\end{definition}

\subsection{Nominal subtyping and substitution}\label{sec:nom-subtype-rules}
While a nominal subtype is requires to at least understand the message the supertype understands, it can (and often does) widen the objetcs interfaces. This widenning can be achieved by multiple ways. New methods and fields can be added to the subtype, the accessibility of the parameters and methods common to the super type can be more flexible in the subtype, and the type of methods and fields can be more general. In summary:
\begin{itemize}
    \item Subtypes can add methods and fields but not remove them (see Example \ref{ex:subtype-rm-fields}).
    \item Subtypes fields and methods can be more accessible but not less. (same example as above but instead of the method being deleted, the method is inaccessible)
    \item Parameter types of the subtype methods can more general/contravariant (see Definition \ref{def:contravariance}), but not more specific/covariant (see Definition \ref{def:covariance}, Example \ref{ex:subtypt-param-cov}).
    \item Subtype method return types can be more specific/covariant (see Definition \ref{def:covariance}) but not more general/contravariant (see Definition \ref{def:contravariance} and Example \ref{ex:subtypt-ret-contra}).
    \item Subtype field types must not change. This is a consequence of the fact that fields can be seen as a pair of getters/setters whose return and parameter type must not be covariant on the one hand and not contravariant on the other hand.
\end{itemize}
\begin{remark}[Immutable fields override]
    While it is theoretically possible to specialize the type of immutable fields only if the superclass constructor does not assign an unsuitable value to the field. This feature is however not allowed in any mainstream languages.
\end{remark}
\begin{remark}[Eiffel allows narrowing interfaces]
    Eiffel nevertheless allows the programmer to change the existence of methods, overriding with covariant types, or to specialize the field types, but at runtime, the system passes a \texttt{null} value to the program and the program has to be prepared to deal with such values otherwise a runtime exception is raised.
\end{remark}

\begin{example}[Removing fields is unsafe]\label{ex:subtype-rm-fields}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    class Super {
        void foo() {...}
        void bar() {...}
    }
    class Sub extends Super {
        void foo() {...}
        //no bar()
    }
        \end{lstlisting}
    \end{minipage}\\
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=badstyle, language=Java]
    baz(Super s) {s.bar();}
    Sub sub = new Sub();
    baz(sub);
        \end{lstlisting}
    \end{minipage}
\end{example}

\begin{example}[Covariant parameters are unsafe]\label{ex:subtypt-param-cov}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    class A {}
    class B extends A {}
    class C { void foo(A a) {...} }
    class D extends C{ void foo(B b) {...} }
        \end{lstlisting}
    \end{minipage}\\
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=badstyle, language=Java]
    C c = new D();
    A a = new A();
    c.foo(A);
        \end{lstlisting}
    \end{minipage}
\end{example}

\begin{example}[Contravariant return types are unsafe]\label{ex:subtypt-ret-contra}
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=style, language=Java]
    class A {}
    class B extends A {}
    class C { B void foo() {...} }
    class D extends C { A foo() {...} }
        \end{lstlisting}
    \end{minipage}\\
    \begin{minipage}{.95\textwidth}
        \begin{lstlisting}[style=badstyle, language=Java]
    B b;
    C c = new D()
    b = c.foo();
        \end{lstlisting}
    \end{minipage}
\end{example}

\subsection{Covariant arrays}

In some languages, for expressiveness and historic reasons, arrays are covariant. This however breaks static type safety. Indeed, a field in an array can be seen as a field of a class which cannot be variant for the reasons explained in Section \ref{sec:nom-subtype-rules}. Nowadays, the gained expresiveness of covariant arrays can be implemented in a type safe manner with generics.

\subsection{Nominal subtyping and reuse}
Nominal subtyping can impede the reusability of the code. If two types representing two similar concepts are taken from two different libraries, it is impossible to use them in a generic way (e.g. a type \texttt{Student} and a type \texttt{Professor} come from two different libraries, we cannot put them in a single list to iterate over them).\\
One of the workaround is to use the adapter pattern in which two new types are created which inherit from the same type, each of which will contain an instance of the corresponding type and forward the calls. This approach however causes memory and runtime overheads and forces the programmer to create a lot of boilerplate code.\\
Another possibility which exists in some research languages (e.g. Sather and Cecil) support generalisation i.e. supertyping. In these languages it is possible to declare a supertype after subtypes have been implemented. This approach has also some issues since generalissation does not necessarily imply an inheritance relationship, and the generalised types might already have supertypes, which leads to multiple inheritance problems. 

\subsection{Nominal subtyping and generalisation}
Nominal subtyping also forces the methods parameters to be overly restrictive. In the Java collections API for instance, the method \texttt{printData(Collection<String> c)} takes a Collection in argument whereas, not nearly all the methods available in Collections are used in this method. This for instance disallows the user to use this function whith a parameter that would satisfy \texttt{printData}'s needs but doesn't conform to a Collection.\\
This issue can be fixed by the use of interfaces, but even for collections alone, the variety of collections that could be used gives rise to too many interfaces to be practical (since the combination from $n$ function grows with $n^2$). Thus, this solution is not a real and practical one.\\
Still in Java, the choice was made to make some of the functions in the interfaces optional which allow for more expressiveness, but, again, break the type safety.

\subsection{Structural typing and substitution}
Regarding the reuse, structural subtyping is very experssive since two types with the a common part of their interface are already subtypes of a type with their intersection of interface. For generalisation, the problem is not posed.

\subsection{Type systems in OO-Languages}
Two main combinations of statically/dynamically checked typed systems and nominal/structural type systems exist: The dynamic and structural OO-languages, and thestatic and nominal OO-languages. Dynamic and structural type systems allow for the maximum expressiveness and flexibility. Nominal and static sype systems allow for the maximum static safety. The two other combinations are much rarely used because they each have problems. Structural and static type systems would force the declaration of many types and problems with the semctics of subtyping arise (to be seen later). Dynamic and nominal ttype systems do not really make sense because why would the programmer need to write all this type information if it is not used to statically check the program ?

\section{Behavioral subtyping}\label{sec:behavioural-subtyping}
In contrast to syntactic subtyping, behavioural subtyping is not based in whether two types have some syntactic link (explicit inheritance or name of methods) to determine if there exists a subtype relation between them, but rather in the behaviour of the types.

\subsection{Contracts}
In order to specify the behaviour of the types, the \textit{contracts} are created. Contracts are part of the code but do not perform any computation during runtime. They are only here to enfore certain conditions on the behaviour of the type and the objects. These contracts are in the form of logical expressions involving the fields and methods of the type they describe the behaviour of
\begin{remark}[Visible states]
    The contracts are only meaningful in the visible states of an object. Indeed, since operations are performed sequentially, the contracts might be breached during the execution of a method, but, once it terminates, the contracts must still hold.
\end{remark}

\subsubsection{Method behaviour}
Method behaviour contracts describe the behaviour of a method of a given type. It is composed of two parts: the precondition (PRE), and the postcondition (POST). The precondition specifies the conditions to which the parameters of the method must comply to in order for the postcondition to be guaranteed. The postcondition is the predicate that specifies which guarantees are given after the method terminates. The precondition is indicated with the keyword \texttt{requires} and the postcondition with the keyword \texttt{ensures}. In postconditions, $\old{n}$ indicates the value of $n$ before the execution of the method.

\subsubsection{Object invariants}
Object invariants (INV) are predicates on the value of the fields of an object. It might for instance require that a field never takes on a negative value for instance.

\subsubsection{History constraints}
History constraints (Cons) specify the way the value of the fields of the objects of a type will change during the lifetime of the object. For instance a field of an object should be only increasing during its lifetime and never decrease. Here, $\old{n}$ is used to indicate the value of $n$ at \textbf{any} previous instant. The history constraints must be reflexive and transitive to be correct.

\subsection{Contracts checking}
Contracts can either be checked dynamically or statically. While dynamical contract essentially boils down to runtime assertions, it has a couple of advantages: The contracts do not need to be complete since not all the properties can be checked at runtime efficiently anyway, it efficiently complements testing, and add only a low overhead. Static contract checking is likely to find more errors at complie time than dynamic checking, but it is exrtremely complex (even undecidable), requires extensive contracts, and impose a large overhead.

\subsection{Nominal subtyping}
In behaioural subtyping, the subtyping correctness is harder to verify than in structural subtyping, where we only concern ourselves with the types. In behavioural subtyping, it is necessary to guarantee the behaviour complies with the supertype. 

\subsubsection{Rules for subtyping}
The subtype $T$ objects must fulfill the contract of the supertype $S$, but some flexibility is possible: 
\begin{itemize}
    \item Subtypes can have stronger invariants i.e. $\inv{T} \Rightarrow \inv{S}$
    \item Subtypes can have stronger history constraints i.e. $\cons{T} \Rightarrow \cons{S}$
    \item Overriding methods of subtypes can have weaker preconditions i.e. $\pre{T} \Rightarrow \pre{S}$
    \item Overriding methods of subtypes can have stronger postconditions i.e. $\old{\pre{T}} \Rightarrow \left( \post{S} \Rightarrow \post{T}\right)$
\end{itemize}

\subsubsection{Specification inheritance}
In order to enforce the subtyping ruels of behavioural subtyping, it is possible to define an inheritance mechanism for the specifications. In order to do so, we must define the \textit{effecive} invariants of the types.
\begin{itemize}
    \item The effective class invariant (EffInv) of a type $T$ is the conjunction of the invariants of $T$ and of its supertypes.
    \item The effective history constraint (EffCons) of a type $T$ is the conjunction of the history constraints of $T$ and of its superclasses.
    \item The effective precondition (EffPre) of a method $m$ of a type $T$ is the disjunction of the preconditions of the method $m$ of $T$ and of the methods $m$ overrides in the superclasses of $T$.
    \item The effective postcondition (EffPost) of a method $m$ of a type $T$ a bit more complicated to define. If $T$ \texttt{<:} $S$ \texttt{<:} $R$ \dots, then 
    \begin{align*}
        \effpost{T.m} = & \left(\old{\pre{T.m}} \Rightarrow \post{T.m} \right) \land \\
                        & \left(\old{\pre{S.m}} \Rightarrow \post{S.m} \right) \land \\
                        & \left(\old{\pre{R.m}} \Rightarrow \post{R.m} \right) \land \\
                        &\dots
    \end{align*}
\end{itemize}

\subsection{Structural subtyping}
In structural subtyping, if no addition is made to the current process of specification of the inheritance, the caller can no longer guarantee it satifies the preconditions of the methods they call since they have no static knowledge of the contracts. Thus, the caller cannot blamed for not complying to the preconditions.\\
Thus, in this cases the caller should always state the expected behaviour of the parameters they use in their methods in order to be able to use static behavioural structural type checking. For instance:

\begin{lstlisting}[style=style, language=Java]
    void render( {void draw() requires P ensures Q} p) {
        p.draw();
    }
\end{lstlisting}
In this setup, static checking of the compliance of the methods is possible (even if not automatic in general). Dynamic checking is however not possible since the caller cannot be blamed for not complying to the preconditions of the method used and the callee cannot be blamed for not complying to the postconditions required by the calling method.

\subsubsection{Types as contracts}
Types can be treated as a specific subgenra of contracts. Note that these contracts are decidable if the underlying type system is type safe. For this purpose we introduce the operator $\type{v}$ which yields the dynamic type of the object stored in $v$.

\subsection{Invariants over inherited fields}
The invariants over the inherited fields are not protected against forbidden changes by methods which have access to the field in question.
\begin{lstlisting}[style = style, language = Java]
    package Library;
    public class Super {
        protected int f;
    }

    /*...*/

    package Client;
    public class Sub extends Super {
        // invariant f >= 0
    }
\end{lstlisting} 
\begin{lstlisting}[style=badstyle, language = Java]
    package Library;
    class Friend {
        void foo(Super s) {s.f = -1;}
    }
\end{lstlisting}
Thus the static checking of such invariants is not possible, thus invariants over inherited fields are not modular.

\subsection{Immutable types}
The relationship between types which have fields (all of them except \texttt{Object} in Java for instance) and their immutable version is not a subtype relationship in any direction. Indeed, the immutable version cannot be a subtype since it has a narrower interface (no getters), and the mutable version cannot be a subtype either since the behaviour is not specialized and contradicts Liskov substitution principle.\\
The clean solution (implemented in Scala for instance) thus does not define any subtype relationship between mutable and immutable version of types (except for \texttt{AnyRef} in Scala which is the alias for the Java \texttt{Object}). In Java however the API contains immutable types which are subtypes of mutable types, but all mutating methods are optional, which break static safety once again.


