\chapter{Introduction}
\section{Algebra}
\subsection{Groups}
\begin{definition}[Groups]
    A group is a mathematical structure $\langle G, \ast \rangle$ consisting of an non-empty set $G$ and a binary operation $\ast : G \times G \mapsto G$ satisfying the following axioms:
    \begin{enumerate}
        \item The operation $\ast$ is associative. i.e. 
        \[
            \forall a,b,c \in G, \ (a \ast b) \ast c  = a \ast (b \ast c)
        \] 
        \item There exists a neutral element $e \in G$ for $\ast$ such that 
        \[
            \forall a \in G, \ a \ast e = e \ast a = a
        \]
        \item Every element $x \in G$ has an inverse $\hat{x} \in G$ such that 
        \[
            x \ast \hat{x} = \hat{x} \ast x = e
        \]
    \end{enumerate}
\end{definition}
\begin{definition}[Abelian groups]
    For $\langle G, \ast \rangle$, if $\ast$ is commutative, the $G$ is commutative or abellian group. i.e. 
    \[ 
        \forall a,b \in G, \ a \ast b = b \ast a 
    \]
\end{definition}
In additive groups, $\ast$ is denoted by +, inverses by -, and the neutral element by 0. In multiplicative groups $\ast$ is denoted by $\cdot$, inverses by $^{-1}$, and the neutral element by 1. \\
Simple examples of groups are integers with additions, i.e. $\langle \mathbb{Z}, + \rangle$, reals without zero and with multiplication, i.e. $\langle \mathbb{R} \setminus 0, \cdot \rangle$, or the integers up to $m$ ($\{0, 1,, \dots, m-1\}$) with addition modulo $m$, i.e. $\langle \mathbb{Z}_m, \oplus_m \rangle$.

\begin{definition}[Order of a group]
    The order of a group $G$, denoted $\vert G \vert$ is the number of elements in the set $G$.
\end{definition}

\begin{definition}[Order of an element]
    The order of an element $x$ of a group $G$, denoted $\ord{x}$ is the least natural number $k$ such that $x^{k} := \underbrace{x \ast \dots \ast x}_{k\ \text{times}} = e$. In particular $\ord{e} = 1$.
\end{definition}

\begin{theorem}[The order of an element divides the order of the group]
    The order of any element $x \in G$ divides the order of the group. i.e.
    \[
        \forall x \in G \ \exists a, \  a \ast \ord{x} = \vert G \vert    
    \]
    Consequently $\forall x \in G, \ x^{\vert G \vert} = e$.
\end{theorem}

\begin{definition}[Cyclic groups]
    A finite group $G$ is cyclic if there exists an element $g \in G$, called the generator, such that $G = \{g^0, g^1, \dots, g^{p-1}\}$. In such cases $g$ generates $G$ which is denoted by $G =- \langle g \rangle$.
\end{definition}

\begin{definition}[Isomorphism]
    Two groups $\langle G, \ast \rangle$ and $\langle H, \star \rangle$ are isomorphic, denoted $G \cong H$ if there exists a bijection $\varphi : G \mapsto H$ such that 
    \[
        \forall x, y \in G, \ \varphi(x \ast y) = \varphi(x) \star \varphi(y)
    \]
    An isomorphism can be seen as two groups being the same up to relabeling the elements.
\end{definition}

\subsection{Inverse modulo $m$ and the Group $\mathbb{Z}_{m}^\ast$}

\begin{definition}[Congruence]
    Two numbers $x, y \in \mathbb{Z}$ are congruent modulo $m$, denoted $x \equiv y \mod m$ if $m$ divides $(x - y)$, i.e. $\exists a \in \mathbb{Z}, \ m = a(x - y)$.
\end{definition}
\begin{definition}[Modular inverse]
    $y$ is the inverse modulo $m$ of $x$ if 
    \[
        x \cdot y \equiv 1 \mod m
    \]
\end{definition}
\begin{theorem}[Existence of inverses]
    If $x$ and $m$ are coprime (i.e. they do not share any factors except 1) then has an inverse $y$ modulo $m$.
\end{theorem}
The extended euclidian algorithm can be used to find such an inverse since it allows to find coefficients $u,v$ such that $ux + yv = \gcd(x,y)$. With $x,m$ coprime, $\gcd(x,m) = 1$ and thus $ux \equiv 1 \mod m$, thus $u$ is the inverse of $x$.\\
The above implies that $\langle \mathbb{Z}_m^\ast, \odot_m \rangle$, where $\mathbb{Z}_m^\ast = \{x \in \mathbb{Z} \vert 0 \leq x \leq m, \gcd(x,m) = 1\}$ is a group with neutral element 1. If $m$ is the product of two primes $p,q$, then $\vert \mathbb{Z}_m^{\ast} \vert = (p-1)(q-1)$ and $\langle \mathbb{Z}_m^{\ast}, \odot_m \rangle \cong \langle Z_p^\ast \times Z_q^\ast, \odot_{p,q} \rangle$.
\subsection{RSA scheme}
The RSA scheme is a public key encryption scheme. For a receiver to be able to receive encrypted messages, they first choose two large prime numbers $p$ and $q$. They then choose a value $e$ coprime to $|\mathbb{Z}_m^\ast| = (p-1)(q-1)$. The receiver then publishes the pair $(m, e)$ and computes the inverse $d$ of $e$ modulo $|\mathbb{Z}_m^\ast|$ which they keep secret. With these values a sender transforms their message in an element $x$ of the group $\mathbb{Z}_m^\ast$ and computes the ciphertext $c$ by raising the plaintext $x$ to the exponent $e$. When the receiver obtains $c$, they raise it to the exponent $d$ which recovers the plaintext because 
\[
    c^d = x^{ed} = x^{k|\mathbb{Z}_m^\ast| + 1} = (x^{|\mathbb{Z}_m^\ast|})^k \cdot x = 1^k \cdot x = x
\]
\begin{remark}
    For the the RSA scheme to work properly, it must be hard to factor $m$. Moreover the above scheme, plaintext RSA, is not secure as is since it is not randomised. Thus, it should not be used as is.
\end{remark}
\subsection{Chinese remainder theorem}
\begin{theorem}[Chinese remainder theorem]
    For a system or $r$ congruence relations with pairwise coprime moduli $m_1, \dots, m_r$:
    \begin{align*}
        x \equiv &a_1 \mod m_1\\
        &\vdots\\
        x \equiv &a_r \mod m_r
    \end{align*}
    There is a unique solution $x$ with $0 \leq x < M := \prod_{i=1}^r m_i$.\\
    The solution is found by first, for each relation $i$, computing $M_i = M / m_i$, then finding the inverse $N_i$ of $M_i$ modulo $m_i$. Thus $a_iN_iM_i \equiv a_i \mod m_i$, but for all other $j$, $a_jN_jM_j \equiv 0 \mod m_j$. Thus 
    \[
        x \equiv \sum_{i = 1}^r a_iM_iN_i \mod M
    \]
\end{theorem}

\subsection{Quadratic residues}
\begin{definition}[Quadratic residues]
    Let $m \in \mathbb{N} \setminus \{0\}$. An integer $a$ is called a quadratic residue modulo $m$ if there exists an integer $r$ (called the square root modulo $m$ of $a$) such that 
    \[
        r^2 \equiv a \mod m
    \]
    Otherwise $a$ is a quadratic non-residue.
\end{definition}
\begin{definition}[Legendre symbol]
    For a prime number $p$, the legendre symbol is defined as
    \[
    \legendre{a}{p} := \begin{cases}
        1 &\text{if}\ a \ \text{is}\ \text{a} \ \text{quadratic} \ \text{residue} \ \text{modulo} \ p\\
        0 &\text{if}\ a \ \text{is}\ \text{dividible} \ \text{by} \ p\\
        -1 &\text{if}\ a \ \text{is}\ \text{a} \ \text{quadratic} \ \text{non-residue} \ \text{modulo} \ p
    \end{cases}
    \]
\end{definition}
\begin{theorem}[Multiplication of quadratic residues]
    The values of the legendre symbol were chosen for the symbol to satisfy
    \[
        \legendre{ab}{p} = \legendre{a}{p} \legendre{b}{p}
    \]
\end{theorem}
\section{Theoretical computer science}
\subsection{Polynomial, Negligible, and Noticable functions}
\begin{definition}[Polynomial functions]
    A function $f : \mathbb{N} \mapsto \mathbb{R}$ is polynomial if it can be upper bounded by some polynomial. i.e.
    \[
        \exists c \in \mathbb{N}\  \exists n_0 \ \forall n \geq n_0 \ f(n) \leq n^c
    \]
\end{definition}
\begin{definition}[Negligible functions]
    A function $f : \mathbb{N} \mapsto \mathbb{R}$ is negligible if it decreases faster than the inverse of every polynomial. i.e.
    \[
        \forall c \in \mathbb{N}\ \exists n_0\ \forall n \geq n_0, \ f(n) \leq \frac{1}{n^c}
    \]
\end{definition}
\begin{definition}[Noticable functions]
    A function $f : \mathbb{N} \mapsto \mathbb{R}$ is noticable if it can be lower bounded by the inverse of a polynomial. i.e.
    \[
        \exists c \in \mathbb{N}\  \exists n_0 \ \forall n \geq n_0 \ f(n) \geq \frac{1}{n^c}
    \]
\end{definition}
\begin{remark}
    An algorithm is called efficient if its running time grows polynomially with its input length. Noticable is not the inverse of negligible.
\end{remark}

\subsection{Decision problems and Languages}
Algorithms are formalised as Turing machines, but for our purpose, we do not need this definition. For our purposes, an algorithm $A$ takes some input $z \in \{1,0\}^\ast$, performs some computation, and outputs a value $A(z)$. The running time and space somplexity of an algorithm is measured as a function of its input length $\vert z \vert$. An algorithm is \textit{efficient} if its running time is bounded by a polynomial.\\
In theory of computation a decision problem is a special type of computational problem where the answer is binary: 0 or 1.\\
A decision problem can be described as a formal language $L$. A formal language $L$ is a subset of all inputs ($L \subseteq \{1,0\}^\ast$). Then the elements of $z \in L$ corresponds to instances of the decision problem for which the answer it 1, and $z \notin L$ if the answer is 0.
\begin{definition}[Complexity class \textbf{P}]
    A language $L$ is in \textbf{P} if there exists  an efficient algorithm $A$ such that for all $z \in L$:
    \begin{align*}
        z\in L &\Rightarrow A(z) = 1 \\
        z \notin L &\Rightarrow A(z) = 0 
    \end{align*}
\end{definition}
 
